package com.studio.karya.lakonesia.Profile.Akun;

import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.studio.karya.lakonesia.R;

import java.util.ArrayList;


public class ReviewRecycleViewAdapter extends RecyclerView.Adapter<ReviewRecycleViewAdapter.ReviewRecycleViewHolder> {

    private ArrayList<ReviewModel> dataList;

    private String TAG = "===== Review Adapter";

    public ReviewRecycleViewAdapter(ArrayList<ReviewModel> dataList) {
        this.dataList = dataList;
    }

    public void addItem(ArrayList<ReviewModel> mData) {
        this.dataList = mData;
        notifyDataSetChanged();
    }

    @Override
    public ReviewRecycleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.fragment_profile_akun_review_layout, parent, false);
        return new ReviewRecycleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ReviewRecycleViewHolder holder, int position) {

        Picasso.get().load(dataList.get(position).getFoto_url()).fit().centerCrop().into(holder.foto_member);
        holder.ratingBar.setRating(Float.parseFloat(dataList.get(position).getRating()) );
        holder.nama_member.setText(dataList.get(position).getNama_pengirim());
        holder.ulasan.setText(dataList.get(position).getUlasan());
        String tanggal[] = dataList.get(position).getCreated_at().split(" ");
        tanggal = tanggal[0].split("-");
        Log.d(TAG, "onBindViewHolder: " + tanggal[0]);
        holder.tanggal.setText(tanggal[2] + "/" +tanggal[1] + "/" + tanggal[0] );
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class ReviewRecycleViewHolder extends RecyclerView.ViewHolder{
        private ImageView foto_member;
        private TextView nama_member , tanggal , ulasan;
        private RatingBar ratingBar;

        public ReviewRecycleViewHolder(View itemView) {
            super(itemView);
            foto_member = (ImageView) itemView.findViewById(R.id.fragment_akun_review_foto_member);
            nama_member = itemView.findViewById(R.id.fragment_akun_review_nama_member);
            tanggal = itemView.findViewById(R.id.fragment_akun_review_tanggal);
            ulasan= itemView.findViewById(R.id.fragment_akun_review_text_ulasan);
            ratingBar = itemView.findViewById(R.id.fragment_akun_review_rating);
        }
    }
}
