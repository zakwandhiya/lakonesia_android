package com.studio.karya.lakonesia.Transaction;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.studio.karya.lakonesia.HargaFragment.HargaFragment;
import com.studio.karya.lakonesia.Model.CurrentUserModel;
import com.studio.karya.lakonesia.Model.LakonModel;
import com.studio.karya.lakonesia.R;
import com.studio.karya.lakonesia.Utils.PublicInt;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Calendar;

import static android.app.Activity.RESULT_OK;


public class TransactionDialog extends BottomSheetDialogFragment {
    private BottomSheetListener mListener;

    private ArrayList<TransactionModel> transactionModels;
    private LakonModel lakonModel;

    private int storeYear = 0;
    private int storeMonth = 0;
    private int storeDay = 0;


    private int storeHour = 0;
    private int storeMinute = 0;



    private String TAG = "===== Transaction Dialog";

    private RecyclerView recyclerViewTransac;
    private TextView txtLocation;
    private TransactionRecycleViewAdapter transactionAdapter;
    private HargaFragment hargaFragment;
    public TransactionDialog() {
    }

    public static TransactionDialog newInstance(ArrayList<TransactionModel> transactionModels , LakonModel lakonModel , HargaFragment hargaFragment) {
        TransactionDialog transactionDialog = new TransactionDialog();
        transactionDialog.transactionModels = transactionModels;
        transactionDialog.lakonModel = lakonModel;
        transactionDialog.hargaFragment = hargaFragment;
        return transactionDialog;
    }

    void initLakonRecycleView(){
        recyclerViewTransac = getView().findViewById(R.id.transaction_recycle_view);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerViewTransac.setLayoutManager(layoutManager);
        recyclerViewTransac.setNestedScrollingEnabled(false);


        transactionAdapter = new TransactionRecycleViewAdapter(transactionModels);
        recyclerViewTransac.setAdapter(transactionAdapter);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_transaction_dialog_layout, container, false);

        return v;
    }

    public int calculateTotal(){
        int sum = 0;
        for(TransactionModel entry : transactionModels ) {
            int hargaTmp =  Integer.parseInt(entry.getHarga());
            sum += (hargaTmp * entry.getKuantitas());
        }
        return sum;
    }

    public int calculateAdminFee(int total){
        return total/20;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initLakonRecycleView();

        int total = calculateTotal();
        int adminFee = calculateAdminFee(total);
        TextView txtNamaLakon = getView().findViewById(R.id.transaction_dialog_nama_lakon);
        txtNamaLakon.setText(lakonModel.getNama());

        TextView txtAdminFee = getView().findViewById(R.id.transaction_dialog_admin_fee_num_text);
        txtAdminFee.setText("RP "+calculateAdminFee(total));



        TextView txtHargaTotal = getView().findViewById(R.id.transaction_dialog_price_num_text);
        txtHargaTotal.setText("RP "+calculateTotal());


        TextView txtHargaTotalSetelahDiskon = getView().findViewById(R.id.transaction_dialog_total_num_text);
        txtHargaTotalSetelahDiskon.setText("RP "+(total+adminFee));

        Button cancel_button =  getView().findViewById(R.id.transaction_dialog_cancel_button);
        cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissAllowingStateLoss();
            }
        });


//        final EditText txtTime = getView().findViewById(R.id.transaction_dialog_time_text);
        final TextView txtDate = getView().findViewById(R.id.transaction__dialog_date_text);
        txtLocation = getView().findViewById(R.id.transaction_dialog_location_text);
        final EditText txtNote = getView().findViewById(R.id.transaction_dialog_note_text);


        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        if(true){

            String day = mDay /10 < 1? "0" + mDay : ""+ mDay;
            String month = mMonth /10 < 1? "0" + (mMonth + 1) : ""+ (mMonth + 1);
            txtDate.setText(day + "-" + (month)+ "-" + mYear);

            String hourTmp = hour /10 < 1? "0" + hour : ""+ hour;
            String minuteTmp = minute /10 < 1? "0" + minute : ""+ minute;
//            txtTime.setText( hourTmp + ":" + minuteTmp);
        }

        txtNote.setSelected(false);
//        txtTime.setFocusable(false);
        txtDate.setFocusable(false);
        txtLocation.setFocusable(false);

//        txtTime.setPaintFlags(txtTime.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);
//        txtDate.setPaintFlags(txtDate.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);
//        txtLocation.setPaintFlags(txtLocation.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);

//        txtTime.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                openTimePicker(txtTime);
//            }
//        });

        txtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDatePicker(txtDate);
            }
        });

//        txtLocation.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
//                Log.w(TAG, "===== onClick: " + "GooglePlayServicesRepairableException");
//                try {
//                    startActivityForResult(builder.build(getActivity()),PublicInt.PLACE_PICKER_REQUEST);
//                } catch (GooglePlayServicesRepairableException e) {
//                    Log.w(TAG, "===== onClick: " + "GooglePlayServicesRepairableException");
//                    e.printStackTrace();
//                } catch (GooglePlayServicesNotAvailableException e) {
//                    Log.w(TAG, "===== onClick: " + "GooglePlayServicesNotAvailableException");
//                    e.printStackTrace();
//                }
//            }
//        });




        Button submit_button =  getView().findViewById(R.id.transaction_dialog_submit_button);
        submit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String idForService = "";
                String qtyForService = "";
                String date = txtDate.getText().toString();
//                String time = txtTime.getText().toString();
                String locaion = txtLocation.getText().toString();
                String note = txtNote.getText().toString();



                for (TransactionModel tmp: transactionModels
                     ) {
                    idForService  = idForService  + tmp.getId_layanan() + ",";
                    qtyForService = qtyForService  + tmp.getKuantitas() + ",";
                }

                idForService = idForService.substring(0 , idForService.length()-1);
                qtyForService = qtyForService.substring(0 , qtyForService.length()-1);

                Log.w(TAG, "date for web service : " + date );

                ANRequest postRequestBuilder= AndroidNetworking.post(getString(R.string.web_get_submit_transac))
                        .addHeaders("key",getString(R.string.api_key))
                        .addBodyParameter("id_layanan" , idForService)
                        .addBodyParameter("kuantitas" , qtyForService)
                        .addBodyParameter("id_pelakon" , lakonModel.getId())
                        .addBodyParameter("id_member" , CurrentUserModel.getCurrentUserModel().getId())
                        .addBodyParameter("jam" , "00:00")
                        .addBodyParameter("tanggal" , date)
                        .addBodyParameter("keterangan" , note)
                        .addBodyParameter("alamat" , locaion)
                        .addBodyParameter("status_kesepakatan" , "pending")
                        .setPriority(Priority.MEDIUM)
                        .build();

                postRequestBuilder.getAsJSONObject(new JSONObjectRequestListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "onResponse: " + response.toString());

                        try{
                            Toast.makeText(getActivity(), "" + response.get("msg"), Toast.LENGTH_SHORT).show();
                            if(response.get("status").equals("failed")){
                                Log.w(TAG, "onResponse failed: "+ response );
                            } else if(response.get("status").equals("success")){
                                Log.w(TAG, "onResponse success: "+ response );
                                TransactionDialog.this.dismiss();
                                hargaFragment.addHarga(lakonModel.getId());
                            }
                        }catch (JSONException e){
                            Log.w(TAG, "onResponse error : "+ response );
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(TAG, "onResponse anerror: " + anError.getErrorBody() + anError.getErrorDetail());
                        Toast.makeText(getActivity(), "" + anError.getErrorBody() + anError.getErrorDetail(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }


    @SuppressLint("SetTextI18n")
    private void  openDatePicker(TextView txt){
        final TextView txtDate  = txt;
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        if(true){

            String day = mDay /10 < 1? "0" + mDay : ""+ mDay;
            String month = mMonth /10 < 1? "0" + (mMonth + 1) : ""+ (mMonth + 1);
            txtDate.setText(day + "-" + (month)+ "-" + mYear);
        }

        DatePickerDialog dpd = new DatePickerDialog(getContext(),
                new DatePickerDialog.OnDateSetListener() {
                    @SuppressLint("SetTextI18n")
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        String day = dayOfMonth /10 < 1? "0" + dayOfMonth : ""+ dayOfMonth;
                        String month = monthOfYear /10 < 1? "0" + (monthOfYear + 1) : ""+ (monthOfYear + 1);


                        txtDate.setText(day + "-" + (month)+ "-" + year);
                        storeYear = year;
                        storeMonth = monthOfYear;
                        storeDay = dayOfMonth;

                    }
                }, mYear, mMonth, mDay);
        dpd.show();

    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PublicInt.PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, getActivity());
                String toastMsg = String.format("Place: %s", place.getName());
                txtLocation.setText(place.getName().toString());
                Toast.makeText(getActivity(), toastMsg, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void  openTimePicker(TextView txt){
        final TextView txtTime  = txt;
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                String hour = selectedHour /10 < 1? "0" + selectedHour : ""+ selectedHour;
                String minute = selectedMinute /10 < 1? "0" + selectedMinute : ""+ selectedMinute;
                txtTime.setText( hour + ":" + minute);
                storeHour = selectedHour;
                storeMinute = selectedMinute;
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    public interface BottomSheetListener {
        void onButtonClicked(String text);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }
}