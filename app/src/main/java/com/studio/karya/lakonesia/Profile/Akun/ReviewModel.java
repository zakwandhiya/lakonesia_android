package com.studio.karya.lakonesia.Profile.Akun;

public class ReviewModel {
    private String
            nama_pengirim,
            foto_url,
            id,
            id_pelakon,
            id_member,
            rating,
            ulasan,
            created_at;

    public ReviewModel(String nama_pengirim, String foto_url, String id, String id_pelakon, String id_member, String rating, String ulasan, String created_at) {
        this.nama_pengirim = nama_pengirim;
        this.foto_url = foto_url;
        this.id = id;
        this.id_pelakon = id_pelakon;
        this.id_member = id_member;
        this.rating = rating;
        this.ulasan = ulasan;
        this.created_at = created_at;
    }

    public String getNama_pengirim() {
        return nama_pengirim;
    }

    public void setNama_pengirim(String nama_pengirim) {
        this.nama_pengirim = nama_pengirim;
    }

    public String getFoto_url() {
        return foto_url;
    }

    public void setFoto_url(String foto_url) {
        this.foto_url = foto_url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_pelakon() {
        return id_pelakon;
    }

    public void setId_pelakon(String id_pelakon) {
        this.id_pelakon = id_pelakon;
    }

    public String getId_member() {
        return id_member;
    }

    public void setId_member(String id_member) {
        this.id_member = id_member;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getUlasan() {
        return ulasan;
    }

    public void setUlasan(String ulasan) {
        this.ulasan = ulasan;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
