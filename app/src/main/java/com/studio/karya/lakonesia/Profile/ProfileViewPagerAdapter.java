package com.studio.karya.lakonesia.Profile;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.studio.karya.lakonesia.Intro.IntroModel;
import com.studio.karya.lakonesia.R;

import java.util.List;

public class ProfileViewPagerAdapter extends PagerAdapter {

   Context mContext ;
   List<IntroModel> mListScreen;

    public ProfileViewPagerAdapter(Context mContext, List<IntroModel> mListScreen) {
        this.mContext = mContext;
        this.mListScreen = mListScreen;
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layoutScreen = inflater.inflate(R.layout.intro_layout_screen,null);

        ImageView imgSlide = layoutScreen.findViewById(R.id.intro_img);
        ImageView imgIntroLogo = layoutScreen.findViewById(R.id.intro_logo);
        TextView description = layoutScreen.findViewById(R.id.intro_description);

        if (position == mListScreen.size()-1) {
            imgIntroLogo.setVisibility(View.VISIBLE);

        }

//        title.setText(mListScreen.get(position).getTitle());
        description.setText(mListScreen.get(position).getDescription());
        imgSlide.setImageResource(mListScreen.get(position).getScreenImg());

        container.addView(layoutScreen);

        return layoutScreen;





    }

    @Override
    public int getCount() {
        return mListScreen.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {

        container.removeView((View)object);

    }
}
