package com.studio.karya.lakonesia.Intro;

import android.content.Intent;

import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import com.pixplicity.easyprefs.library.Prefs;
import com.studio.karya.lakonesia.LoginActivity;
import com.studio.karya.lakonesia.R;
import com.studio.karya.lakonesia.Utils.PublicString;

import java.util.ArrayList;
import java.util.List;

public class IntroActivity extends AppCompatActivity {

    private ViewPager screenPager;
    IntroViewPagerAdapter introViewPagerAdapter ;
    TabLayout tabIndicator;
//    Button btnNext;
    int position = 0 ;
    Button btnGetStarted;
    Animation btnAnim ;
//    TextView tvSkip;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // make the activity on full screen

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);



        // when this activity is about to be launch we need to check if its openened before or not

        if (restorePrefData()) {

            Intent mainActivity = new Intent(getApplicationContext(),LoginActivity.class );
            startActivity(mainActivity);
            finish();


        }

        setContentView(R.layout.intro_activity);



        // ini views
//        btnNext = findViewById(R.id.btn_next);
        btnGetStarted = findViewById(R.id.btn_get_started);
        tabIndicator = findViewById(R.id.tab_indicator);
        btnAnim = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.button_animation);
//        tvSkip = findViewById(R.id.tv_skip);

        // fill list screen

        final List<IntroModel> mList = new ArrayList<>();
        mList.add(new IntroModel("Fresh Food","Temukan pengalaman baru menemukan pekerjaan yang sempurna sesuai keahlianmu bersama kami",R.drawable.wellcome1));
        mList.add(new IntroModel("Fast Delivery","Lakon membuka peluang seluas-luasnya bagi kamu yang ingin berkembang dan menjadi bagian dari sistem produktif lingkaran aktivitas perekonomian Indonesia dimasa sekarang dan yang akan datang",R.drawable.wellcome1));
        mList.add(new IntroModel("Easy Payment","Interaksi penyedia part time terhadap customer menggunakan feature permintaan dan penawaran secara cepat beserta harga yang mereka sepakati bersama",R.drawable.wellcome33));

        // setup viewpager
        screenPager =findViewById(R.id.screen_viewpager);
        introViewPagerAdapter = new IntroViewPagerAdapter(this,mList);
        screenPager.setAdapter(introViewPagerAdapter);

        // setup tablayout with viewpager

        tabIndicator.setupWithViewPager(screenPager);

        // next button click Listner

//        btnNext.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                position = screenPager.getCurrentItem();
//                if (position < mList.size()) {
//
//                    position++;
//                    screenPager.setCurrentItem(position);
//
//
//                }
//
//                if (position == mList.size()-1) { // when we rech to the last screen
//
//                    // TODO : show the GETSTARTED Button and hide the indicator and the next button
//
//                    loaddLastScreen();
//
//
//                }
//
//
//
//            }
//        });

        // tablayout add change listener


        tabIndicator.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                if (tab.getPosition() == mList.size()-1) {

                    loaddLastScreen();

                }


            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });



        // Get Started button click listener

        btnGetStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //open main activity

                savePrefsData();
                Intent mainActivity = new Intent(getApplicationContext(),LoginActivity.class);
                startActivity(mainActivity);
                // also we need to save a boolean value to storage so next time when the user run the app
                // we could know that he is already checked the intro screen activity
                // i'm going to use shared preferences to that process
                finish();



            }
        });

        // skip button click listener

//        tvSkip.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                screenPager.setCurrentItem(mList.size());
//            }
//        });



    }

    private boolean restorePrefData() {

        Boolean isIntroActivityOpnendBefore = Prefs.getBoolean(PublicString.IS_INTRO_OPENED, false);

//        SharedPreferences pref = getApplicationContext().getSharedPreferences("myPrefs",MODE_PRIVATE);
        return  isIntroActivityOpnendBefore;
    }

    private void savePrefsData() {
        Prefs.putBoolean(PublicString.IS_INTRO_OPENED, true);
//        SharedPreferences pref = getApplicationContext().getSharedPreferences("myPrefs",MODE_PRIVATE);
//        SharedPreferences.Editor editor = pref.edit();
//        editor.putBoolean("isIntroOpnend",true);
//        editor.commit();
    }

    // show the GETSTARTED Button and hide the indicator and the next button
    private void loaddLastScreen() {
        final View touchView = findViewById(R.id.screen_viewpager);
        touchView.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                return true;
            }
        });

//        btnNext.setVisibility(View.INVISIBLE);
        btnGetStarted.setVisibility(View.VISIBLE);
//        tvSkip.setVisibility(View.INVISIBLE);
        tabIndicator.setVisibility(View.INVISIBLE);
        // TODO : ADD an animation the getstarted button
        // setup animation
        btnGetStarted.setAnimation(btnAnim);




    }
}
