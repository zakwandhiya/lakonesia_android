package com.studio.karya.lakonesia.Notification;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.studio.karya.lakonesia.History.HitoryDetail.HistoryDetailFragment;
import com.studio.karya.lakonesia.MainActivity;
import com.studio.karya.lakonesia.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class NotificationRecycleViewAdapter extends RecyclerView.Adapter<NotificationRecycleViewAdapter.NotificationRecycleViewHolder>  {

    private static final String TAG = "===== Notif Adapter";


    private ArrayList<NotificationModel> dataList;
    private FragmentActivity activity;
    private NotificationListFragment notificationListFragment;

    public NotificationRecycleViewAdapter(ArrayList<NotificationModel> dataList, FragmentActivity activity ,NotificationListFragment notificationListFragment) {
        this.dataList = dataList;
        this.activity = activity;
        this.notificationListFragment = notificationListFragment;
    }

    public void addItem(ArrayList<NotificationModel> mData ) {
        this.dataList = mData;
        Log.w(TAG, "addItem size : "+ mData.size() + " " + this.dataList.size() );
        notifyDataSetChanged();
    }

    @Override
    public NotificationRecycleViewAdapter.NotificationRecycleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.fragment_notification_recycle_layout, parent, false);
        return new NotificationRecycleViewAdapter.NotificationRecycleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationRecycleViewAdapter.NotificationRecycleViewHolder holder,final int position) {
        Log.w(TAG, "onBindViewHolder notification : " + position );
        String mainString = "Silahkan periksa transaksi anda";

        if (dataList.get(position).getTipe_notifikasi().equals("11")) mainString = "Transaksi anda dengan " + dataList.get(position).getNama_pengirim() + " sudah disetujui";
        else if (dataList.get(position).getTipe_notifikasi().equals("10")) mainString = "Transaksi anda dengan " + dataList.get(position).getNama_pengirim() + " ditolak";
        else if (dataList.get(position).getTipe_notifikasi().equals("21")) mainString = "" + dataList.get(position).getNama_pengirim() + " memberikan nego balasan";

        holder.textMain.setText(mainString);
        holder.textMainRead.setText(mainString);

        String[] time = dataList.get(position).getWaktu_dibuat().split(" ");
        holder.textTime.setText(time[0] + "  " + time[1]);

        if(dataList.get(position).getSudah_dibaca().equals("1")){
            holder.textMain.setVisibility(View.GONE);
            holder.textMainRead.setVisibility(View.VISIBLE);
        }

        final int positionTmp = position;

        holder.layoutNotif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AndroidNetworking.post(activity.getString(R.string.web_read_notifikasi))
                        .addHeaders("key",activity.getString(R.string.api_key))
                        .addBodyParameter("id_notifikasi",dataList.get(position).getId_notifikasi())
                        .setPriority(Priority.MEDIUM)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.w(TAG, "onResponse: " + response.toString());
                                try{
                                    if(response.get("status").equals("failed")){
                                        Log.w(TAG, "onResponse failed: "+ response );
                                    } else if(response.get("status").equals("success")){
                                        Log.w(TAG, "onResponse success: "+ response );
                                    }
                                }catch (JSONException e){
                                    Log.w(TAG, "onResponse json error : " + e.toString());
                                }
                            }

                            @Override
                            public void onError(ANError anError) {
                                Log.w(TAG, "onResponse error: " + anError.getErrorBody() +  anError.getErrorDetail() + anError.getMessage());
                            }
                        });
                if(dataList.get(position).getSudah_dibaca().equals("0")){
                    dataList.get(position).setSudah_dibaca("1");
                    notificationListFragment.decreaseNotif();
                }
                notifyItemChanged(position);
                if(dataList.get(position).getTipe_notifikasi().equals("11") || dataList.get(position).getTipe_notifikasi().equals("10") || dataList.get(position).getTipe_notifikasi().equals("21") ){

                    ((MainActivity)activity).replaceFragment(2 , HistoryDetailFragment.newInstance(dataList.get(positionTmp).getId_transaksi()));
//                    HistoryDetailFragment historyDetailFragment=
//                    FragmentManager fragmentManager = activity.getSupportFragmentManager();
//                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                    fragmentTransaction.replace(R.id.fragment_notification_frame_layout , historyDetailFragment);
//                    fragmentTransaction.addToBackStack(null);
//                    fragmentTransaction.commit();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        Log.w(TAG, "getItemCount: " + ((dataList != null) ? dataList.size() : 0));
        return (dataList != null) ? dataList.size() : 0;
    }

    public class NotificationRecycleViewHolder extends RecyclerView.ViewHolder{
        private TextView textMain , textMainRead , textTime;
        private LinearLayout layoutNotif;
        private View view;

        public NotificationRecycleViewHolder(View itemView) {
            super(itemView);
            Log.w(TAG, "NotificationRecycleViewHolder" );
            view = itemView;
            textMainRead = itemView.findViewById(R.id.fragment_notification_recycle_view_main_text_read);
            textMain = itemView.findViewById(R.id.fragment_notification_recycle_view_main_text);
            textTime = itemView.findViewById(R.id.fragment_notification_recycle_view_time_text);
            layoutNotif = itemView.findViewById(R.id.fragment_notification_recycle_view_linear_layout);

        }
    }
}
