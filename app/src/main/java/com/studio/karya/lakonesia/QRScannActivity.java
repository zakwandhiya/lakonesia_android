package com.studio.karya.lakonesia;

import android.content.Context;
import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Vibrator;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.studio.karya.lakonesia.Transaction.TransactionActivity;
import com.studio.karya.lakonesia.Utils.PublicString;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

public class QRScannActivity extends AppCompatActivity {

    SurfaceView cameraPreview;
    TextView txtResult;
    BarcodeDetector barcodeDetector;
    CameraSource cameraSource;
    final int RequestCameraPermissionID = 1001;
    boolean scanned = false;

    private String TAG = "===== QR scan activity";

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case RequestCameraPermissionID: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    try {
                        cameraSource.start(cameraPreview.getHolder());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrscann);

        cameraPreview = findViewById(R.id.cameraPreview);
        txtResult = (TextView) findViewById(R.id.txtResult);
        txtResult.setText("scan qr code");

        barcodeDetector = new BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.QR_CODE)
                .build();
        cameraSource = new CameraSource
                .Builder(this, barcodeDetector)
//                .setRequestedPreviewSize(10000920, 10000920)
                .setRequestedPreviewSize(getResources().getDisplayMetrics().heightPixels, getResources().getDisplayMetrics().widthPixels)
                .setAutoFocusEnabled(true)
                .build();

        //Add Event
        cameraPreview.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder surfaceHolder) {
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    //Request permission
                    ActivityCompat.requestPermissions(QRScannActivity.this,
                            new String[]{Manifest.permission.CAMERA},RequestCameraPermissionID);
                    return;
                }
                try {
                    cameraSource.start(cameraPreview.getHolder());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
                cameraSource.stop();

            }
        });

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {

            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> qrcodes = detections.getDetectedItems();
                if(qrcodes.size() != 0)
                {
                    txtResult.post(new Runnable() {
                        @Override
                        public void run() {
                            //Create vibrate
                            if(!scanned){
                                Vibrator vibrator = (Vibrator)getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(10);
                                AndroidNetworking.post(getString(R.string.web_get_lakon_by_email))
                                        .addHeaders("key",getString(R.string.api_key))
                                        .addBodyParameter("email", qrcodes.valueAt(0).displayValue)
                                        .setPriority(Priority.MEDIUM)
                                        .build()
                                        .getAsJSONObject(new JSONObjectRequestListener() {
                                            @Override
                                            public void onResponse(JSONObject response) {
                                                Log.d(TAG, "onResponse: success" + response.toString());

                                                try{
                                                    if(response.get("status").equals("failed")){
                                                        Toast.makeText(QRScannActivity.this, "" + response.getString("msg"), Toast.LENGTH_SHORT).show();
                                                    } else if(response.get("status").equals("success")){
                                                        String tmp = response.getJSONArray("data").get(0).toString();
                                                        Log.w(TAG, "onResponse: "+ tmp );

                                                        if(response.getJSONArray("data").getJSONObject(0).getString("role").equals(PublicString.ROLE_LAKON)){

                                                            Intent intent = new Intent(QRScannActivity.this, TransactionActivity.class);
                                                            intent.putExtra(PublicString.TRANSACTION_LAKON_ID, response.getJSONArray("data").getJSONObject(0).getString("id"));
                                                            intent.putExtra(PublicString.TRANSACTION_LAKON_NAMA, response.getJSONArray("data").getJSONObject(0).getString("nama"));
                                                            intent.putExtra(PublicString.TRANSACTION_LAKON_EMAIL, response.getJSONArray("data").getJSONObject(0).getString("email"));
                                                            intent.putExtra(PublicString.TRANSACTION_LAKON_FOTO_URL, response.getJSONArray("data").getJSONObject(0).getString("foto_url"));
                                                            intent.putExtra(PublicString.TRANSACTION_LAKON_DESKRIPSI, response.getJSONArray("data").getJSONObject(0).getString("deskripsi"));

                                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                            finish();
                                                            startActivity(intent);
                                                        } else if(response.getJSONArray("data").getJSONObject(0).getString("role").equals(PublicString.ROLE_MEMBER)){

                                                            Toast.makeText(QRScannActivity.this, "" + "user member", Toast.LENGTH_SHORT).show();
                                                        }



                                                    }
                                                }catch (JSONException e){
                                                    Log.w(TAG, "onResponse json error : "+ e.getMessage() );
                                                    Toast.makeText(QRScannActivity.this, "Terjadi kesalahan pada server", Toast.LENGTH_SHORT).show();
                                                }
                                            }

                                            @Override
                                            public void onError(ANError anError) {
                                                Log.d("FAN", "onResponse: " + anError.toString() );
                                                Toast.makeText(QRScannActivity.this, "" + anError.getErrorDetail(), Toast.LENGTH_SHORT).show();
                                            }
                                        });

                            }
                            scanned = true;

                        }
                    });
                }
            }
        });
    }

    private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.05;
        double targetRatio = (double) w/h;

        if (sizes==null) return null;

        Camera.Size optimalSize = null;

        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        // Find size
        for (Camera.Size size : sizes) {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }

}
