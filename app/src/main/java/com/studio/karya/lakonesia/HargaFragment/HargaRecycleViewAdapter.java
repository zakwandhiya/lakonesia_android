package com.studio.karya.lakonesia.HargaFragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.studio.karya.lakonesia.Model.CurrentUserModel;
import com.studio.karya.lakonesia.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class HargaRecycleViewAdapter extends RecyclerView.Adapter<HargaRecycleViewAdapter.HargaRecycleViewHolder> {

    private ArrayList<HargaModel> dataList;
    private FragmentActivity activity;
    private Context context;
    private HargaFragment hargaFragment;

    private final String TAG = "===== Harga Adapter";

    public HargaRecycleViewAdapter(ArrayList<HargaModel> dataList , FragmentActivity activity , Context context , HargaFragment hargaFragment) {
        this.dataList = dataList;
        this.activity =  activity;
        this.context = context;
        this.hargaFragment = hargaFragment;
    }

    public void addItem(ArrayList<HargaModel> mData , FragmentActivity activity , Context context , HargaFragment hargaFragment) {
        this.dataList = mData;
        this.activity =  activity;
        this.context = context;
        this.hargaFragment = hargaFragment;
        notifyDataSetChanged();
    }

    boolean showDeleteDialog(String id){
        final Dialog dialog = new Dialog(activity);

        dialog.setContentView(R.layout.fragment_profile_harga_custom_dialog_for_delete_layanan);
        dialog.setTitle("DELETE LAYANAN");
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final RelativeLayout restOfDialog = (RelativeLayout) dialog.findViewById(R.id.profile_harga_delete_main_relative_layout);
        final ProgressBar dialogProgressBar = (ProgressBar) dialog.findViewById(R.id.profile_harga_delete_progress_bar);

        final String id_tmp = id;
        dialogProgressBar.setVisibility(View.INVISIBLE);

        Button dialogButton = (Button) dialog.findViewById(R.id.profile_harga_delete_submit_button);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                restOfDialog.setVisibility(View.INVISIBLE);
                dialogProgressBar.setVisibility(View.VISIBLE);


                    deleteLayanan(dialog , id_tmp);
            }
        });

        Button dialogDismissButton = (Button) dialog.findViewById(R.id.profile_harga_delete_cancel_button);
        dialogDismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
        return true;

    }

    boolean showEditDialog(String id , String nama , String harga ,String harga_promo, String deskripsi){
        final Dialog dialog = new Dialog(activity);
        dialog.setContentView(R.layout.fragment_profile_harga_custom_dialog_for_edit_layanan);
        dialog.setTitle("EDIT LAYANAN");
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final EditText etNama = (EditText) dialog.findViewById(R.id.profile_harga_edit_nama_layanan);
        final EditText etHarga = (EditText) dialog.findViewById(R.id.profile_harga_edit_harga_layanan);
        final EditText etHargaPromo = (EditText) dialog.findViewById(R.id.profile_harga_edit_harga_promo_layanan);
        final EditText etDeskripsi = (EditText) dialog.findViewById(R.id.profile_harga_edit_deskripsi_layanan);
        final RelativeLayout restOfDialog = (RelativeLayout) dialog.findViewById(R.id.profile_harga_edit_main_relative_layout);
        final ProgressBar dialogProgressBar = (ProgressBar) dialog.findViewById(R.id.profile_harga_edit_progress_bar);

        etNama.setText(nama);
        etHarga.setText(harga);
        etDeskripsi.setText(deskripsi);
        etHargaPromo.setText(harga_promo);


        final String id_tmp = id;
        dialogProgressBar.setVisibility(View.INVISIBLE);

        Button dialogButton = (Button) dialog.findViewById(R.id.profile_harga_edit_submit_button);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                restOfDialog.setVisibility(View.INVISIBLE);
                dialogProgressBar.setVisibility(View.VISIBLE);

                String namaForEdit = etNama.getText().toString();
                String hargaForEdit = etHarga.getText().toString();
                String hargaPromoForEdit = etHargaPromo.getText().toString();
                String deskripsiForEdit = etDeskripsi.getText().toString();

                if(namaForEdit.isEmpty() || hargaForEdit.isEmpty() || deskripsiForEdit.isEmpty()){
                    Toast.makeText(context , "lengkapi form terlebih dahulu" , Toast.LENGTH_SHORT);

                    restOfDialog.setVisibility(View.VISIBLE);
                    dialogProgressBar.setVisibility(View.INVISIBLE);
                } else {
                    submitLayanan(dialog , id_tmp ,namaForEdit , hargaForEdit ,hargaPromoForEdit, deskripsiForEdit);
                }
            }
        });

        Button dialogDismissButton = (Button) dialog.findViewById(R.id.profile_harga_edit_cancel_button);
        dialogDismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
        return true;

    }

    boolean deleteLayanan(final Dialog dialog ,String id){
        AndroidNetworking.post(activity.getString(R.string.web_delete_layanan))
                .addHeaders("key",activity.getString(R.string.api_key))
                .addBodyParameter("id_layanan", id)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.w(TAG , "onResponse: success" + response.toString());

                        try{
                            if(response.get("status").equals("failed")){
                                Toast.makeText(context , "layanan gagal dihapus" , Toast.LENGTH_SHORT);
                                dialog.dismiss();
                            } else if(response.get("status").equals("success")){
                                Toast.makeText(context , "layanan berhasil dihapus" , Toast.LENGTH_SHORT);
                                hargaFragment.addHarga(CurrentUserModel.getCurrentUserModel().getId());
                                dialog.dismiss();
                            }
                        }catch (JSONException e){
                            Log.w(TAG, "onResponse: json error" + e.toString() );
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(TAG, "onResponse: fanerror " + anError.toString() );
                        dialog.dismiss();
                    }
                });
        return true;
    }


    @Override
    public int getItemViewType(int position) {
        if (dataList.get(position) instanceof HargaModelMain) {
            return 10;
        } else if (dataList.get(position) instanceof HargaModelForButton) {
            return 20;
        }
        return 10;
    }

    boolean submitLayanan(final Dialog dialog ,String id, String nama , String harga ,String harga_promo, String deskripsi){
        AndroidNetworking.post(activity.getString(R.string.web_update_layanan))
                .addHeaders("key",activity.getString(R.string.api_key))
                .addBodyParameter("id_layanan", id)
                .addBodyParameter("nama_layanan", nama)
                .addBodyParameter("deskripsi", deskripsi)
                .addBodyParameter("harga", harga)
                .addBodyParameter("harga_promo", harga_promo)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "onResponse: success" + response.toString());

                        try{
                            if(response.get("status").equals("failed")){
                                Toast.makeText(context , "pembaharuan layanan gagal" , Toast.LENGTH_SHORT);
                                dialog.dismiss();
                            } else if(response.get("status").equals("success")){
                                Toast.makeText(context , "pembaharuan layanan berhasil" , Toast.LENGTH_SHORT);
                                hargaFragment.addHarga(CurrentUserModel.getCurrentUserModel().getId());
                                dialog.dismiss();
                            }
                        }catch (JSONException e){
                            Log.w(TAG, "onResponse: json error" + e.toString() );
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(TAG, "onResponse fanerror : " + anError.toString() );
                        dialog.dismiss();
                    }
                });
        return true;
    }

    @Override
    public HargaRecycleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
//        View view = layoutInflater.inflate(R.layout.fragment_profile_harga_layout, parent, false);
//        return new HargaRecycleViewHolder(view);

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view;
        view = layoutInflater.inflate(R.layout.fragment_profile_harga_layout, parent, false);
        switch (viewType) {
            case 10:
                view = layoutInflater.inflate(R.layout.fragment_profile_harga_layout, parent, false);
                break;
            case 20:
                view = layoutInflater.inflate(R.layout.fragment_profile_harga_button, parent, false);
                break;
        }
        return new HargaRecycleViewHolder(view);
    }


    @Override
    public void onBindViewHolder(HargaRecycleViewHolder holder,final int position) {
        if(dataList.get(position) instanceof HargaModelMain){
            holder.txtNama.setText(dataList.get(position).getNama_layanan());
            holder.txtHarga.setText("RP " + dataList.get(position).getHarga());
            final String id_tmp = dataList.get(position).getId_layanan();
            final String nama_tmp = dataList.get(position).getNama_layanan();
            final String harga_tmp = dataList.get(position).getHarga();
            final String harga_promo_tmp = dataList.get(position).getHarga_promo();
            final String deskripsi_tmp = dataList.get(position).getDeskripsi();

            if(dataList.get(position).getStatus_promo().equals("1")){
                holder.txtHarga.setText("RP " + dataList.get(position).getHarga_promo());

            }else{
                holder.txtHarga.setText("RP " + dataList.get(position).getHarga());
            }

            if(dataList.get(position).isExpanded()){
                holder.descLayout.setVisibility(View.VISIBLE);
                TextView descText =  holder.view.findViewById(R.id.profile_harga_deskripsi_layanan);
                descText.setText(dataList.get(position).getDeskripsi());
            } else{
                holder.descLayout.setVisibility(View.GONE);
            }

            holder.btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showEditDialog(id_tmp , nama_tmp , harga_tmp , harga_promo_tmp,deskripsi_tmp);
                }
            });

            holder.btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDeleteDialog(id_tmp );
                }
            });

            holder.recycleLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean expanded = dataList.get(position).isExpanded();
                    dataList.get(position).setExpanded(!expanded);
                    notifyItemChanged(position);
                }
            });
        }else if (dataList.get(position) instanceof HargaModelForButton){
            holder.btnTambahService.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    hargaFragment.showEditDialog();
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class HargaRecycleViewHolder extends RecyclerView.ViewHolder{
        private TextView txtNama, txtHarga, txtNoHp;
        private ImageView btnEdit , btnDelete;
        private Button btnTambahService;
        private LinearLayout recycleLayout , descLayout;
        private View view;


        public HargaRecycleViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            txtHarga = (TextView) itemView.findViewById(R.id.profile_harga_layanan);
            txtNama =  (TextView) itemView.findViewById(R.id.profile_nama_layanan);
            btnEdit =  (ImageView) itemView.findViewById(R.id.profile_harga_update_layanan);
            btnDelete =  (ImageView) itemView.findViewById(R.id.profile_harga_delete_layanan);
            btnTambahService = itemView.findViewById(R.id.profile_harga_dynamic_button);
            recycleLayout = itemView.findViewById(R.id.fragment_profile_recycle_layout);
            descLayout = itemView.findViewById(R.id.fragment_profile_recycle_descripsion_layout);
        }
    }
}
