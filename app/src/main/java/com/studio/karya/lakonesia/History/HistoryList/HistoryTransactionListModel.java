package com.studio.karya.lakonesia.History.HistoryList;

import java.util.ArrayList;

public class HistoryTransactionListModel {
    private String id_transaksi , nama_lakon , nama_member ,status , note , alamat , time , date , price , admin_fee , total , payment , id_lakon , id_member;
    private ArrayList<HistoryTransactionLayananListModel> list_layanan;
    public HistoryTransactionListModel(String id_transaksi, String nama_lakon, String nama_member, String status, String note, String alamat, String time, String date, String price, String admin_fee, String total, String payment , String id_lakon , String id_member, ArrayList<HistoryTransactionLayananListModel> list_layanan) {
        this.id_transaksi = id_transaksi;
        this.nama_lakon = nama_lakon;
        this.nama_member = nama_member;
        this.status = status;
        this.note = note;
        this.alamat = alamat;
        this.time = time;
        this.date = date;
        this.price = price;
        this.admin_fee = admin_fee;
        this.total = total;
        this.payment = payment;
        this.id_lakon = id_lakon;
        this.id_member = id_member;
        this.list_layanan = list_layanan;
    }

    public String getId_lakon() {
        return id_lakon;
    }

    public void setId_lakon(String id_lakon) {
        this.id_lakon = id_lakon;
    }

    public String getId_member() {
        return id_member;
    }

    public void setId_member(String id_member) {
        this.id_member = id_member;
    }

    public String getId_transaksi() {
        return id_transaksi;
    }

    public void setId_transaksi(String id_transaksi) {
        this.id_transaksi = id_transaksi;
    }

    public String getNama_lakon() {
        return nama_lakon;
    }

    public void setNama_lakon(String nama_lakon) {
        this.nama_lakon = nama_lakon;
    }

    public String getNama_member() {
        return nama_member;
    }

    public void setNama_member(String nama_member) {
        this.nama_member = nama_member;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAdmin_fee() {
        return admin_fee;
    }

    public void setAdmin_fee(String admin_fee) {
        this.admin_fee = admin_fee;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public ArrayList<HistoryTransactionLayananListModel> getList_layanan() {
        return list_layanan;
    }

    public void setList_layanan(ArrayList<HistoryTransactionLayananListModel> list_layanan) {
        this.list_layanan = list_layanan;
    }
}
