package com.studio.karya.lakonesia.Dashboard;

import androidx.recyclerview.widget.RecyclerView;

import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.studio.karya.lakonesia.MainActivity;
import com.studio.karya.lakonesia.Model.KategoriModel;
import com.studio.karya.lakonesia.R;
import com.studio.karya.lakonesia.SearchFragment;

import java.util.ArrayList;


public class KategoriRecycleViewAdapter extends RecyclerView.Adapter<KategoriRecycleViewAdapter.KategoriRecycleViewHolder> {

    private ArrayList<KategoriModel> dataList;
    private MainActivity mainActivity;

    public KategoriRecycleViewAdapter(ArrayList<KategoriModel> dataList , MainActivity mainActivity) {
        this.dataList = dataList;
        this.mainActivity = mainActivity;
    }

    public void addItem(ArrayList<KategoriModel> mData) {
        this.dataList = mData;
        notifyDataSetChanged();
    }

    @Override
    public KategoriRecycleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.fragment_dashboard_kategori_layout, parent, false);
        return new KategoriRecycleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(KategoriRecycleViewHolder holder, int position) {
        final int positionIndex = position;
        holder.txtNama.setText(dataList.get(position).getNama());

        Picasso.get().load(dataList.get(position).getFoto_url()).fit().centerCrop().into(holder.ivKategoriIcon);

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.replaceFragment(1 ,SearchFragment.newInstance(dataList.get(positionIndex).getNama()));

            }
        });
//        holder.txtNpm.setText(dataList.get(position).getNpm());
//        holder.txtNoHp.setText(dataList.get(position).getNohp());
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class KategoriRecycleViewHolder extends RecyclerView.ViewHolder{
        private TextView txtNama, txtNpm, txtNoHp;
        private ImageView ivKategoriIcon;
        private View view;

        public KategoriRecycleViewHolder(View itemView) {
            super(itemView);
            view  = itemView;
            txtNama = (TextView) itemView.findViewById(R.id.dashboard_kategori_title);
            ivKategoriIcon = (ImageView) itemView.findViewById(R.id.dashboard_kategori_img);
//            txtNpm = (TextView) itemView.findViewById(R.id.txt_npm_mahasiswa);
//            txtNoHp = (TextView) itemView.findViewById(R.id.txt_nohp_mahasiswa);
        }
    }
}
