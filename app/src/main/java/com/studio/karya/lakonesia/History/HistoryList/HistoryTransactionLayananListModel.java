package com.studio.karya.lakonesia.History.HistoryList;

public class HistoryTransactionLayananListModel {
    private String nama_layanan , kuantitas , harga;

    public HistoryTransactionLayananListModel(String nama_layanan, String kuantitas, String harga) {
        this.nama_layanan = nama_layanan;
        this.kuantitas = kuantitas;
        this.harga = harga;
    }

    public String getNama_layanan() {
        return nama_layanan;
    }

    public void setNama_layanan(String nama_layanan) {
        this.nama_layanan = nama_layanan;
    }

    public String getKuantitas() {
        return kuantitas;
    }

    public void setKuantitas(String kuantitas) {
        this.kuantitas = kuantitas;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }
}
