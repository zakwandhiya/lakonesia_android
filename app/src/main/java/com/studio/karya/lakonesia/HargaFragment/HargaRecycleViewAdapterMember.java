package com.studio.karya.lakonesia.HargaFragment;

import android.content.Context;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.studio.karya.lakonesia.Model.LakonModel;
import com.studio.karya.lakonesia.R;
import com.studio.karya.lakonesia.Transaction.TransactionDialog;
import com.studio.karya.lakonesia.Transaction.TransactionModel;

import java.util.ArrayList;


public class HargaRecycleViewAdapterMember extends RecyclerView.Adapter<HargaRecycleViewAdapterMember.HargaRecycleViewHolder> {

    private ArrayList<HargaModel> dataList;
    private FragmentActivity activity;
    private Context context;
    private HargaFragment hargaFragment;

    private String TAG = "===== Harga Adapter";

    public ArrayList<TransactionModel> getTransactionModels(){
        ArrayList<TransactionModel> transactionModels = new ArrayList<>();
        for(HargaModel entry : dataList ) {
            if(entry.getQuantity() > 0){
                transactionModels.add(new TransactionModel(
                        entry.getId_layanan(),
                        entry.getId_pelakon(),
                        entry.getNama_layanan(),
                        entry.getDeskripsi(),
                        entry.getHarga(),
                        entry.getStatus_promo(),
                        entry.getHarga_promo(),
                        entry.getQuantity())
                );
            }
        }

        return transactionModels;
    }

    public int getSum(){
        int sum = 0;
        for(HargaModel entry : dataList ) {
            sum += entry.getQuantity();
        }
        return sum;
    }

    public HargaRecycleViewAdapterMember(ArrayList<HargaModel> dataList , FragmentActivity activity , Context context , HargaFragment hargaFragment) {
        this.dataList = dataList;
        this.activity =  activity;
        this.context = context;
        this.hargaFragment = hargaFragment;
    }

    public void addItem(ArrayList<HargaModel> mData , FragmentActivity activity , Context context , HargaFragment hargaFragment) {
        this.dataList = mData;
        this.activity =  activity;
        this.context = context;
        this.hargaFragment = hargaFragment;
//        this.itemHolder = new HashMap<String , TransactionModel>();
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (dataList.get(position) instanceof HargaModelMain) {
            return 10;
        } else if (dataList.get(position) instanceof HargaModelForButton) {
            return 20;
        } else if (dataList.get(position) instanceof HargaModelForPromoText) {
            return 30;
        } else if (dataList.get(position) instanceof HargaModelForNormalText) {
            return 40;
        }
        return 10;
    }

    @Override
    public HargaRecycleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view;
        view = layoutInflater.inflate(R.layout.fragment_profile_harga_for_transact_layout, parent, false);
        switch (viewType) {
            case 10:
                view = layoutInflater.inflate(R.layout.fragment_profile_harga_for_transact_layout, parent, false);
                break;
            case 20:
                view = layoutInflater.inflate(R.layout.fragment_profile_harga_button, parent, false);
                break;
            case 30:
                view = layoutInflater.inflate(R.layout.fragment_profile_harga_promo_text, parent, false);
                break;
            case 40:
                view = layoutInflater.inflate(R.layout.fragment_profile_harga_normal_text, parent, false);
                break;
        }
        return new HargaRecycleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final HargaRecycleViewHolder holder, final int position) {
        Log.w(TAG, "onBindViewHolder: " + dataList.get(position).getNama_layanan());
//        itemHolder = new HashMap<String , TransactionModel>();

        if(dataList.get(position) instanceof HargaModelMain){
            holder.txtKuantitas.setText(""+dataList.get(position).getQuantity());
            final HargaModel tmpHargaModel = dataList.get(position);
            final String id_tmp = tmpHargaModel.getId_layanan();
            final String nama_tmp = tmpHargaModel.getNama_layanan();
            final String harga_tmp = tmpHargaModel.getHarga();
            final String deskripsi_tmp = tmpHargaModel.getDeskripsi();


            if(dataList.get(position).getStatus_promo().equals("1")){
                holder.txtHarga.setText("RP " + dataList.get(position).getHarga_promo());

            }else{
                holder.txtHarga.setText("RP " + dataList.get(position).getHarga());
            }

            if(dataList.get(position).isExpanded()){
                holder.descLayout.setVisibility(View.VISIBLE);
                TextView descText =  holder.view.findViewById(R.id.profile_harga_deskripsi_layanan);
                descText.setText(dataList.get(position).getDeskripsi());
            } else{
                holder.descLayout.setVisibility(View.GONE);
            }

            holder.txtNama.setText(dataList.get(position).getNama_layanan());
            holder.txtHarga.setText("RP " + dataList.get(position).getHarga());
            holder.btnTambah.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dataList.get(position).tambahQty();
                    holder.txtKuantitas.setText(""+dataList.get(position).getQuantity());
                }
            });

            holder.btnKurang.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dataList.get(position).kurangQty();
                    holder.txtKuantitas.setText(""+dataList.get(position).getQuantity());
                }
            });

            holder.recycleLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean expanded = dataList.get(position).isExpanded();
                    dataList.get(position).setExpanded(!expanded);
                    notifyItemChanged(position);
                }
            });

        } else if (dataList.get(position) instanceof HargaModelForButton){

            Log.w(TAG, "onClick dynamic button: " + hargaFragment.mEmail );
            holder.btn_dynamic_fragment_harga.setText(" CHECKOUT ");
            holder.btn_dynamic_fragment_harga.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (hargaFragment.adapterHargaMember.getSum() > 0){
                        TransactionDialog bottomSheet = TransactionDialog.newInstance(
                                hargaFragment. adapterHargaMember.getTransactionModels(),
                                new LakonModel(hargaFragment.mId , hargaFragment.mNama , hargaFragment.mEmail , hargaFragment.mRole ,  hargaFragment.mFoto_url),
                                hargaFragment
                        );
                        bottomSheet.show(activity.getSupportFragmentManager(), "Dari Harga Fregment");

                    } else {
                        Toast.makeText(context , "anda tidak memilih layanan apapun" ,Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class HargaRecycleViewHolder extends RecyclerView.ViewHolder{
        private TextView txtNama, txtHarga, txtKuantitas;
        private ImageView btnTambah , btnKurang;
        private LinearLayout recycleLayout , descLayout;
        private Button btn_dynamic_fragment_harga ;
        private View view;

        public HargaRecycleViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            btn_dynamic_fragment_harga = itemView.findViewById(R.id.profile_harga_dynamic_button);
            txtHarga =  itemView.findViewById(R.id.profile_harga_layanan);
            txtNama =  itemView.findViewById(R.id.profile_nama_layanan);
            txtKuantitas = itemView.findViewById(R.id.fragment_profile_harga_for_transact_text);
            btnTambah =  itemView.findViewById(R.id.profile_harga_tambah_kuantitas);
            btnKurang =  itemView.findViewById(R.id.profile_harga_kurangi_kuantitas);
            recycleLayout = itemView.findViewById(R.id.fragment_profile_recycle_layout);
            descLayout = itemView.findViewById(R.id.fragment_profile_recycle_descripsion_layout);
        }
    }
}
