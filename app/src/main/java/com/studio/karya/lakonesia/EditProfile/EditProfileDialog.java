package com.studio.karya.lakonesia.EditProfile;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import androidx.fragment.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.studio.karya.lakonesia.Model.CurrentUserModel;
import com.studio.karya.lakonesia.R;

import org.json.JSONException;
import org.json.JSONObject;


public class EditProfileDialog extends BottomSheetDialogFragment {
    private BottomSheetListener mListener;


    private String TAG = "===== Edit Dialog";

    private String mId , mKolom , mNilai;
    private EditCallback mEditCallback;

    public EditProfileDialog() {
    }

    public static EditProfileDialog newInstance(String mId , String kolom , String nilai , EditCallback editCallback) {
        EditProfileDialog transactionDialog = new EditProfileDialog();
        transactionDialog.mId = mId;
        transactionDialog.mKolom = kolom;
        transactionDialog.mNilai = nilai;
        transactionDialog.mEditCallback = editCallback;
        return transactionDialog;

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_edit_profile_dynamic_dialog_layout, container, false);
        return v;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        TextView editLabel = getView().findViewById(R.id.edit_profile_dialog_label_text);
        if(mKolom.equals(EditColumn.EDIT_DESKRIPSI)){
            editLabel.setText("Masukan deskripsi lakon anda");
        } else if(mKolom.equals(EditColumn.EDIT_NAMA)) {
            editLabel.setText("Masukan nama anda");
        }

        final TextView inputText = getView().findViewById(R.id.edit_profile_dialog_input);
        inputText.setText(mNilai);

        TextView dismiss = getView().findViewById(R.id.edit_profile_dialog_dismiss_button);
        dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        TextView submit = getView().findViewById(R.id.edit_profile_dialog_submit_button);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleUpdateProfile(mKolom , inputText.getText().toString());
            }
        });
    }

    private void handleUpdateProfile(final String kolom , final String nilai){
        AndroidNetworking.post(getString(R.string.web_update_akun))
                .addHeaders("key" , getString(R.string.api_key))
                .addBodyParameter("id" , mId)
                .addBodyParameter("kolom" , kolom)
                .addBodyParameter("nilai" , nilai)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG , "onResponse: success " + response.toString());

                        try{
                            if(response.get("status").equals("failed")){
                                Log.w(TAG, "onResponse: " + "user unregistererd" );
                                Toast.makeText(getActivity(), "update profile gagal", Toast.LENGTH_SHORT).show();
                                dismiss();
                            } else if(response.get("status").equals("success")){
                                Log.w(TAG, "onResponse: success" + response.toString() );
                                Toast.makeText(getActivity(), "profile berhasil diperbaharui", Toast.LENGTH_SHORT).show();

                                if(mKolom.equals(EditColumn.EDIT_DESKRIPSI)){
                                    CurrentUserModel.getCurrentUserModel().setDeskripsi(nilai);
                                } else if(mKolom.equals(EditColumn.EDIT_NAMA)) {
                                    CurrentUserModel.getCurrentUserModel().setNama(nilai);
                                }
                                mEditCallback.callbackMethod(true);
                                dismiss();
                            }
                        }catch (JSONException e){
                            Log.w(TAG, "onResponse json error : " + e.toString() );
                            dismiss();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(TAG, "onResponse an error: " + anError.getErrorDetail());
                        Toast.makeText(getActivity(), "" + anError.getErrorDetail(), Toast.LENGTH_SHORT).show();
                        dismiss();
                    }
                });
    }



    public interface BottomSheetListener {
        void onButtonClicked(String text);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogStyle);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }
}

