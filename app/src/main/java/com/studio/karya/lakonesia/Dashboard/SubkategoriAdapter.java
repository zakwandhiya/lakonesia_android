package com.studio.karya.lakonesia.Dashboard;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.studio.karya.lakonesia.MainActivity;
import com.studio.karya.lakonesia.Model.GaleryModel;
import com.studio.karya.lakonesia.R;
import com.studio.karya.lakonesia.SearchFragment;

import java.util.ArrayList;

public class SubkategoriAdapter extends RecyclerView.Adapter<SubkategoriAdapter.GridViewHolder> {
        private ArrayList<GaleryModel> dataList;

        private MainActivity mainActivity;

        public SubkategoriAdapter(ArrayList<GaleryModel> list , MainActivity mainActivity) {
            this.dataList = list;
            this.mainActivity = mainActivity;
        }

        @NonNull
        @Override
        public SubkategoriAdapter.GridViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fragment_dashboard_subcategory_layout, viewGroup, false);
            return new SubkategoriAdapter.GridViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final SubkategoriAdapter.GridViewHolder holder, final int position) {
            holder.button.setText(dataList.get(position).getFotoUrl());

            holder.button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mainActivity.replaceFragment(1 , SearchFragment.newInstance(dataList.get(position).getFotoUrl()));
                }
            });
        }


        @Override
        public int getItemCount() {
            return dataList.size();
        }

        class GridViewHolder extends RecyclerView.ViewHolder {
            Button button;

            GridViewHolder(View itemView) {
                super(itemView);
                button = itemView.findViewById(R.id.fragment_dashboard_subcategory);
            }
        }
    }
