package com.studio.karya.lakonesia.Model;

public class LakonModel {
    private String id, nama,email ,  role , foto_url;

    public LakonModel(String id, String nama, String email, String role, String foto_url) {
        this.id = id;
        this.nama = nama;
        this.email = email;
        this.role = role;
        this.foto_url = foto_url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getFoto_url() {
        return foto_url;
    }

    public void setFoto_url(String foto_url) {
        this.foto_url = foto_url;
    }
}
