package com.studio.karya.lakonesia.Dashboard;

import android.app.Activity;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.studio.karya.lakonesia.R;
import com.studio.karya.lakonesia.Transaction.TransactionActivity;
import com.studio.karya.lakonesia.Utils.PublicString;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

public class LakonRecycleViewAdapter extends RecyclerView.Adapter<LakonRecycleViewAdapter.LakonForRecycleViewViewHolder> {

    private ArrayList<LakonRecycleViewModel> dataList;
    private Activity activity;

    private String TAG = "==================== Lakon View Holder";

    public LakonRecycleViewAdapter(ArrayList<LakonRecycleViewModel> dataList , Activity activity) {
        this.dataList = dataList;
        this.activity = activity;
    }

    public void addItem(ArrayList<LakonRecycleViewModel> mData) {
        this.dataList = mData;
        notifyDataSetChanged();
    }

    @Override
    public LakonForRecycleViewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.fragment_dashboard_service_layout, parent, false);
        return new LakonForRecycleViewViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LakonForRecycleViewViewHolder holder, int position) {
        holder.txtNamaLayanan.setText(dataList.get(position).getNama_layanan());
        DecimalFormat df2 = new DecimalFormat("#.#");
        holder.txtRatingUser.setText(  df2.format(Double.valueOf(dataList.get(position).getRating_user())));

        NumberFormat formatter = new DecimalFormat("#,###");
        double hargaPromo = Double.parseDouble(dataList.get(position).getHarga_promo());
        double hargaLayanan = Double.parseDouble(dataList.get(position).getHarga_layanan());
        String txtHargaPromo = formatter.format(hargaPromo);
        String txtHargaLayanan = formatter.format(hargaLayanan);

//        holder.txtKategori.setText(dataList.get(position).getNama_kategori());

        if(dataList.get(position).getStatus_promo().equals("1")){
            holder.txtHargaLayanan.setText("RP " + txtHargaPromo);
            holder.hargaNormalViewgroup.setVisibility(View.VISIBLE);
            holder.txtHargaNormal.setVisibility(View.VISIBLE);
            holder.txtHargaNormal.setText("RP " + txtHargaLayanan);
            holder.txtHargaNormal.setPaintFlags(holder.txtHargaNormal.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }else{
            holder.txtHargaLayanan.setText("RP " + txtHargaLayanan);
        }

        holder.txtNamaLakon.setText(dataList.get(position).getNama());

        if(!dataList.get(position).getFoto_url().isEmpty()){
            Picasso.get().load(dataList.get(position).getFoto_url()).fit().centerCrop().into(holder.ivLakonProfile);
        }
        final int position_tmp = position;

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity, TransactionActivity.class);
                intent.putExtra(PublicString.TRANSACTION_LAKON_ID, dataList.get(position_tmp).getId_pelakon());
                Log.w(TAG, "onClick id pelakon: "+  dataList.get(position_tmp).getId_pelakon());
                intent.putExtra(PublicString.TRANSACTION_LAKON_NAMA, dataList.get(position_tmp).getNama());
                intent.putExtra(PublicString.TRANSACTION_LAKON_EMAIL, dataList.get(position_tmp).getEmail());
                intent.putExtra(PublicString.TRANSACTION_LAKON_FOTO_URL, dataList.get(position_tmp).getFoto_url());
                intent.putExtra(PublicString.TRANSACTION_LAKON_DESKRIPSI, dataList.get(position_tmp).getDeskripsi());
                activity.startActivity(intent);
            }
        });
//        holder.txtNpm.setText(dataList.get(position).getNpm());
//        holder.txtNoHp.setText(dataList.get(position).getNohp());
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class LakonForRecycleViewViewHolder extends RecyclerView.ViewHolder{
        private LinearLayout container;
        private TextView txtNamaLayanan, txtNpm, txtNoHp , txtHargaLayanan , txtNamaLakon , txtHargaNormal , txtKategori, txtRatingUser;
        private ImageView ivLakonProfile;
        private LinearLayout hargaNormalViewgroup;

        public LakonForRecycleViewViewHolder(View itemView) {
            super(itemView);
            txtNamaLayanan = (TextView) itemView.findViewById(R.id.txt_nama_lakon);
            txtHargaLayanan = (TextView) itemView.findViewById(R.id.dashboard_harga_service);
            txtNamaLakon = (TextView) itemView.findViewById(R.id.dashboard_nama_lakon);
            ivLakonProfile = (ImageView) itemView.findViewById(R.id.lakon_foto_profile);
            container = (LinearLayout) itemView.findViewById(R.id.dashboard_service_container);
            hargaNormalViewgroup = itemView.findViewById(R.id.dashboard_harga_normal_viewgroup);
            txtHargaNormal = itemView.findViewById(R.id.dashboard_harga_normal);
//            txtKategori = itemView.findViewById(R.id.dashboard_layanan_kategori_text);
            txtRatingUser = itemView.findViewById(R.id.dashboard_rating_user_text);
            //txtNpm = (TextView) itemView.findViewById(R.id.txt_npm_mahasiswa);
            //txtNoHp = (TextView) itemView.findViewById(R.id.txt_nohp_mahasiswa);
        }
    }
}

