package com.studio.karya.lakonesia.Notification;

public class NotificationModel {
    private String id_notifikasi , id_penerima , id_pengirim , nama_penerima , nama_pengirim , id_transaksi , teks_notifikasi , tipe_notifikasi , sudah_dibaca , waktu_dibuat , waktu_dimodifikasi;

    public NotificationModel(String id_notifikasi, String id_penerima, String id_pengirim, String nama_penerima, String nama_pengirim, String teks_notifikasi, String tipe_notifikasi, String sudah_dibaca, String waktu_dibuat, String waktu_dimodifikasi) {
        this.id_notifikasi = id_notifikasi;
        this.id_penerima = id_penerima;
        this.id_pengirim = id_pengirim;
        this.nama_penerima = nama_penerima;
        this.nama_pengirim = nama_pengirim;
        this.teks_notifikasi = teks_notifikasi;
        this.tipe_notifikasi = tipe_notifikasi;
        this.sudah_dibaca = sudah_dibaca;
        this.waktu_dibuat = waktu_dibuat;
        this.waktu_dimodifikasi = waktu_dimodifikasi;
    }

    public String getWaktu_dibuat() {
        return waktu_dibuat;
    }

    public void setWaktu_dibuat(String waktu_dibuat) {
        this.waktu_dibuat = waktu_dibuat;
    }

    public String getWaktu_dimodifikasi() {
        return waktu_dimodifikasi;
    }

    public void setWaktu_dimodifikasi(String waktu_dimodifikasi) {
        this.waktu_dimodifikasi = waktu_dimodifikasi;
    }

    public String getId_notifikasi() {
        return id_notifikasi;
    }

    public void setId_notifikasi(String id_notifikasi) {
        this.id_notifikasi = id_notifikasi;
    }

    public String getId_penerima() {
        return id_penerima;
    }

    public void setId_penerima(String id_penerima) {
        this.id_penerima = id_penerima;
    }

    public String getId_pengirim() {
        return id_pengirim;
    }

    public void setId_pengirim(String id_pengirim) {
        this.id_pengirim = id_pengirim;
    }

    public String getNama_penerima() {
        return nama_penerima;
    }

    public void setNama_penerima(String nama_penerima) {
        this.nama_penerima = nama_penerima;
    }

    public String getNama_pengirim() {
        return nama_pengirim;
    }

    public void setNama_pengirim(String nama_pengirim) {
        this.nama_pengirim = nama_pengirim;
    }

    public String getId_transaksi() {
        return id_transaksi;
    }

    public void setId_transaksi(String id_transaksi) {
        this.id_transaksi = id_transaksi;
    }

    public String getTeks_notifikasi() {
        return teks_notifikasi;
    }

    public void setTeks_notifikasi(String teks_notifikasi) {
        this.teks_notifikasi = teks_notifikasi;
    }

    public String getTipe_notifikasi() {
        return tipe_notifikasi;
    }

    public void setTipe_notifikasi(String tipe_notifikasi) {
        this.tipe_notifikasi = tipe_notifikasi;
    }

    public String getSudah_dibaca() {
        return sudah_dibaca;
    }

    public void setSudah_dibaca(String sudah_dibaca) {
        this.sudah_dibaca = sudah_dibaca;
    }
}
