package com.studio.karya.lakonesia.Profile;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.studio.karya.lakonesia.Profile.Akun.ReviewModel;
import com.studio.karya.lakonesia.Profile.Akun.ReviewRecycleViewAdapter;
import com.studio.karya.lakonesia.R;
import com.taufiqrahman.reviewratings.BarLabels;
import com.taufiqrahman.reviewratings.RatingReviews;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Random;


public class AkunFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    private String TAG = "============  AKUN FRAGMENT";

    private int raters[];
    private int rating_sum;
    private int rating_count;
    private float rating_total;


    private RecyclerView recyclerViewReview;
    private ReviewRecycleViewAdapter adapterReview;
    private ArrayList<ReviewModel> reviewArrayList;

    private static final String USER_ID = "USER_ID" , NAMA_USER = "NAMA_USER" , EMAIL_USER = "EMAIL_USER" , ROLE_USER = "ROLE_USER" , FOTO_USER = "FOTO_USER" , DESKRIPSI_USER = "DESKRIPSI_USER" ;

    private String mDeskripsi , mId;
    public AkunFragment() {
    }

    public static AkunFragment newInstance(String deskripsi , String id) {
        AkunFragment fragment = new AkunFragment();
        Bundle args = new Bundle();
        args.putString(USER_ID, id);
        args.putString(DESKRIPSI_USER, deskripsi);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mId = getArguments().getString(USER_ID) ;
            mDeskripsi = getArguments().getString(DESKRIPSI_USER);
        }
    }

    void initReviewRecycleView(){
        recyclerViewReview = (RecyclerView) getView(). findViewById(R.id.fragment_profile_akun_recycle_view);

        adapterReview = new ReviewRecycleViewAdapter(reviewArrayList);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());

        recyclerViewReview.setNestedScrollingEnabled(false);

        recyclerViewReview.setLayoutManager(layoutManager);

        recyclerViewReview.setAdapter(adapterReview);

    }

    void initRatingView(){
        RatingReviews ratingReviews = (RatingReviews) getView().findViewById(R.id.rating_reviews);

        int colors[] = new int[]{
                Color.parseColor("#0e9d58"),
                Color.parseColor("#bfd047"),
                Color.parseColor("#ffc105"),
                Color.parseColor("#ef7e14"),
                Color.parseColor("#d36259")};

        rating_sum = 0;
        rating_count = 0;
        rating_total = 0;

        int max = 0;
        for (int i = 0; i < raters.length; i++) {
            rating_sum += (raters[i] * (raters.length - i));
            rating_count += raters[i];

            if(raters[i] > max){
                max = raters[i];
            }
        }

        rating_total = ((float) rating_sum) / ((float) rating_count);

        TextView total_rating_text = getView().findViewById(R.id.fragment_akun_review_total_rating);
        RatingBar total_rating_bar = getView().findViewById(R.id.fragment_akun_review_total_rating_bar);
        TextView total_ulasan_text = getView().findViewById(R.id.fragment_akun_review_total_ulasan);

        rating_total = (float )rating_total - (float)(rating_total % 0.1);

        total_rating_text.setText("" + rating_total);
        total_ulasan_text.setText(rating_count + " ULASAN");
        total_rating_bar.setRating(rating_total);
//
//        raters = new int[]{
//                50,
//                4,
//                0,
//                0,
//                0,
//                0
//        };

        ratingReviews.createRatingBars((max + 3), BarLabels.STYPE1, colors, raters);
    }

    void addreview(){
        reviewArrayList = new ArrayList<>();
        AndroidNetworking.post(getString(R.string.web_get_review))
                .addHeaders("key",getString(R.string.api_key))
                .addBodyParameter("id_pelakon", mId)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "onResponse: success" + response.toString());

                        try{
                            if(response.get("status").equals("failed")){
                                Log.w(TAG, "onResponse failed : "+ response.toString() );
                                Toast.makeText(getActivity(), "" + response.getString("msg"), Toast.LENGTH_SHORT).show();
                                hideRating();
                            } else if(response.get("status").equals("success")){
                                String tmp = response.getJSONArray("data").get(0).toString();
                                Log.w(TAG, "onResponse: "+ tmp );


                                reviewArrayList = new ArrayList<>();
                                for (int i = 0; i <response.getJSONArray("data").length() ; i++) {
                                    reviewArrayList.add(new ReviewModel(
                                            response.getJSONArray("data").getJSONObject(i).getString("nama_pengirim"),
                                            response.getJSONArray("data").getJSONObject(i).getString("foto_url"),
                                            response.getJSONArray("data").getJSONObject(i).getString("id"),
                                            response.getJSONArray("data").getJSONObject(i).getString("id_pelakon"),
                                            response.getJSONArray("data").getJSONObject(i).getString("id_member"),
                                            response.getJSONArray("data").getJSONObject(i).getString("rating"),
                                            response.getJSONArray("data").getJSONObject(i).getString("ulasan"),
                                            response.getJSONArray("data").getJSONObject(i).getString("created_at")

                                    ));
                                }


                                if(reviewArrayList.size() == 0){
                                    hideRating();
                                }

                                adapterReview.addItem(reviewArrayList);
                                adapterReview.notifyDataSetChanged();

                                raters = new int[]{
                                        0,0,0,0,0
                                };
                                for (int i = 0; i <response.getJSONArray("rating_count").length() ; i++) {

                                    if(response.getJSONArray("rating_count").getJSONObject(i).getString("nilai").equals("1")){
                                        raters[4] = Integer.parseInt(response.getJSONArray("rating_count").getJSONObject(i).getString("jumlah"));
                                    }
                                    if(response.getJSONArray("rating_count").getJSONObject(i).getString("nilai").equals("2")){
                                        raters[3] = Integer.parseInt(response.getJSONArray("rating_count").getJSONObject(i).getString("jumlah"));
                                    }
                                    if(response.getJSONArray("rating_count").getJSONObject(i).getString("nilai").equals("3")){
                                        raters[2] = Integer.parseInt(response.getJSONArray("rating_count").getJSONObject(i).getString("jumlah"));
                                    }
                                    if(response.getJSONArray("rating_count").getJSONObject(i).getString("nilai").equals("4")){
                                        raters[1] = Integer.parseInt(response.getJSONArray("rating_count").getJSONObject(i).getString("jumlah"));
                                    }
                                    if(response.getJSONArray("rating_count").getJSONObject(i).getString("nilai").equals("5")){
                                        raters[0] = Integer.parseInt(response.getJSONArray("rating_count").getJSONObject(i).getString("jumlah"));
                                    }


                                }
                                Log.i(TAG, "onResponse: 1 " + raters[0]);
                                Log.i(TAG, "onResponse: 2 " + raters[1]);
                                Log.i(TAG, "onResponse: 3 " + raters[2]);
                                Log.i(TAG, "onResponse: 4 " + raters[3]);
                                Log.i(TAG, "onResponse: 5 " + raters[4]);

                                initRatingView();

                            }
                        }catch (JSONException e){
                            Log.w(TAG, "onResponse json error : "+ e.getMessage() );
                            Toast.makeText(getActivity(), "Terjadi kesalahan pada server", Toast.LENGTH_SHORT).show();

                            hideRating();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d("FAN", "onResponse: " + anError.toString() );
                        hideRating();
                    }
                });
    }

    private void hideRating(){
        TextView ratingTitle2 = getView().findViewById(R.id.fragment_akun_review_ulasan_title);
        TextView ratingKosongTitle = getView().findViewById(R.id.fragment_akun_review_belum_ada_rating_text);
        LinearLayout ratingLinearLayout = getView().findViewById(R.id.fragment_akun_review_linear_layout);
        ratingLinearLayout.setVisibility(View.GONE);
        ratingKosongTitle.setVisibility(View.VISIBLE);
        ratingTitle2.setVisibility(View.GONE);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile_akun, container, false);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        TextView txtDesc = getView().findViewById(R.id.fragment_profile_akun_deskripsi);
        if(mDeskripsi != null || !mDeskripsi.isEmpty()){
            txtDesc.setText(mDeskripsi);
        }

        addreview();
        initReviewRecycleView();

//        initRatingView();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
