package com.studio.karya.lakonesia;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.pixplicity.easyprefs.library.Prefs;
import com.studio.karya.lakonesia.Model.CurrentUserModel;
import com.studio.karya.lakonesia.Utils.PublicString;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;


public class LoginActivity extends AppCompatActivity {

//    private Button btn_facebook_signin;
    private Button btn_google_singin , btn_fb_login;
    private GoogleSignInClient mGoogleSignInClient;
    private CallbackManager callbackManager;
    private String TAG = "===== Login Activity";
    private TextView signupText;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);
        AndroidNetworking.initialize(getApplicationContext());


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        signupText = findViewById(R.id.text_signup);
        progressBar = findViewById(R.id.progressbar_signup);

        btn_google_singin = findViewById(R.id.btn_login_google);
        btn_google_singin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_google_singin.setClickable(false);
                progressBar.setVisibility(View.VISIBLE);
                signupText.setVisibility(View.INVISIBLE);

//                handleLoginToWebservice("zakwandhiya@student.ub.ac.id");
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, 1);
            }
        });

        FacebookSdk.sdkInitialize(this.getApplicationContext());

        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.d("Success", "Login");

                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(LoginActivity.this, "Login Cancel", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Toast.makeText(LoginActivity.this, exception.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });


        btn_fb_login = (Button)findViewById(R.id.btn_login_facebook);
        btn_fb_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                handleLoginToWebservice("infokaryastudio@gmail.com");
                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("public_profile", "user_friends"));

            }
        });

        String emailTmp = Prefs.getString(PublicString.CUREENT_USER_EMAIL , null);
        if(emailTmp != null){
            if(!emailTmp.isEmpty()){
                Intent signInIntent = new Intent(LoginActivity.this, MainActivity.class);
                startActivityForResult(signInIntent, 1);
                handleInitial(emailTmp);
                finish();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        try{
            if (requestCode == 1) {
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                handleSignInResult(task);
            }
        } catch (Exception e){
            Log.w(TAG, "signInResult:failed code=" + e.getMessage());
            progressBar.setVisibility(View.INVISIBLE);
            btn_google_singin.setClickable(true);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        Log.w(TAG, "handleSignInResult : " + completedTask.getResult().toString());
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            Log.w(TAG, "signInResult account photo : " + account.getPhotoUrl());
            handleLoginToWebservice(account.getEmail());
            if(logedIn){

            } else {
//                handleRegisterToWebservice();
            }

        } catch (ApiException e) {
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            progressBar.setVisibility(View.INVISIBLE);
            btn_google_singin.setClickable(true);
        }
    }


    private boolean logedIn = false;

    private void handleLoginToWebservice(String email){
        AndroidNetworking.post(getString(R.string.web_login))
                .addHeaders("key","12345")
                .addBodyParameter("email", email)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG , "onResponse: success " + response.toString());

                        try{
                            if(response.get("status").equals("failed")){
                                Log.w(TAG, "onResponse: " + "user unregistererd" );
                                Intent signInIntent = new Intent(LoginActivity.this, RegisterActivity.class);
                                startActivityForResult(signInIntent, 1);
                            } else if(response.get("status").equals("success")){
                                Log.w(TAG, "onResponse: succes" + response.toString() );
                                PublicString.setPhoneNumber(response.getString("phone"));
                                CurrentUserModel.setCurrentUserModel(
                                        response.getJSONArray("data").getJSONObject(0).getString("id"),
                                        response.getJSONArray("data").getJSONObject(0).getString("nama"),
                                        response.getJSONArray("data").getJSONObject(0).getString("email"),
                                        response.getJSONArray("data").getJSONObject(0).getString("role"),
                                        response.getJSONArray("data").getJSONObject(0).getString("foto_url"),
                                        response.getJSONArray("data").getJSONObject(0).getString("deskripsi"),
                                        response.getJSONArray("data").getJSONObject(0).getString("id_kategori"),
                                        response.getJSONArray("data").getJSONObject(0).getString("nama_kategori")
                                );
                                Intent signInIntent = new Intent(LoginActivity.this, MainActivity.class);
                                startActivityForResult(signInIntent, 1);
                                finish();
                            }
                        }catch (JSONException e){
                            Log.w(TAG, "onResponse json error : " + e.toString() );
                            progressBar.setVisibility(View.INVISIBLE);
                        }

                        btn_google_singin.setClickable(true);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(TAG, "onResponse an error: " + anError.getErrorDetail());
                        Toast.makeText(LoginActivity.this, "" + anError.getErrorDetail(), Toast.LENGTH_SHORT).show();
                        btn_google_singin.setClickable(true);
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                });
    }

    private void handleInitial(String email){
        AndroidNetworking.post(getString(R.string.web_login))
                .addHeaders("key","12345")
                .addBodyParameter("email", email)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG , "onResponse: success " + response.toString());

                        try{
                            if(response.get("status").equals("failed")){
                                Log.w(TAG, "onResponse: " + "user unregistererd" );
                                CurrentUserModel.deleteCurentUserModel();
                                Intent signInIntent = new Intent(LoginActivity.this, RegisterActivity.class);
                                startActivity(signInIntent);
                            } else if(response.get("status").equals("success")){
                                Log.w(TAG, "onResponse: succes" + response.toString() );
                                PublicString.setPhoneNumber(response.getString("phone"));
                                CurrentUserModel.setCurrentUserModel(
                                        response.getJSONArray("data").getJSONObject(0).getString("id"),
                                        response.getJSONArray("data").getJSONObject(0).getString("nama"),
                                        response.getJSONArray("data").getJSONObject(0).getString("email"),
                                        response.getJSONArray("data").getJSONObject(0).getString("role"),
                                        response.getJSONArray("data").getJSONObject(0).getString("foto_url"),
                                        response.getJSONArray("data").getJSONObject(0).getString("deskripsi"),
                                        response.getJSONArray("data").getJSONObject(0).getString("id_kategori"),
                                        response.getJSONArray("data").getJSONObject(0).getString("nama_kategori")
                                );
                            }
                        }catch (JSONException e){
                            Log.w(TAG, "onResponse json error : " + e.toString() );
                            Toast.makeText(LoginActivity.this, "" + "internal server error", Toast.LENGTH_SHORT).show();
//                            Intent signInIntent = new Intent(LoginActivity.this, LoginActivity.class);
//                            startActivity(signInIntent);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(TAG, "onResponse an error: " + anError.getErrorDetail());
                        Toast.makeText(LoginActivity.this, "" + anError.getErrorDetail(), Toast.LENGTH_SHORT).show();
//                        Intent signInIntent = new Intent(LoginActivity.this, LoginActivity.class);
//                        startActivity(signInIntent);
                    }
                });
    }

}
