package com.studio.karya.lakonesia.HargaFragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.studio.karya.lakonesia.Model.CurrentUserModel;
import com.studio.karya.lakonesia.R;
import com.studio.karya.lakonesia.Utils.PublicString;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class HargaFragment extends Fragment {

//    private static final String ARG_ID = "param1";
//    private static final String ARG_EMAIL = "param2";
//    private static final String ARG_ROLE = "param3";

    private static final String USER_ID = "USER_ID" , NAMA_USER = "NAMA_USER" , EMAIL_USER = "EMAIL_USER" , ROLE_USER = "ROLE_USER" , FOTO_USER = "FOTO_USER" , DESKRIPSI_USER = "DESKRIPSI_USER" ;

    public String mId , mNama,mEmail , mRole ,mFoto_url;

    private String TAG = "===== Harga Fragment";

    private OnFragmentInteractionListener mListener;

    private RecyclerView recyclerViewHarga;
    private ArrayList<HargaModel> hargaArrayList;

    public HargaRecycleViewAdapter adapterHarga;
    public HargaRecycleViewAdapterMember adapterHargaMember;

    public HargaFragment() {}

    public static HargaFragment newInstance(String id, String nama,String email , String role ,String foto_url) {
        HargaFragment fragment = new HargaFragment();
        Bundle args = new Bundle();
        args.putString(USER_ID, id);
        args.putString(NAMA_USER, nama);
        args.putString(EMAIL_USER, email);
        args.putString(ROLE_USER, role);
        args.putString(FOTO_USER, foto_url);
        fragment.setArguments(args);

        fragment.mId = id;
        fragment.mNama = nama;
        fragment.mEmail = email;
        fragment.mRole = role;
        fragment.mFoto_url = foto_url;
        Log.w("===== Harga Fragment", "newInstance: "+ fragment.mId + " " + id);
        return fragment;
    }


//    public static HargaFragment newInstance() {
//        HargaFragment fragment = new HargaFragment();
//        Bundle args = new Bundle();
//        fragment.setArguments(args);
//        return fragment;
//    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.w(TAG, "onActivityCreated: id"+ mId );
        Log.w(TAG, "onActivityCreated: role"+ mRole );
        initLakonRecycleView();
//
//        Button btn_dynamic_fragment_harga = getView().findViewById(R.id.profile_harga_dynamic_button);
//        if(! mId.equals(CurrentUserModel.getCurrentUserModel().getId()) &&
//                mRole.equals(PublicString.ROLE_LAKON)){
//
//            Log.w(TAG, "onClick dynamic button: " + mEmail );
//            btn_dynamic_fragment_harga.setText(" CHECKOUT ");
//            btn_dynamic_fragment_harga.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (adapterHargaMember.getSum() > 0){
//                        TransactionDialog bottomSheet = TransactionDialog.newInstance(
//                                adapterHargaMember.getTransactionModels(),
//                                new LakonModel(mId , mNama , mEmail , mRole ,  mFoto_url)
//                        );
//                        bottomSheet.show(getActivity().getSupportFragmentManager(), "Dari Harga Fregment");
//
//                    } else {
//                        Toast.makeText(getContext() , "anda tidak memilih layanan apapun" ,Toast.LENGTH_SHORT).show();
//                    }
//                }
//            });
//
//
//
//        } else if(mId.equalsIgnoreCase(CurrentUserModel.getCurrentUserModel().getId()) &&
//                mRole.equalsIgnoreCase(PublicString.ROLE_LAKON)){
//
//
//            btn_dynamic_fragment_harga.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    showEditDialog();
//                }
//            });
//
//        }


        addHarga(mId);


    }

    boolean showEditDialog(){
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.fragment_profile_harga_custom_dialog_for_edit_layanan);
        dialog.setTitle("EDIT LAYANAN");
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final EditText etNama =  dialog.findViewById(R.id.profile_harga_edit_nama_layanan);
        final EditText etHarga = dialog.findViewById(R.id.profile_harga_edit_harga_layanan);
        final EditText etHargaPromo = dialog.findViewById(R.id.profile_harga_edit_harga_promo_layanan);
        final EditText etDeskripsi = dialog.findViewById(R.id.profile_harga_edit_deskripsi_layanan);
        final RelativeLayout restOfDialog = dialog.findViewById(R.id.profile_harga_edit_main_relative_layout);
        final ProgressBar dialogProgressBar = dialog.findViewById(R.id.profile_harga_edit_progress_bar);


        dialogProgressBar.setVisibility(View.INVISIBLE);

        Button dialogButton = (Button) dialog.findViewById(R.id.profile_harga_edit_submit_button);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                restOfDialog.setVisibility(View.INVISIBLE);
                dialogProgressBar.setVisibility(View.VISIBLE);

                String namaForEdit = etNama.getText().toString();
                String hargaForEdit = etHarga.getText().toString();
                String hargaPromoForEdit = etHargaPromo.getText().toString();
                String deskripsiForEdit = etDeskripsi.getText().toString();

                if(namaForEdit.isEmpty() || hargaForEdit.isEmpty() || deskripsiForEdit.isEmpty()){
                    Toast.makeText(getContext() , "lengkapi form terlebih dahulu" , Toast.LENGTH_SHORT).show();

                    restOfDialog.setVisibility(View.VISIBLE);
                    dialogProgressBar.setVisibility(View.INVISIBLE);
                } else {
                    submitLayanan(dialog , namaForEdit , hargaForEdit ,hargaPromoForEdit, deskripsiForEdit);
                }
            }
        });

        Button dialogDismissButton = (Button) dialog.findViewById(R.id.profile_harga_edit_cancel_button);
        dialogDismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
        return true;

    }

    boolean submitLayanan(final Dialog dialog , String nama , String harga ,String harga_promo, String deskripsi){
        AndroidNetworking.post(getString(R.string.web_add_layanan))
                .addHeaders("key","12345")
                .addBodyParameter("id", CurrentUserModel.getCurrentUserModel().getId() )
                .addBodyParameter("nama_layanan", nama)
                .addBodyParameter("deskripsi", deskripsi)
                .addBodyParameter("harga", harga)
                .addBodyParameter("harga_promo", harga_promo)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG , "onResponse: success" + response.toString());

                        try{
                            if(response.get("status").equals("failed")){
                                Toast.makeText(getContext() , "penambahan layanan gagal" , Toast.LENGTH_LONG).show();
                                dialog.dismiss();
                            } else if(response.get("status").equals("success")){
                                Toast.makeText(getContext() , "penambahan layanan berhasil" , Toast.LENGTH_LONG).show();
                                addHarga(CurrentUserModel.getCurrentUserModel().getId());
                                dialog.dismiss();
                            }
                        }catch (JSONException e){
                            Log.w(TAG, "onResponse: " + e.toString() );
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d("FAN", "onResponse: " + anError.toString() );
                        dialog.dismiss();
                    }
                });
        return true;
    }

    void initLakonRecycleView(){
        recyclerViewHarga = (RecyclerView) getView().findViewById(R.id.profile_harga_recycler_view);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerViewHarga.setLayoutManager(layoutManager);
        recyclerViewHarga.setNestedScrollingEnabled(false);


        if(! mId.equals(CurrentUserModel.getCurrentUserModel().getId()) &&
           mRole.equals(PublicString.ROLE_LAKON)){

            adapterHargaMember = new HargaRecycleViewAdapterMember(hargaArrayList , getActivity() , getContext() , HargaFragment.this);
            recyclerViewHarga.setAdapter(adapterHargaMember);


        } else if(mId.equals(CurrentUserModel.getCurrentUserModel().getId()) &&
                mRole.equals(PublicString.ROLE_LAKON)){

            adapterHarga = new HargaRecycleViewAdapter(hargaArrayList  , getActivity() , getContext() , HargaFragment.this);
            recyclerViewHarga.setAdapter(adapterHarga);

        }
    }

    public void addHarga(String id){
        hargaArrayList = new ArrayList<>();
        Log.w(TAG, "addHarga: " + id );
        AndroidNetworking.post(getString(R.string.web_get_harga))
                .addHeaders("key",getString(R.string.api_key))
                .addBodyParameter("id",id)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "onResponse: " + response.toString());

                        try{
                            if(response.get("status").equals("failed")){
                                Log.w(TAG, "onResponse failed: "+ response );
                                if(mId.equals(CurrentUserModel.getCurrentUserModel().getId()) &&
                                        mRole.equals(PublicString.ROLE_LAKON)){


                                    hargaArrayList.add(new HargaModelForButton(
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            ""
                                    ));

                                    adapterHarga.addItem(hargaArrayList , getActivity() , getContext() , HargaFragment.this);
                                    adapterHarga.notifyDataSetChanged();

                                }
                            } else if(response.get("status").equals("success")){
                                String tmp = response.getJSONArray("data").get(0).toString();
                                Log.w(TAG, "onResponse success: "+ response );


                                if(! mId.equals(CurrentUserModel.getCurrentUserModel().getId()) &&
                                        mRole.equals(PublicString.ROLE_LAKON)){

                                    hargaArrayList = new ArrayList<>();

                                    boolean promo = false , normal = false;

                                    for (int i = 0; i <response.getJSONArray("data").length() ; i++) {
                                        if(response.getJSONArray("data").getJSONObject(i).getString("status_promo").equals("1")){
                                            if(!promo){
                                                hargaArrayList.add(new HargaModelForPromoText("", "", "", "", "", "" , ""));
                                                promo = true;
                                            }
                                            hargaArrayList.add(new HargaModelMain(
                                                    response.getJSONArray("data").getJSONObject(i).getString("id_layanan"),
                                                    response.getJSONArray("data").getJSONObject(i).getString("id_pelakon"),
                                                    response.getJSONArray("data").getJSONObject(i).getString("nama_layanan"),
                                                    response.getJSONArray("data").getJSONObject(i).getString("deskripsi"),
                                                    response.getJSONArray("data").getJSONObject(i).getString("harga"),
                                                    response.getJSONArray("data").getJSONObject(i).getString("status_promo"),
                                                    response.getJSONArray("data").getJSONObject(i).getString("harga_promo")
                                            ));
                                        }
                                    }

                                    for (int i = 0; i <response.getJSONArray("data").length() ; i++) {
                                        if(response.getJSONArray("data").getJSONObject(i).getString("status_promo").equals("0")){
                                            if(!normal){
                                                hargaArrayList.add(new HargaModelForNormalText("", "", "", "", "", "0" , ""));
                                                normal = true;
                                            }
                                            hargaArrayList.add(new HargaModelMain(
                                                    response.getJSONArray("data").getJSONObject(i).getString("id_layanan"),
                                                    response.getJSONArray("data").getJSONObject(i).getString("id_pelakon"),
                                                    response.getJSONArray("data").getJSONObject(i).getString("nama_layanan"),
                                                    response.getJSONArray("data").getJSONObject(i).getString("deskripsi"),
                                                    response.getJSONArray("data").getJSONObject(i).getString("harga"),
                                                    response.getJSONArray("data").getJSONObject(i).getString("status_promo"),
                                                    response.getJSONArray("data").getJSONObject(i).getString("harga_promo")

                                            ));
                                        }
                                    }

                                    hargaArrayList.add(new HargaModelForButton("","","","","","", ""                                    ));

                                    adapterHargaMember.addItem(hargaArrayList , getActivity() , getContext() , HargaFragment.this);
                                    adapterHargaMember.notifyDataSetChanged();

                                } else if(mId.equals(CurrentUserModel.getCurrentUserModel().getId()) &&
                                        mRole.equals(PublicString.ROLE_LAKON)){


                                    hargaArrayList = new ArrayList<>();
                                    for (int i = 0; i <response.getJSONArray("data").length() ; i++) {
                                        hargaArrayList.add(new HargaModelMain(
                                                response.getJSONArray("data").getJSONObject(i).getString("id_layanan"),
                                                response.getJSONArray("data").getJSONObject(i).getString("id_pelakon"),
                                                response.getJSONArray("data").getJSONObject(i).getString("nama_layanan"),
                                                response.getJSONArray("data").getJSONObject(i).getString("deskripsi"),
                                                response.getJSONArray("data").getJSONObject(i).getString("harga"),
                                                response.getJSONArray("data").getJSONObject(i).getString("status_promo"),
                                                response.getJSONArray("data").getJSONObject(i).getString("harga_promo")
                                        ));
                                    }

                                    hargaArrayList.add(new HargaModelForButton("","","","","","" , ""));

                                    adapterHarga.addItem(hargaArrayList , getActivity() , getContext() , HargaFragment.this);
                                    adapterHarga.notifyDataSetChanged();

                                }
                            }
                        }catch (JSONException e){
                            Log.w(TAG, "onResponse: "+ response );
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(TAG, "================ onResponse error: " + anError.toString() );
                    }
                });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mId = getArguments().getString(USER_ID) != null ? getArguments().getString(USER_ID) : mId;
            mNama = getArguments().getString(NAMA_USER);
            mEmail = getArguments().getString(EMAIL_USER);
            mRole = getArguments().getString(ROLE_USER) != null? getArguments().getString(ROLE_USER) : mRole;
            mFoto_url = getArguments().getString(FOTO_USER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile_harga, container, false);
    }


    private void getHarga(){
        AndroidNetworking.post(getString(R.string.web_get_harga))
                .addHeaders("key",getString(R.string.api_key))
                .addBodyParameter("id" , "1")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "onResponse: success" + response.toString());

                        try{
                            if(response.get("status").equals("failed")){

                            } else if(response.get("status").equals("success")){
                                String tmp = response.getJSONArray("data").get(0).toString();
                                Log.w(TAG, "onResponse: "+ tmp );


//                                iklanArrayList = new ArrayList<>();
//                                for (int i = 0; i <response.getJSONArray("data").length() ; i++) {
//                                    iklanArrayList.add(new old_IklanModel(
//                                            response.getJSONArray("data").getJSONObject(i).getString("id_iklan"),
//                                            response.getJSONArray("data").getJSONObject(i).getString("url_iklan")
//
//                                    ));
//                                }
//                                adapterIklan.addItem(iklanArrayList);
//                                adapterIklan.notifyDataSetChanged();


                            }
                        }catch (JSONException e){

                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(TAG, "onResponse: " + anError.toString() );
                    }
                });
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        getHarga();
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
