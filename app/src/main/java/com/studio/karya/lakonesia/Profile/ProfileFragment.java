package com.studio.karya.lakonesia.Profile;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.pixplicity.easyprefs.library.Prefs;
import com.squareup.picasso.Picasso;
import com.studio.karya.lakonesia.EditProfile.EditProfileFragment;
import com.studio.karya.lakonesia.HargaFragment.HargaFragment;
import com.studio.karya.lakonesia.LoginActivity;
import com.studio.karya.lakonesia.MainActivity;
import com.studio.karya.lakonesia.Model.CurrentUserModel;
import com.studio.karya.lakonesia.R;
import com.studio.karya.lakonesia.Transaction.TransactionDialog;
import com.studio.karya.lakonesia.Utils.PublicString;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ProfileFragment extends Fragment implements  TransactionDialog.BottomSheetListener{

    private static final String USER_ID = "USER_ID" , NAMA_USER = "NAMA_USER" , EMAIL_USER = "EMAIL_USER" , ROLE_USER = "ROLE_USER" , FOTO_USER = "FOTO_USER" , DESKRIPSI_USER = "DESKRIPSI_USER" ;

    private static final String TAG = "===== Profile Fragment";

    private String mId , mNama,mEmail , mRole ,mFoto_url , mDeskripsi;

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private TextView tvProfileTitle;
    private ScrollView profileScrollView;

    private OnFragmentInteractionListener mListener;

    public ProfileFragment() {
    }

    public static ProfileFragment newProfileInstance(String id) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(USER_ID, CurrentUserModel.getCurrentUserModel().getId());
        args.putString(NAMA_USER, CurrentUserModel.getCurrentUserModel().getNama());
        args.putString(EMAIL_USER, CurrentUserModel.getCurrentUserModel().getEmail());
        args.putString(ROLE_USER, CurrentUserModel.getCurrentUserModel().getRole());
        args.putString(FOTO_USER, CurrentUserModel.getCurrentUserModel().getFoto_url());
        args.putString(DESKRIPSI_USER, CurrentUserModel.getCurrentUserModel().getDeskripsi());
        fragment.setArguments(args);

        Log.w(TAG, "newProfileInstance : " + " " + args.getString(ROLE_USER) + " " + args.getString(DESKRIPSI_USER) );
        return fragment;
    }


    public static ProfileFragment newProfileInstance(String id, String nama, String email , String role , String foto_url , String deskripsi) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(USER_ID, id);
        args.putString(NAMA_USER, nama);
        args.putString(EMAIL_USER, email);
        args.putString(ROLE_USER, role);
        args.putString(FOTO_USER, foto_url);
        args.putString(DESKRIPSI_USER, deskripsi);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
//            mId = getArguments().getString(USER_ID) != null ? getArguments().getString(USER_ID) : mId;
//            mNama = getArguments().getString(NAMA_USER) != null ? getArguments().getString(NAMA_USER) : mNama;
//            mEmail = getArguments().getString(EMAIL_USER) != null? getArguments().getString(EMAIL_USER) : mEmail;
//            mRole = getArguments().getString(ROLE_USER) != null ? getArguments().getString(ROLE_USER) : mRole;
//            mFoto_url = getArguments().getString(FOTO_USER) != null?getArguments().getString(FOTO_USER) : mFoto_url;
//            mDeskripsi = getArguments().getString(DESKRIPSI_USER) != null? getArguments().getString(DESKRIPSI_USER): mDeskripsi;

            mId = getArguments().getString(USER_ID) ;
            mNama = getArguments().getString(NAMA_USER);
            mEmail = getArguments().getString(EMAIL_USER);
            mRole = getArguments().getString(ROLE_USER) ;
            mFoto_url = getArguments().getString(FOTO_USER);
            mDeskripsi = getArguments().getString(DESKRIPSI_USER);

            Log.w(TAG, "onCreate: " + " " + mFoto_url + " " + mRole + " " + mDeskripsi);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_profile, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Log.w(TAG, "onActivityCreated: id " + mId + " role " + mRole );
        viewPager = getView().findViewById(R.id.viewpager);
        profileScrollView = getView().findViewById(R.id.fragment_profile_scroll_view);
        if(mRole.equals(PublicString.ROLE_LAKON)){
            setupViewPager(viewPager);
            tabLayout = getView().findViewById(R.id.profile_tab);
            tabLayout.setupWithViewPager(viewPager);
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.colorPrimary));
            tabLayout.setSelectedTabIndicatorHeight((int) (3 * getResources().getDisplayMetrics().density));
            tabLayout.setTabTextColors(getResources().getColor(R.color.grey2), getResources().getColor(R.color.colorPrimary));
        }else{
            viewPager.setVisibility(View.GONE);
            tabLayout = getView().findViewById(R.id.profile_tab);
            tabLayout.setVisibility(View.GONE);
        }

        tvProfileTitle = getView().findViewById(R.id.fragment_profile_title);
        Toolbar profileAppBar = getView().findViewById(R.id.fragment_profile_appbar);
        ImageView ivProfilePlaceholder = getView().findViewById(R.id.fragment_profile_image_place_holder);
        ImageView ivProfilePlaceholder2 = getView().findViewById(R.id.fragment_profile_image_place_holder2);
        ImageView ivProfilePlaceholder3 = getView().findViewById(R.id.fragment_profile_image_place_holder3);

        if(mRole == null){
            mRole = CurrentUserModel.getCurrentUserModel().getRole();
        }
        if(mId == null){
            mId = CurrentUserModel.getCurrentUserModel().getId();
        }

        if(mId.equalsIgnoreCase(CurrentUserModel.getCurrentUserModel().getId())){
            Log.w(TAG, "onActivityCreated: current user" );
            ivProfilePlaceholder2.setVisibility(View.GONE);

            profileAppBar.inflateMenu(R.menu.profile_option_menu);

            profileAppBar.setOverflowIcon(ContextCompat.getDrawable(getContext(), R.drawable.ic_option_menu));

            profileAppBar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    Log.w(TAG, "onOptionsItemSelected: edit profile" );
                    switch(menuItem.getItemId())
                    {
                        case R.id.profile_menu_edit_profile:
                            Log.w(TAG, "onOptionsItemSelected: edit profile" );
                            ((MainActivity)getActivity()).replaceFragment(4 ,EditProfileFragment.newInstance(mId));
//                            Fragment newFragment =
//                            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
//
//                            transaction.replace(R.id.fragment_profile_main_frame_layout, newFragment);
//                            transaction.addToBackStack(null);
//
//                            transaction.commit();
                            break;
                        case R.id.profile_menu_logout_profile:
                            Log.w(TAG, "onOptionsItemSelected: logout" );
                            CurrentUserModel.deleteCurentUserModel();
                            showDialogKeluar();
                            break;
                    }
                    return false;
                }
            });

        }else if(! mId.equals(CurrentUserModel.getCurrentUserModel().getId()) &&
                mRole.equals(PublicString.ROLE_LAKON)){

            Log.w(TAG, "onActivityCreated: not current user" );

            ivProfilePlaceholder.setVisibility(View.GONE);
            ivProfilePlaceholder3.setVisibility(View.GONE);
            profileAppBar.setNavigationIcon(R.drawable.ic_back_button);
            profileAppBar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().onBackPressed();
                }
            });
            tvProfileTitle.setText(mNama);

        }


//        wrapTabIndicatorToTitle(tabLayout , 100 ,100);

        ImageView imageView = getView().findViewById(R.id.fragment_profile_foto);

        if(mFoto_url != null){
            if(!mFoto_url.isEmpty())
            Picasso.get().load(mFoto_url).fit().centerCrop().into(imageView);
            ProgressBar progressBar = getView().findViewById(R.id.fragment_profile_foto_progress_bar);
        }

    }

    private void showDialogKeluar(){
        final Dialog dialog = new Dialog(getActivity());

        dialog.setContentView(R.layout.fragment_profile_harga_custom_dialog_for_delete_layanan);
        dialog.setTitle("Signout");
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final RelativeLayout restOfDialog = (RelativeLayout) dialog.findViewById(R.id.profile_harga_delete_main_relative_layout);
        final ProgressBar dialogProgressBar = (ProgressBar) dialog.findViewById(R.id.profile_harga_delete_progress_bar);

        final TextView textView = dialog.findViewById(R.id.profile_harga_delete_deskripsi_layanan_text);
        textView.setText("Apakah anda yakin?");
        dialogProgressBar.setVisibility(View.INVISIBLE);

        Button dialogButton = (Button) dialog.findViewById(R.id.profile_harga_delete_submit_button);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                restOfDialog.setVisibility(View.INVISIBLE);
                dialogProgressBar.setVisibility(View.VISIBLE);


                AndroidNetworking.post(getString(R.string.web_logout))
                        .addHeaders("key","12345")
                        .setPriority(Priority.MEDIUM)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.d(TAG , "onResponse: success " + response.toString());

                                try{
                                    if(response.get("status").equals("failed")){
                                        Log.w(TAG, "onResponse: " + "user unregistererd" );GoogleSignInClient acct = GoogleSignIn.getClient(getActivity() , GoogleSignInOptions.DEFAULT_SIGN_IN);
                                        acct.signOut().addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                Toast.makeText(getContext(), Prefs.getString(PublicString.CUREENT_USER_EMAIL , "null"), Toast.LENGTH_SHORT).show();
                                                Intent signInIntent = new Intent(getActivity(), LoginActivity.class);
                                                startActivityForResult(signInIntent, 1);
                                                getActivity().finish();
                                            }
                                        }).addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Toast.makeText(getContext(), "Terjadi Kesalahan, periksa koneksi internet anda", Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    } else if(response.get("status").equals("success")){
                                        Log.w(TAG, "onResponse: succes" + response.toString() );
                                        GoogleSignInClient acct = GoogleSignIn.getClient(getActivity() , GoogleSignInOptions.DEFAULT_SIGN_IN);
                                        acct.signOut().addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {

                                                Intent signInIntent = new Intent(getActivity(), LoginActivity.class);
                                                startActivityForResult(signInIntent, 1);
                                                getActivity().finish();
                                            }
                                        }).addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Toast.makeText(getContext(), "Terjadi Kesalahan, periksa koneksi internet anda", Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    }
                                }catch (JSONException e){
                                    Log.w(TAG, "onResponse json error : " + e.toString() );
                                    Toast.makeText(getContext(), "Terjadi Kesalahan, periksa koneksi internet anda", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onError(ANError anError) {
                                Log.d(TAG, "onResponse an error: " + anError.getErrorDetail());
                                Toast.makeText(getContext(), "Terjadi Kesalahan, periksa koneksi internet anda", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });

        Button dialogDismissButton = (Button) dialog.findViewById(R.id.profile_harga_delete_cancel_button);
        dialogDismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void setupViewPager(final ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        Log.w(TAG, "setupViewPager: profile fragment id " + mId + " role " + mRole );
        adapter.addFragment(HargaFragment.newInstance(mId , mNama ,mEmail , mRole , mFoto_url), "LAYANAN");
        adapter.addFragment(AkunFragment.newInstance(mDeskripsi , mId), "AKUN");
        adapter.addFragment(new GaleriFragment(), "GALERI");
        viewPager.setAdapter(adapter);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onButtonClicked(String text) {
//        mTextView.setText(text);
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
