package com.studio.karya.lakonesia.Profile;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.studio.karya.lakonesia.Model.GaleryModel;
import com.studio.karya.lakonesia.R;

import java.util.ArrayList;

public class GaleryAdapter extends RecyclerView.Adapter<GaleryAdapter.GridViewHolder> {
    private ArrayList<GaleryModel> listGalery;

    public GaleryAdapter(ArrayList<GaleryModel> list) {
        this.listGalery = list;
    }

    @NonNull
    @Override
    public GridViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fragment_profile_galeri_image, viewGroup, false);
        return new GridViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final GridViewHolder holder, int position) {
        Picasso.get().load(listGalery.get(position).getFotoUrl()).into(holder.imgPhoto);
    }

    @Override
    public int getItemCount() {
        return listGalery.size();
    }

    class GridViewHolder extends RecyclerView.ViewHolder {
        ImageView imgPhoto;

        GridViewHolder(View itemView) {
            super(itemView);
            imgPhoto = itemView.findViewById(R.id.icon);
        }
    }
}
