package com.studio.karya.lakonesia.Utils;

import android.annotation.SuppressLint;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class StringUtils {

    @SuppressLint("SimpleDateFormat")
    public static String dayName(String inputDate, String format){
        Date date = null;
        try {
            date = new SimpleDateFormat("d-M-Y").parse(inputDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date);
    }

    @SuppressLint("SimpleDateFormat")
    public static String dateFormat(String inputdate){
        Date date = null;
        try {
            date = new SimpleDateFormat("d-M-Y").parse(inputdate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return  new SimpleDateFormat("d-M-Y").format(date);
    }


}
