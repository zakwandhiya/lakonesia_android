package com.studio.karya.lakonesia.Transaction;

import android.content.Context;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.studio.karya.lakonesia.HargaFragment.HargaFragment;
import com.studio.karya.lakonesia.HargaFragment.HargaModel;
import com.studio.karya.lakonesia.R;

import java.util.ArrayList;
import java.util.HashMap;


public class TransactionRecycleViewAdapter2 extends RecyclerView.Adapter<TransactionRecycleViewAdapter2.HargaRecycleViewHolder> {

    private ArrayList<TransactionModel> dataList;
//    private FragmentActivity activity;
//    private Context context;
//    private HargaFragment hargaFragment;

    private String TAG = "============== Harga Adapter";

    private HashMap<String , TransactionModel> itemHolder;

    private HargaFragment hargaFragment;

    public TransactionRecycleViewAdapter2(ArrayList<TransactionModel> dataList) {
        this.dataList = dataList;
    }

    public void addItem(ArrayList<TransactionModel> mData , FragmentActivity activity , Context context , HargaFragment hargaFragment) {
        this.dataList = mData;
        this.itemHolder = new HashMap<String , TransactionModel>();
//        this.activity =  activity;
//        this.context = context;
        this.hargaFragment = hargaFragment;
        notifyDataSetChanged();
    }

    @Override
    public HargaRecycleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.activity_transaction_recycle_view_layout, parent, false);
        return new HargaRecycleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final HargaRecycleViewHolder holder, int position) {
        itemHolder = new HashMap<String , TransactionModel>();
//        holder.txtNama.setText(dataList.get(position).getNama_layanan());
//        holder.txtHarga.setText("RP " + dataList.get(position).getHarga());
        final HargaModel tmpHargaModel = dataList.get(position);
        final String id_tmp = tmpHargaModel.getId_layanan();
        final String nama_tmp = tmpHargaModel.getNama_layanan();
        final String harga_tmp = tmpHargaModel.getHarga();
        final String deskripsi_tmp = tmpHargaModel.getDeskripsi();


//        holder.btnTambah.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(itemHolder.get(tmpHargaModel.getId_layanan()) == null){
//                    Log.w("===================== Harga Member Adapter", "onClick button tambah: id" + tmpHargaModel.getId_layanan() + "is null" );
//                    itemHolder.put(tmpHargaModel.getId_layanan() , new TransactionModel(
//                            tmpHargaModel.getId_layanan() ,
//                            tmpHargaModel.getId_pelakon() ,
//                            tmpHargaModel.getNama_layanan() ,
//                            tmpHargaModel.getDeskripsi() ,
//                            tmpHargaModel.getHarga() ,
//                            tmpHargaModel.getStatus_promo() ,
//                            1));
//                } else {
//                    itemHolder.get(tmpHargaModel.getId_layanan()).tambah();
//                }
//
//                int kuantitasJumlah = itemHolder.get(tmpHargaModel.getId_layanan()).getKuantitas();
//
//                Log.w("===================== Harga Member Adapter", "onClick button tambah: " + kuantitasJumlah );
//                holder.txtKuantitas.setText(""+kuantitasJumlah);
//            }
//        });
//
//        holder.btnKurang.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(itemHolder.get(tmpHargaModel.getId_layanan()) == null){
//
//                } else if (itemHolder.get(tmpHargaModel.getId_layanan()).getKuantitas() <= 0) {
//
//                } else if (itemHolder.get(tmpHargaModel.getId_layanan()).getKuantitas() > 0) {
//
//                    itemHolder.get(tmpHargaModel.getId_layanan()).kurang();
//                    int kuantitasJumlah = itemHolder.get(tmpHargaModel.getId_layanan()).getKuantitas();
//                    Log.w("===================== Harga Member Adapter", "onClick button kurang: " + kuantitasJumlah );
//                    holder.txtKuantitas.setText(""+kuantitasJumlah);
//                }
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class HargaRecycleViewHolder extends RecyclerView.ViewHolder{
        private TextView txtNama, txtHarga, txtKuantitas;
        private ImageView btnTambah , btnKurang;

        public HargaRecycleViewHolder(View itemView) {
            super(itemView);
//            txtHarga = (TextView) itemView.findViewById(R.id.profile_harga_layanan);
//            txtNama =  (TextView) itemView.findViewById(R.id.profile_nama_layanan);
//            txtKuantitas = (TextView) itemView.findViewById(R.id.fragment_profile_harga_for_transact_text);
//            btnTambah =  (ImageView) itemView.findViewById(R.id.profile_harga_tambah_kuantitas);
//            btnKurang =  (ImageView) itemView.findViewById(R.id.profile_harga_kurangi_kuantitas);
        }
    }
}
