package com.studio.karya.lakonesia;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.studio.karya.lakonesia.Dashboard.LakonRecycleViewAdapter;
import com.studio.karya.lakonesia.Dashboard.LakonRecycleViewModel;
import com.studio.karya.lakonesia.Model.CurrentUserModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.facebook.FacebookSdk.getApplicationContext;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SearchFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SearchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private ImageView notFound;
    private RecyclerView recyclerViewLakon;
    private LakonRecycleViewAdapter adapterLakon;
    private ArrayList<LakonRecycleViewModel> lakonArrayList;

    private EditText searchEditText;
    private ProgressBar dashboardProgressbar;
    private boolean init = true;

    private String TAG = "===== Search Activity";



    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public SearchFragment() {
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment SearchFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SearchFragment newInstance(String param1) {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        fragment.mParam1 = param1;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        dashboardProgressbar = getView().findViewById(R.id.fragment_search_progressbar);
        notFound = getView().findViewById(R.id.fragment_seach_list_not_found);
        setRetainInstance(true);

        addData(mParam1);
        initLakonRecycleView();


        searchEditText = (EditText) getView().findViewById(R.id.edit_text_field_search);
        searchEditText.setText(mParam1);
        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null) {
                    addData(s.toString());

                } else {

                }
                adapterLakon.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    void initLakonRecycleView(){
        recyclerViewLakon = (RecyclerView) getView().findViewById(R.id.search_recycler_view);

        adapterLakon = new LakonRecycleViewAdapter(lakonArrayList , getActivity());

//      RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(),2);

        recyclerViewLakon.setNestedScrollingEnabled(false);

        recyclerViewLakon.setLayoutManager(gridLayoutManager);

        recyclerViewLakon.setAdapter(adapterLakon);
    }

    void addData(String searchString){
        notFound.setVisibility(View.GONE);
        lakonArrayList = new ArrayList<>();
        AndroidNetworking.post(getString(R.string.web_search))
                .addHeaders("key","12345")
                .addBodyParameter("cari", searchString)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "onResponse: success" + response.toString());

                        try{
                            if(response.get("status").equals("failed")){
                                Log.w(TAG, "onResponse succes : " + response.toString() );
                                dashboardProgressbar.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), "" + response.getString("msg"), Toast.LENGTH_SHORT).show();
                            } else if(response.get("status").equals("success")){
                                String tmp = response.getJSONArray("data").get(0).toString();
                                Log.w(TAG, "onResponse succes : "+ tmp );

                                Log.w(TAG, "onResponse succes : data nol " +response.getJSONArray("data").length());

                                lakonArrayList = new ArrayList<>();
                                if(response.getJSONArray("data").length() == 0){
                                    Log.w(TAG, "onResponse succes : data nol" );
                                    notFound.setVisibility(View.VISIBLE);
                                    return;
                                }
                                else if(response.getJSONArray("data").length() > 0){
                                    for (int i = 0; i <response.getJSONArray("data").length() ; i++) {
                                        if(!response.getJSONArray("data").getJSONObject(i).getString("id").equals(CurrentUserModel.getCurrentUserModel().getId()))
                                            lakonArrayList.add(new LakonRecycleViewModel(
                                                    response.getJSONArray("data").getJSONObject(i).getString("id"),
                                                    response.getJSONArray("data").getJSONObject(i).getString("nama"),
                                                    response.getJSONArray("data").getJSONObject(i).getString("email"),
                                                    response.getJSONArray("data").getJSONObject(i).getString("deskripsi"),
                                                    response.getJSONArray("data").getJSONObject(i).getString("foto_url"),
                                                    response.getJSONArray("data").getJSONObject(i).getString("id_layanan"),
                                                    response.getJSONArray("data").getJSONObject(i).getString("nama_layanan"),
                                                    response.getJSONArray("data").getJSONObject(i).getString("harga"),
                                                    response.getJSONArray("data").getJSONObject(i).getString("nama_kategori"),
                                                    response.getJSONArray("data").getJSONObject(i).getString("status_promo"),
                                                    response.getJSONArray("data").getJSONObject(i).getString("harga_promo"),
                                                    response.getJSONArray("data").getJSONObject(i).getString("rating_user")));
                                    }
                                }
                                adapterLakon.addItem(lakonArrayList);
                                adapterLakon.notifyDataSetChanged();

                                dashboardProgressbar.setVisibility(View.GONE);

                            }
                        }catch (JSONException e){
                            dashboardProgressbar.setVisibility(View.GONE);
                            notFound.setVisibility(View.VISIBLE);


                            lakonArrayList = new ArrayList<>();
                            adapterLakon.addItem(lakonArrayList);
                            adapterLakon.notifyDataSetChanged();

                            if(init){
                                Toast.makeText(getActivity(), "Tidak memiliki lakon dengan kategori " + mParam1, Toast.LENGTH_SHORT).show();
                                init = false;
                            }
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(TAG, "onResponse error: " + anError.toString() );
                        dashboardProgressbar.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "" + anError.getErrorDetail(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }



    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
