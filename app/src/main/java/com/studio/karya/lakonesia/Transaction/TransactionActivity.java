package com.studio.karya.lakonesia.Transaction;

import android.content.Intent;

import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.studio.karya.lakonesia.Profile.ProfileFragment;
import com.studio.karya.lakonesia.R;
import com.studio.karya.lakonesia.Utils.PublicString;

import java.util.List;


public class TransactionActivity extends AppCompatActivity {

    private String id,nama,email,foto_url , deskripsi;
    private String TAG = "===== Transaction Activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction);

        id = getIntent().getStringExtra(PublicString.TRANSACTION_LAKON_ID);
        nama = getIntent().getStringExtra(PublicString.TRANSACTION_LAKON_NAMA);
        email = getIntent().getStringExtra(PublicString.TRANSACTION_LAKON_EMAIL);
        foto_url = getIntent().getStringExtra(PublicString.TRANSACTION_LAKON_FOTO_URL);
        deskripsi = getIntent().getStringExtra(PublicString.TRANSACTION_LAKON_DESKRIPSI);

        Log.w(TAG, "onCreate id pelakon: "+ id );
        Log.w(TAG, "onCreate nama : "+ nama );
        Log.w(TAG, "onCreate email : "+ email );

        ProfileFragment firstFragment = ProfileFragment.newProfileInstance(id , nama , email , PublicString.ROLE_LAKON , foto_url , deskripsi);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.transaction_activity_frame_layout, firstFragment).commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {

            List<Fragment> fragments = getSupportFragmentManager().getFragments();
            if (fragments != null) {
                for (Fragment fragment : fragments) {
                    fragment.onActivityResult(requestCode, resultCode, data);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
