package com.studio.karya.lakonesia.Transaction;

import com.studio.karya.lakonesia.HargaFragment.HargaModel;

public class TransactionModel extends HargaModel {
    private int kuantitas;

    public int getKuantitas() {
        return kuantitas;
    }

    public void setKuantitas(int kuantitas) {
        this.kuantitas = kuantitas;
    }

    public TransactionModel(String id_layanan, String id_pelakon, String nama_layanan, String deskripsi, String harga, String status_promo,String harga_promo, int kuantitas) {
        super(id_layanan, id_pelakon,  nama_layanan, deskripsi,  harga,  status_promo , harga_promo);
        this.kuantitas = kuantitas;
    }

    public void tambah(){
        this.kuantitas++;
    }

    public void kurang(){
        this.kuantitas--;
    }
}
