package com.studio.karya.lakonesia.Model;

import android.util.Log;

import com.pixplicity.easyprefs.library.Prefs;
import com.studio.karya.lakonesia.Utils.PublicString;


public class CurrentUserModel {
    private String nama , id , email , role , foto_url , deskripsi , id_kategori , nama_kategori;

    private static CurrentUserModel instance;

    public CurrentUserModel(String id, String nama, String email, String role, String foto_url, String deskripsi, String id_kategori, String nama_kategori) {
        this.nama = nama;
        this.id = id;
        this.email = email;
        this.role = role;
        this.foto_url = foto_url;
        this.deskripsi = deskripsi;
        this.id_kategori = id_kategori;
        this.nama_kategori = nama_kategori;
    }

    public void saveToPref(){

    }

    public String getId_kategori() {
        return id_kategori;
    }

    public void setId_kategori(String id_kategori) {
        this.id_kategori = id_kategori;
        Prefs.putString(PublicString.CUREENT_USER_ID_KATEGORI , id_kategori);
    }

    public String getNama_kategori() {
        return nama_kategori;
    }

    public void setNama_kategori(String nama_kategori) {
        this.nama_kategori = nama_kategori;
        Prefs.putString(PublicString.CUREENT_USER_NAMA_KATEGORI , nama_kategori);
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
        Prefs.putString(PublicString.CUREENT_USER_DESKRIPSI,deskripsi);
    }

    public static CurrentUserModel getCurrentUserModel() {
        if(instance != null)
            return instance;
        else {

            String id = Prefs.getString(PublicString.CUREENT_USER_ID,null);
            String email = Prefs.getString(PublicString.CUREENT_USER_EMAIL,null);
            String nama = Prefs.getString(PublicString.CUREENT_USER_NAMA,null);
            String foto_url = Prefs.getString(PublicString.CUREENT_USER_FOTO_URL,null);
            String role = Prefs.getString(PublicString.CUREENT_USER_ROLE,null);
            String des = Prefs.getString(PublicString.CUREENT_USER_DESKRIPSI,null);
            String id_kategori = Prefs.getString(PublicString.CUREENT_USER_ID_KATEGORI,null);
            String nama_kategori = Prefs.getString(PublicString.CUREENT_USER_NAMA_KATEGORI,null);
            instance = new CurrentUserModel(
                    id , nama , email , role , foto_url , des , id_kategori , nama_kategori
            );
            return instance;
        }
    }

    public static void setCurrentUserModel(String id, String nama, String email, String role , String foto_url , String deskripsi , String id_kategori , String nama_kategori) {

        Prefs.putString(PublicString.CUREENT_USER_ID,id);
        Prefs.putString(PublicString.CUREENT_USER_EMAIL,email);
        Prefs.putString(PublicString.CUREENT_USER_NAMA,nama);
        Prefs.putString(PublicString.CUREENT_USER_ROLE,role);
        Prefs.putString(PublicString.CUREENT_USER_FOTO_URL,foto_url);
        Prefs.putString(PublicString.CUREENT_USER_DESKRIPSI,deskripsi);
        Prefs.putString(PublicString.CUREENT_USER_ID_KATEGORI,id_kategori);
        Prefs.putString(PublicString.CUREENT_USER_NAMA_KATEGORI,nama_kategori);
        instance =  new CurrentUserModel(id , nama ,email , role , foto_url , deskripsi , id_kategori , nama_kategori);
    }

    public static void deleteCurentUserModel(){
        Prefs.remove(PublicString.CUREENT_USER_ID);
        Prefs.remove(PublicString.CUREENT_USER_EMAIL);
        Prefs.remove(PublicString.CUREENT_USER_NAMA_KATEGORI);
        Prefs.remove(PublicString.CUREENT_USER_ROLE);
        Prefs.remove(PublicString.CUREENT_USER_FOTO_URL);
        Prefs.remove(PublicString.CUREENT_USER_DESKRIPSI);
        Prefs.remove(PublicString.CUREENT_USER_ID_KATEGORI);
        Prefs.remove(PublicString.CUREENT_USER_NAMA_KATEGORI);
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
        Prefs.putString(PublicString.CUREENT_USER_NAMA,nama);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getFoto_url() {
        Log.w("===== Current User", "getFoto_url: "+ foto_url );
        return foto_url;
    }

    public void setFoto_url(String foto_url) {
        this.foto_url = foto_url;
    }
}
