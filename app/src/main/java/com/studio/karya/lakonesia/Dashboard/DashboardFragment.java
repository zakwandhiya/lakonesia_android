package com.studio.karya.lakonesia.Dashboard;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipDrawable;
import com.google.android.material.chip.ChipGroup;
import com.studio.karya.lakonesia.MainActivity;
import com.studio.karya.lakonesia.Model.CurrentUserModel;
import com.studio.karya.lakonesia.Model.GaleryModel;
import com.studio.karya.lakonesia.Model.KategoriModel;
import com.studio.karya.lakonesia.Profile.GaleryAdapter;
import com.studio.karya.lakonesia.Profile.UploadImageActivity;
import com.studio.karya.lakonesia.QRCodeActivity;
import com.studio.karya.lakonesia.R;
import com.studio.karya.lakonesia.SearchFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.facebook.FacebookSdk.getApplicationContext;

public class DashboardFragment extends Fragment {
    private static final String USER_ID = "USER_ID";
    private static final String USER_ROLE = "USER_ROLE";

    private RecyclerView recyclerViewLakon;
    private LakonRecycleViewAdapter adapterLakon;
    private ArrayList<LakonRecycleViewModel> lakonArrayList;

    private RecyclerView recyclerViewPopular;
    private LakonRecycleViewAdapter adapterPopular;
    private ArrayList<LakonRecycleViewModel> popularArrayList;


    private RecyclerView recyclerViewRekomendasi;
    private LakonRecycleViewAdapter adapterRekomendasi;
    private ArrayList<LakonRecycleViewModel> rekomendasiArrayList;

    private RecyclerView recyclerViewKategori;
    private KategoriRecycleViewAdapter adapterKategori;
    private ArrayList<KategoriModel> kategoriArrayList;

    private ViewPager pagerViewIklan;
    private IklanPagerViewAdapter adapterIklan;

    private List<IklanModels> iklanArrayList;

    private LinearLayout dotsLayout;
    private int position_dots = 0;

    private EditText searchEditText;
    private ProgressBar dashboardProgressbar;


    private GridLayoutManager gridLayoutManager;
    private RecyclerView recyclerView;
    private SubkategoriAdapter galeryAdapter;
    private ArrayList<GaleryModel> listGalery  = new ArrayList<>();

    private String TAG = "===== Dashboard Activity";

    private String mId;
    private String mRole;

    private ChipGroup chipGroup;

    private OnFragmentInteractionListener mListener;

    public DashboardFragment() {
    }

    public static DashboardFragment newInstance(String mId, String mRole) {
        DashboardFragment fragment = new DashboardFragment();
        Bundle args = new Bundle();
        args.putString(USER_ID, mId);
        args.putString(USER_ROLE, mRole);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        gridLayoutManager = new GridLayoutManager(getContext(), 2);
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mId = getArguments().getString(USER_ID);
            mRole = getArguments().getString(USER_ROLE);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dashboard, container, false);
    }

    void onEvent(int data) {
        Log.d("Event", "Berhasilll");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.w(TAG, "onActivityCreated: fragment dashboard" );
        dashboardProgressbar = getView().findViewById(R.id.fragment_dashboard_progressbar);
        kategoriArrayList = new ArrayList<KategoriModel>();

        dotsLayout = getView().findViewById(R.id.dashboard_iklan_dots_view);
        setRetainInstance(true);
        chipGroup = getView().findViewById(R.id.fragment_dashboard_chip_subkategori);

        final int sdk = android.os.Build.VERSION.SDK_INT;


        addData("");
        addKategori();
        addIklan();
        addPopular();
        addRekomend();
        addKategori();
        addSubkategori();

        initSubkategori();
        initRekomendasiRecycleView();
        initPopularRecycleView();
        initKategoriRecycleView();
        initLakonRecycleView();
        initIklanRecycleView();

//        IklanDots(position_dots++);

        ImageView toQrActivity = getView().findViewById(R.id.fragment_dashboard_to_qr_button);
        toQrActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), QRCodeActivity.class);
                startActivity(intent);
            }
        });

        adapterKategori.addItem(kategoriArrayList);
        adapterKategori.notifyDataSetChanged();

        searchEditText = (EditText) getView().findViewById(R.id.edit_text_dashboard_search);

        searchEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).replaceFragment(1 , SearchFragment.newInstance(""));
            }
        });
        searchEditText.setFocusable(false);
//        searchEditText.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                if (s != null) {
//                    addData(s.toString());
//
//                } else {
//
//                }
//                adapterLakon.notifyDataSetChanged();
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
    }

    void initLakonRecycleView(){
        recyclerViewLakon = (RecyclerView) getView().findViewById(R.id.dashboard_recycler_view);

        adapterLakon = new LakonRecycleViewAdapter(lakonArrayList , getActivity());
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(),2);

        //Determine screen size
        if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE) {
            gridLayoutManager = new GridLayoutManager(getApplicationContext(),2);
        }
        else if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
            gridLayoutManager = new GridLayoutManager(getApplicationContext(),2);
        }
        else if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_SMALL) {
            gridLayoutManager = new GridLayoutManager(getApplicationContext(),2);
        }
        else {
            //Bigger
            gridLayoutManager = new GridLayoutManager(getApplicationContext(),3);
        }

        recyclerViewLakon.setNestedScrollingEnabled(false);

        recyclerViewLakon.setLayoutManager(gridLayoutManager);

        recyclerViewLakon.setAdapter(adapterLakon);

    }

    void initKategoriRecycleView(){
        recyclerViewKategori = (RecyclerView) getView().findViewById(R.id.dashboard_kategori_recycler_view);

        adapterKategori = new KategoriRecycleViewAdapter(kategoriArrayList , ((MainActivity) getActivity()));

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(),5);

        //RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity() , LinearLayoutManager.HORIZONTAL , false);

        //Log.w(TAG, "initKategoriRecycleView: "+ recyclerViewKategori.toString() );

        recyclerViewKategori.setLayoutManager(gridLayoutManager);

        recyclerViewKategori.setAdapter(adapterKategori);

    }


    void initPopularRecycleView(){
        Log.i(TAG, "initPopularRecycleView: " + popularArrayList.size());
        recyclerViewPopular= (RecyclerView) getView().findViewById(R.id.dashboard_popular_recycler_view);

        adapterPopular = new LakonRecycleViewAdapter(popularArrayList, ((MainActivity) getActivity()));

//        GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(),5);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity() , LinearLayoutManager.HORIZONTAL , false);


        recyclerViewPopular.setLayoutManager(layoutManager);

        recyclerViewPopular.setAdapter(adapterPopular);

    }


    void initRekomendasiRecycleView(){
        Log.i(TAG, "initPopularRecycleView: " + rekomendasiArrayList.size());
        recyclerViewRekomendasi= (RecyclerView) getView().findViewById(R.id.dashboard_recomended_recycler_view);

        adapterRekomendasi= new LakonRecycleViewAdapter(rekomendasiArrayList, ((MainActivity) getActivity()));

//        GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(),5);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity() , LinearLayoutManager.HORIZONTAL , false);


        recyclerViewRekomendasi.setLayoutManager(layoutManager);

        recyclerViewRekomendasi.setAdapter(adapterRekomendasi);

    }

    void initSubkategori(){
        gridLayoutManager = new GridLayoutManager(getContext(), 2);

        recyclerView = getView().findViewById(R.id.fragment_dashboard_grid_subkategori);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(gridLayoutManager);
        addSubkategori();
    }

    void initIklanRecycleView(){
        adapterIklan = new IklanPagerViewAdapter(iklanArrayList, getActivity());

        pagerViewIklan = getView().findViewById(R.id.dashboard_iklan_recycler_view);

        pagerViewIklan.setAdapter(adapterIklan);
        pagerViewIklan.setPadding(0, 0 , 48,0);
        pagerViewIklan.setClipToPadding(false);
        pagerViewIklan.setPageMargin(0);

        pagerViewIklan.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                IklanDots(position);
                position_dots = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if(position_dots == adapterIklan.getCount())
                    position_dots=0;
                pagerViewIklan.setCurrentItem(position_dots++, true);
            }
        };

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(runnable);
            }
        }, 250,2500);
    }

    void addPopular(){
        popularArrayList = new ArrayList<>();
        AndroidNetworking.get(getString(R.string.web_get_popular))
                .addHeaders("key",getString(R.string.api_key))
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "onResponse popular lakon : success" + response.toString());

                        try{
                            if(response.get("status").equals("failed")){
                                Log.w(TAG, "onResponse failed : "+ response.toString() );
                                dashboardProgressbar.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), "" + response.getString("msg"), Toast.LENGTH_SHORT).show();
                            } else if(response.get("status").equals("success")){

                                for (int i = 0; i <response.getJSONArray("data").length() ; i++) {

                                    String tmp = response.getJSONArray("data").get(i).toString();
                                    Log.w(TAG, "onResponse popular: "+ tmp );
                                    popularArrayList.add(new LakonRecycleViewModel(
                                            response.getJSONArray("data").getJSONObject(i).getString("id"),
                                            response.getJSONArray("data").getJSONObject(i).getString("nama"),
                                            response.getJSONArray("data").getJSONObject(i).getString("email"),
                                            response.getJSONArray("data").getJSONObject(i).getString("deskripsi"),
                                            response.getJSONArray("data").getJSONObject(i).getString("foto_url"),
                                            response.getJSONArray("data").getJSONObject(i).getString("id_layanan"),
                                            response.getJSONArray("data").getJSONObject(i).getString("nama_layanan"),
                                            response.getJSONArray("data").getJSONObject(i).getString("harga"),
                                            response.getJSONArray("data").getJSONObject(i).getString("nama_kategori"),
                                            response.getJSONArray("data").getJSONObject(i).getString("status_promo"),
                                            response.getJSONArray("data").getJSONObject(i).getString("harga_promo"),
                                            response.getJSONArray("data").getJSONObject(i).getString("rating_user")

                                    ));
                                }

                                adapterPopular.addItem( popularArrayList);
                                adapterPopular.notifyDataSetChanged();

                                dashboardProgressbar.setVisibility(View.GONE);


                            }
                        }catch (JSONException e){
                            Log.w(TAG, "onResponse json error : "+ e.getMessage() );
                            dashboardProgressbar.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), "Terjadi kesalahan pada server", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d("FAN", "onResponse: " + anError.toString() );
                        dashboardProgressbar.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "" + anError.getErrorDetail(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    void addSubkategori(){
        AndroidNetworking.get(getString(R.string.web_get_sub))
                .addHeaders("key",getString(R.string.api_key))
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            if(response.get("status").equals("failed")){
                                Toast.makeText(getContext(), "" + response.get("msg"), Toast.LENGTH_SHORT).show();
                            } else if(response.get("status").equals("success")){
                                listGalery.clear();
                                for (int i = 0; i < response.getJSONArray("data").length(); i++) {
                                    try {

                                        View view = getLayoutInflater().inflate(R.layout.subkategori_chip, chipGroup, false);
                                        Chip chip = view.findViewById(R.id.chips_item_filter);
                                        final String nama = response.getJSONArray("data").getString(i);
                                        chip.setText(nama);
                                        chip.setClickable(true);
                                        chip.setChipBackgroundColor(ColorStateList.valueOf(ContextCompat.getColor(getContext(), R.color.less_grey)));
                                        chip.setChipStrokeColor(ColorStateList.valueOf(ContextCompat.getColor(getContext(), R.color.less_grey)));
                                        chipGroup.addView(chip);

                                        chip.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                ((MainActivity)getActivity()).replaceFragment(1 , SearchFragment.newInstance(nama));
                                            }
                                        });
                                    } catch (JSONException e){
                                        Toast.makeText(getActivity(), "Terjadi kesalahan pada server", Toast.LENGTH_SHORT).show();
                                    }

                                }
//                                for (int i = 0; i <response.getJSONArray("data").length() ; i++) {
//                                    try {
//                                        String nama = response.getJSONArray("data").getString(i);
//                                        GaleryModel galeryModel = new GaleryModel(
//                                                nama,
//                                                nama,
//                                                nama
//                                        );
//                                        listGalery.add(galeryModel);
//
//                                        galeryAdapter = new SubkategoriAdapter(listGalery , (MainActivity) getActivity());
//                                        recyclerView.setAdapter(galeryAdapter);
//                                    } catch (JSONException e){
//                                        Toast.makeText(getActivity(), "Terjadi kesalahan pada server", Toast.LENGTH_SHORT).show();
//                                    }
//
//                                }
                            }
                        }catch (JSONException e){
                            Toast.makeText(getActivity(), "Terjadi kesalahan pada server", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(getContext(), "Terjadi kesalahan pada server", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    void addRekomend(){
        rekomendasiArrayList = new ArrayList<>();
        AndroidNetworking.get(getString(R.string.web_get_recomended))
                .addHeaders("key",getString(R.string.api_key))
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "onResponse popular lakon : success" + response.toString());

                        try{
                            if(response.get("status").equals("failed")){
                                Log.w(TAG, "onResponse failed : "+ response.toString() );
                                dashboardProgressbar.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), "" + response.getString("msg"), Toast.LENGTH_SHORT).show();
                            } else if(response.get("status").equals("success")){

                                for (int i = 0; i <response.getJSONArray("data").length() ; i++) {

                                    String tmp = response.getJSONArray("data").get(i).toString();
                                    Log.w(TAG, "onResponse popular: "+ tmp );
                                    rekomendasiArrayList.add(new LakonRecycleViewModel(
                                            response.getJSONArray("data").getJSONObject(i).getString("id"),
                                            response.getJSONArray("data").getJSONObject(i).getString("nama"),
                                            response.getJSONArray("data").getJSONObject(i).getString("email"),
                                            response.getJSONArray("data").getJSONObject(i).getString("deskripsi"),
                                            response.getJSONArray("data").getJSONObject(i).getString("foto_url"),
                                            response.getJSONArray("data").getJSONObject(i).getString("id_layanan"),
                                            response.getJSONArray("data").getJSONObject(i).getString("nama_layanan"),
                                            response.getJSONArray("data").getJSONObject(i).getString("harga"),
                                            response.getJSONArray("data").getJSONObject(i).getString("nama_kategori"),
                                            response.getJSONArray("data").getJSONObject(i).getString("status_promo"),
                                            response.getJSONArray("data").getJSONObject(i).getString("harga_promo"),
                                            response.getJSONArray("data").getJSONObject(i).getString("rating_user")

                                    ));
                                }

                                adapterRekomendasi.addItem( rekomendasiArrayList);
                                adapterRekomendasi.notifyDataSetChanged();

                                dashboardProgressbar.setVisibility(View.GONE);


                            }
                        }catch (JSONException e){
                            Log.w(TAG, "onResponse json error : "+ e.getMessage() );
                            dashboardProgressbar.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), "Terjadi kesalahan pada server", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d("FAN", "onResponse: " + anError.toString() );
                        dashboardProgressbar.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "" + anError.getErrorDetail(), Toast.LENGTH_SHORT).show();
                    }
                });
    }


    void addIklan(){
        iklanArrayList = new ArrayList<>();
        AndroidNetworking.get(getString(R.string.web_get_iklan))
                .addHeaders("key",getString(R.string.api_key))
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "onResponse: success" + response.toString());

                        try{
                            if(response.get("status").equals("failed")){
                                Log.w(TAG, "onResponse failed : "+ response.toString() );
                                dashboardProgressbar.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), "" + response.getString("msg"), Toast.LENGTH_SHORT).show();
                            } else if(response.get("status").equals("success")){
                                String tmp = response.getJSONArray("data").get(0).toString();
                                Log.w(TAG, "onResponse: "+ tmp );

                                for (int i = 0; i <response.getJSONArray("data").length() ; i++) {
                                    iklanArrayList.add(new IklanModels(
                                            response.getJSONArray("data").getJSONObject(i).getString("url_iklan"),
                                            response.getJSONArray("data").getJSONObject(i).getString("id_iklan"),
                                            response.getJSONArray("data").getJSONObject(i).getString("nama"),
                                            response.getJSONArray("data").getJSONObject(i).getString("email"),
                                            response.getJSONArray("data").getJSONObject(i).getString("foto_url"),
                                            response.getJSONArray("data").getJSONObject(i).getString("deskripsi")

                                    ));
                                    Log.d("Jsonnya adalah", response.getJSONArray("data").getJSONObject(i).getString("id_iklan"));
                                }

                                adapterIklan.addItem((ArrayList<IklanModels>) iklanArrayList);
                                adapterKategori.notifyDataSetChanged();

                                dashboardProgressbar.setVisibility(View.GONE);

                            }
                        }catch (JSONException e){
                            Log.w(TAG, "onResponse json error : "+ e.getMessage() );
                            dashboardProgressbar.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), "Terjadi kesalahan pada server", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d("FAN", "onResponse: " + anError.toString() );
                        dashboardProgressbar.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "" + anError.getErrorDetail(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    void addKategori(){
        kategoriArrayList = new ArrayList<>();
        String id[] = {"1", "2", "3", "4", "5"};
        String nama[] = {"Kesehatan", "Otomotif", "Pendidikan", "Seni", "Traveling"};
        int image[] = {R.drawable.love, R.drawable.wrench, R.drawable.book, R.drawable.image, R.drawable.place};

        for(int i = 0; i < id.length ; i++){
            kategoriArrayList.add(new KategoriModel(id[i],nama[i], image[i]));
        }
//        OLD Kategori
//        kategoriArrayList = new ArrayList<>();
//
//        AndroidNetworking.get(getString(R.string.web_search_kategori2))
//                .addHeaders("key",getString(R.string.api_key))
//                .setPriority(Priority.MEDIUM)
//                .build()
//                .getAsJSONObject(new JSONObjectRequestListener() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        Log.d(TAG, "onResponse: success" + response.toString());
//
//                        try{
//                            if(response.get("status").equals("failed")){
//                                Toast.makeText(getActivity(), "" + response.getString("msg"), Toast.LENGTH_SHORT).show();
//                                dashboardProgressbar.setVisibility(View.GONE);
//                            } else if(response.get("status").equals("success")){
//                                String tmp = response.getJSONArray("data").get(0).toString();
//                                Log.w(TAG, "onResponse: "+ tmp );
//
//
//                                kategoriArrayList = new ArrayList<>();
//                                for (int i = 0; i <response.getJSONArray("data").length() ; i++) {
//                                    kategoriArrayList.add(new KategoriModel(
//                                            response.getJSONArray("data").getJSONObject(i).getString("id_kategori"),
//                                            response.getJSONArray("data").getJSONObject(i).getString("nama"),
//                                            response.getJSONArray("data").getJSONObject(i).getString("foto_url")
//                                    ));
//                                }
//                                adapterKategori.addItem(kategoriArrayList);
//                                adapterKategori.notifyDataSetChanged();
//
//                                dashboardProgressbar.setVisibility(View.GONE);
//
//                                KategoriModel.setKategoriModels(kategoriArrayList);
//
//                            }
//                        }catch (JSONException e){
//                            Log.w(TAG, "onResponse json error : "+ e.getMessage() );
//                            dashboardProgressbar.setVisibility(View.GONE);
//                            Toast.makeText(getActivity(), "Terjadi kesalahan pada server", Toast.LENGTH_SHORT).show();
//                        }
//                    }
//
//                    @Override
//                    public void onError(ANError anError) {
//                        Log.d("FAN", "onResponse: " + anError.toString() );
//                        dashboardProgressbar.setVisibility(View.GONE);
//                        Toast.makeText(getActivity(), "" + anError.getErrorDetail(), Toast.LENGTH_SHORT).show();
//                    }
//                });
    }

    void addData(String searchString){
        lakonArrayList = new ArrayList<>();
        AndroidNetworking.post(getString(R.string.web_search))
                .addHeaders("key","12345")
                .addBodyParameter("cari", searchString)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "onResponse: success" + response.toString());

                        try{
                            if(response.get("status").equals("failed")){
                                Log.w(TAG, "onResponse succes : " + response.toString() );
                                dashboardProgressbar.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), "" + response.getString("msg"), Toast.LENGTH_SHORT).show();
                            } else if(response.get("status").equals("success")){
                                String tmp = response.getJSONArray("data").get(0).toString();
                                Log.w(TAG, "onResponse succes : "+ tmp );

                                lakonArrayList = new ArrayList<>();
                                for (int i = 0; i <response.getJSONArray("data").length() ; i++) {
                                    if(!response.getJSONArray("data").getJSONObject(i).getString("id").equals(CurrentUserModel.getCurrentUserModel().getId()))
                                    lakonArrayList.add(new LakonRecycleViewModel(
                                            response.getJSONArray("data").getJSONObject(i).getString("id"),
                                            response.getJSONArray("data").getJSONObject(i).getString("nama"),
                                            response.getJSONArray("data").getJSONObject(i).getString("email"),
                                            response.getJSONArray("data").getJSONObject(i).getString("deskripsi"),
                                            response.getJSONArray("data").getJSONObject(i).getString("foto_url"),
                                            response.getJSONArray("data").getJSONObject(i).getString("id_layanan"),
                                            response.getJSONArray("data").getJSONObject(i).getString("nama_layanan"),
                                            response.getJSONArray("data").getJSONObject(i).getString("harga"),
                                            response.getJSONArray("data").getJSONObject(i).getString("nama_kategori"),
                                            response.getJSONArray("data").getJSONObject(i).getString("status_promo"),
                                            response.getJSONArray("data").getJSONObject(i).getString("harga_promo"),
                                            response.getJSONArray("data").getJSONObject(i).getString("rating_user")
                                    ));
                                }
                                adapterLakon.addItem(lakonArrayList);
                                adapterLakon.notifyDataSetChanged();

                                dashboardProgressbar.setVisibility(View.GONE);

                            }
                        }catch (JSONException e){

                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

     void IklanDots(int currentSlidePosition){
        if(dotsLayout.getChildCount() > 0){
            dotsLayout.removeAllViews();
        }

        ImageView dots[] = new ImageView[7];

        for(int i = 0; i < 7 ; i++){
            dots[i] = new ImageView(this.dotsLayout.getContext());
            if(i==currentSlidePosition)
                dots[i].setImageDrawable(ContextCompat.getDrawable(this.dotsLayout.getContext(), R.drawable.active_dots ));
            else
                dots[i].setImageDrawable(ContextCompat.getDrawable(this.dotsLayout.getContext(), R.drawable.inactive_dots ));

            LinearLayout.LayoutParams layoutParams= new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(4,0,4,0);
            dotsLayout.addView(dots[i], layoutParams);
        }
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
