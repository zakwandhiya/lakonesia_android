package com.studio.karya.lakonesia.History.HitoryDetail;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.stepstone.apprating.AppRatingDialog;
import com.stepstone.apprating.listener.RatingDialogListener;
import com.studio.karya.lakonesia.History.HistoryList.HistoryTransactionLayananListModel;
import com.studio.karya.lakonesia.History.HistoryList.HistoryTransactionListModel;
import com.studio.karya.lakonesia.MainActivity;
import com.studio.karya.lakonesia.Model.CurrentUserModel;
import com.studio.karya.lakonesia.R;
import com.studio.karya.lakonesia.Utils.PublicString;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;

public class HistoryDetailFragment extends Fragment implements RatingDialogListener {
    private static final String TRANSACTION_ID = "TRANSACTION_ID";
    private static final String TRANSACTION_AVAILABLE = "TRANSACTION_AVAILABLE";

    private String mId;
    private boolean mAvailable;

    private String TAG = "===== History Detail";

    private RecyclerView recyclerViewDetailNegoTransaksi;
    private HistoryTransactionListModel mHistoryModel;
    private ArrayList<HistoryDetailTransactionNegoModel> negoModels;
    private HistoryDetailTransactionNegoRecycleViewAdapter adapterNego;
    private RecyclerView.LayoutManager layoutManager;

    public HistoryDetailTransactionNegoRecycleViewAdapter historyDetailAdapter;

    private TextView txtNamaLakon, txtDate, txtTime , txtNote ,txtLocation , txtTotal , txtAdminfee , txtPrice , txtPaymentWith ;
    private RecyclerView layananRecycleView;
    private LinearLayout mainDetailLayout;
    private Button tolak,terima,nego;
    private SwipeRefreshLayout swipeRefreshLayout;

    private OnFragmentInteractionListener mListener;

    public HistoryDetailFragment() {

    }
    public static HistoryDetailFragment newInstance(String id , HistoryTransactionListModel tmp , boolean available) {
        HistoryDetailFragment fragment = new HistoryDetailFragment();
        Bundle args = new Bundle();
        args.putString(TRANSACTION_ID, id);
        args.putBoolean(TRANSACTION_AVAILABLE, available);
        fragment.setArguments(args);
        fragment.mHistoryModel = tmp;
        return fragment;
    }

    public static HistoryDetailFragment newInstance(String id) {
        HistoryDetailFragment fragment = new HistoryDetailFragment();
        Bundle args = new Bundle();
        args.putString(TRANSACTION_ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.w(TAG, "onActivityCreated: " );
        initNegoRecycleView();
        mainDetailLayout = getView().findViewById(R.id.fragment_history_detail_main_transaction_detail);

        if(mHistoryModel != null){
            initView();
        }

        getDetailTransaksi(mId);
        swipeRefreshLayout = getView().findViewById(R.id.fragment_history_detail_swipe_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getNego(mId);
            }
        });


    }

    void initView(){
        Log.w(TAG, "initView: nama lakon" + mHistoryModel.getNama_lakon() );

        mainDetailLayout.setVisibility(View.VISIBLE);
        tolak = getView().findViewById(R.id.btn_tolak);
        terima = getView().findViewById(R.id.btn_terima);
        nego = getView().findViewById(R.id.btn_nego);
        txtNamaLakon = getView().findViewById(R.id.fragment_history_recycle_nama_lakon);
        txtDate = getView().findViewById(R.id.fragment_history_recycle_date_text);
        txtTime = getView().findViewById(R.id.fragment_history_recycle_time_text);
        txtNote = getView().findViewById(R.id.fragment_history_recycle_note_text);
        txtLocation = getView().findViewById(R.id.fragment_history_recycle_location_text);

        txtTotal = getView().findViewById(R.id.fragment_history_recycle_total_num_text);
        txtAdminfee = getView().findViewById(R.id.fragment_history_recycle_admin_fee_num_text);
//        txtPrice = getView().findViewById(R.id.fragment_history_recycle_price_num_text);
        txtPaymentWith =  getView().findViewById(R.id.fragment_history_recycle_payment_with_value_text);
        layananRecycleView = getView().findViewById(R.id.transaction_recycle_view);

        //==========================================================================================
        //==========================================================================================
        //==========================================================================================

        txtNamaLakon.setText(mHistoryModel.getNama_member());
        txtDate.setText(mHistoryModel.getDate());
        txtTime.setText(mHistoryModel.getTime());
        txtLocation.setText(mHistoryModel.getAlamat());
        txtNote.setText(mHistoryModel.getNote());
        int total = Integer.parseInt(mHistoryModel.getTotal())+Integer.parseInt(mHistoryModel.getAdmin_fee());
        txtTotal.setText("RP "+total);
//        txtPrice.setText("RP "+mHistoryModel.getPrice());
        txtAdminfee.setText("RP "+mHistoryModel.getAdmin_fee());
        txtPaymentWith.setText(mHistoryModel.getPayment());
//        initButton();

        //==========================================================================================
        //==========================================================================================
        //==========================================================================================
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layananRecycleView.setLayoutManager(layoutManager);
        layananRecycleView.setNestedScrollingEnabled(false);
        HistoryDetailTransactionLayananRecycleViewAdapter historyDetailLayananAdapter = new HistoryDetailTransactionLayananRecycleViewAdapter(mHistoryModel.getList_layanan() , getActivity());
        layananRecycleView.setAdapter(historyDetailLayananAdapter);
        historyDetailLayananAdapter.addItem(mHistoryModel.getList_layanan());
        historyDetailLayananAdapter.notifyDataSetChanged();

        tolak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTolakDialog();
            }
        });
        terima.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTerimaDialog();
            }
        });
        nego.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNegoDialog();
            }
        });
    }

//    void initButton(){
//        mainDetailButton = getView().findViewById(R.id.fragment_history_detail_main_detail_button);
//        if(CurrentUserModel.getCurrentUserModel().getRole().equals(PublicString.ROLE_MEMBER) && mHistoryModel.getStatus().equals("disetujui")){
//            mainDetailButton.setText("Chat Whatsapp");
//            mainDetailButton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    String contact = PublicString.getPhoneNumber();
//                    String url = "https://api.whatsapp.com/send?phone=" + contact;
//                    if(contact != null){
//                        try {
//                            PackageManager pm = getContext().getPackageManager();
//                            pm.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES);
//                            Intent i = new Intent(Intent.ACTION_VIEW);
//                            i.setData(Uri.parse(url));
//                            startActivity(i);
//                        } catch (PackageManager.NameNotFoundException e) {
//                            Toast.makeText(getActivity(), "Whatsapp tidak terinstall di ponsel anda", Toast.LENGTH_SHORT).show();
//                            e.printStackTrace();
//                        }
//                    }
//                }
//            });
//        } else if (mAvailable){
//            mainDetailButton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Log.w(TAG, "onClick main detail button" );
//                    showTolakDialog(mId);
//                }
//            });
//        } else {
//            mainDetailButton.setVisibility(View.GONE);
//        }
//    }

    boolean getDetailTransaksi(String id){
        AndroidNetworking.post(getActivity().getString(R.string.web_get_detail_transac))
                .addHeaders("key",getActivity().getString(R.string.api_key))
                .addBodyParameter("id_transaksi", id)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "onResponse get transaksi detail : success" + response.toString());

                        try{
                            if(response.get("status").equals("failed")){
                                Toast.makeText(getContext() , "nego gagal" , Toast.LENGTH_SHORT);
                            } else if(response.get("status").equals("success")){
                                JSONObject jsonObject = response.getJSONArray("data").getJSONObject(0);
                                ArrayList<HistoryTransactionLayananListModel> tmp = new ArrayList<>();
                                for (int j = 0; j <jsonObject.getJSONArray("transaksi_item").length() ; j++) {
                                    tmp.add(new HistoryTransactionLayananListModel(
                                            jsonObject.getJSONArray("transaksi_item").getJSONObject(j).getString("nama_layanan"),
                                            jsonObject.getJSONArray("transaksi_item").getJSONObject(j).getString("kuantitas"),
                                            jsonObject.getJSONArray("transaksi_item").getJSONObject(j).getString("harga_satuan")
                                    ));
                                }
                                mHistoryModel = new HistoryTransactionListModel(
                                        response.getJSONArray("data").getJSONObject(0).getString("id_pemesanan"),
                                        response.getJSONArray("data").getJSONObject(0).getString("nama_pelakon"),
                                        response.getJSONArray("data").getJSONObject(0).getString("nama_member"),
                                        response.getJSONArray("data").getJSONObject(0).getString("status_kesepakatan"),
                                        response.getJSONArray("data").getJSONObject(0).getString("keterangan"),
                                        response.getJSONArray("data").getJSONObject(0).getString("alamat"),
                                        response.getJSONArray("data").getJSONObject(0).getString("jam"),
                                        response.getJSONArray("data").getJSONObject(0).getString("tanggal"),
                                        response.getJSONArray("data").getJSONObject(0).getString("total_harga"),//price
                                        response.getJSONArray("data").getJSONObject(0).getString("ongkos"),//admin fee
                                        response.getJSONArray("data").getJSONObject(0).getString("total_harga"),//total
                                        response.getJSONArray("data").getJSONObject(0).getString("tipe_pembayaran"),//payment
                                        response.getJSONArray("data").getJSONObject(0).getString("id_pelakon"),//id lakon
                                        response.getJSONArray("data").getJSONObject(0).getString("id_member"),//id member
                                        tmp
                                );

                                if (response.getJSONArray("data").getJSONObject(0).getString("status_kesepakatan").equals("pending")){
                                    mAvailable = true;
                                } if(response.getJSONArray("data").getJSONObject(0).getString("status_kesepakatan").equals("selesai")
                                && response.getJSONArray("data").getJSONObject(0).getString("status_pembayaran").equals("selesai")){
                                    showDialog();
                                }
                                else {
                                    mAvailable = false;
                                }
                                initView();
                            }
                        }catch (JSONException e){
                            Log.w(TAG, "onResponse: json error" + e.toString() );
                            Toast.makeText(getContext(), "Teerjadi kesalahan pada server" , Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(TAG, "onResponse fanerror : " + anError.toString() );
                        Toast.makeText(getContext(), "" +anError.getErrorBody(), Toast.LENGTH_SHORT).show();
                    }
                });
        return true;
    }

    private void showDialog() {
        new AppRatingDialog.Builder()
                .setPositiveButtonText("Submit")
                .setNegativeButtonText("Batal")
                .setNoteDescriptions(Arrays.asList("Sangat Buruk", "Buruk", "Biasa", "Bagus", "Sangat Bagus"))
                .setDefaultRating(3)
                .setTitle("Berikan Nilai")
                .setDescription("Pilih rating dan berikan komentar terhadap pelakon")
                .setCommentInputEnabled(true)
                .setDefaultComment("Cukup")
                .setStarColor(R.color.colorAccent)
//                .setTitleTextColor(R.color.grey3)
                .setDescriptionTextColor(R.color.grey2)
                .setHint("Tulis komentar anda disini")
//                .setHintTextColor(R.color.grey2)
                .setCommentTextColor(R.color.grey3)
                .setCommentBackgroundColor(R.color.white)
//                .setWindowAnimation(R.style.MyDialogFadeAnimation)
                .setCancelable(false)
                .setCanceledOnTouchOutside(false)
                .create(getActivity())
                .setTargetFragment(this, 100) // only if listener is implemented by fragment
                .show();
    }

    void initNegoRecycleView(){
        recyclerViewDetailNegoTransaksi = getView().findViewById(R.id.fragment_history_detail_recycle_view);
        negoModels = new ArrayList<>();
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerViewDetailNegoTransaksi.setLayoutManager(layoutManager);
        recyclerViewDetailNegoTransaksi.setNestedScrollingEnabled(false);
        adapterNego = new HistoryDetailTransactionNegoRecycleViewAdapter(negoModels , getActivity() , this);
        recyclerViewDetailNegoTransaksi.setAdapter(adapterNego);
        getNego(mId);
    }

    boolean getNego(String id){
        AndroidNetworking.post(getActivity().getString(R.string.web_get_nego))
                .addHeaders("key",getActivity().getString(R.string.api_key))
                .addBodyParameter("id_transaksi", id)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "onResponse: get nego success get nego" + response.toString());

                        try{
                            if(response.getString("status").equalsIgnoreCase("failed")){
//                                Toast.makeText(getContext(), "" + response.get("status").equals("msg"), Toast.LENGTH_SHORT).show();
                                Toast.makeText(getContext(), "Teerjadi kesalahan pada server" , Toast.LENGTH_SHORT).show();
                                swipeRefreshLayout.setRefreshing(false);
                            } else if(response.get("status").equals("success")){

                                negoModels.clear();

                                for (int i = 0; i <response.getJSONArray("data").length() ; i++) {
                                    negoModels.add(new HistoryDetailTransactionNegoModel(
                                            response.getJSONArray("data").getJSONObject(i).getString("id_transaksi"),
                                            response.getJSONArray("data").getJSONObject(i).getString("tanggal"),
                                            response.getJSONArray("data").getJSONObject(i).getString("jam"),
                                            response.getJSONArray("data").getJSONObject(i).getString("alamat"),
                                            response.getJSONArray("data").getJSONObject(i).getString("keterangan"),
                                            response.getJSONArray("data").getJSONObject(i).getString("id_pengirim"),
                                            response.getJSONArray("data").getJSONObject(i).getString("nama_pengirim"),
                                            response.getJSONArray("data").getJSONObject(i).getString("id_transaksi"),
                                            response.getJSONArray("data").getJSONObject(i).getString("tanggal_dibuat"),
                                            response.getJSONArray("data").getJSONObject(i).getString("tanggal_diubah")
                                    ));
                                }
                                Collections.reverse(negoModels);
                                recyclerViewDetailNegoTransaksi.setAdapter(null);
                                recyclerViewDetailNegoTransaksi.setLayoutManager(null);
                                recyclerViewDetailNegoTransaksi.setAdapter(adapterNego);
                                recyclerViewDetailNegoTransaksi.setLayoutManager(layoutManager);
                                adapterNego.addItem(negoModels);

                                swipeRefreshLayout.setRefreshing(false);
                            }
                        }catch (JSONException e){
                            Log.w(TAG, "onResponse: json error get nego" + e.toString() );
                            Toast.makeText(getContext(), "Teerjadi kesalahan pada server" , Toast.LENGTH_SHORT).show();
                            swipeRefreshLayout.setRefreshing(false);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(TAG, "onResponse fanerror get nego: " + anError.toString() );
                        Toast.makeText(getContext(), "" + anError.getErrorDetail() + anError.getErrorBody(), Toast.LENGTH_SHORT).show();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                });
        return true;
    }


    boolean showNegoDialog(){
        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.fragment_history_detail_dialog_for_nego_transaksi);
        dialog.setTitle("Nego Transaksi");
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final EditText etTime =  dialog.findViewById(R.id.fragment_history_detail_nego_dialog_jam_txt);
        final EditText etDate =  dialog.findViewById(R.id.fragment_history_detail_nego_dialog_tanggal_txt);
        final EditText etLocation =  dialog.findViewById(R.id.fragment_history_detail_nego_dialog_lokasi_txt);
        final EditText etNote =  dialog.findViewById(R.id.fragment_history_detail_nego_dialog_keterangan_txt);
        final RelativeLayout restOfDialog =  dialog.findViewById(R.id.profile_harga_edit_main_relative_layout);
        final ProgressBar dialogProgressBar =  dialog.findViewById(R.id.profile_harga_edit_progress_bar);

        etTime.setFocusable(false);
        etDate.setFocusable(false);

        etTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimePicker(etTime);
            }
        });

        etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDatePicker(etDate);
            }
        });


        dialogProgressBar.setVisibility(View.INVISIBLE);

        Button dialogButton = (Button) dialog.findViewById(R.id.fragment_history_detail_nego_dialog_submit_btn);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                restOfDialog.setVisibility(View.INVISIBLE);
                dialogProgressBar.setVisibility(View.VISIBLE);

                String namaForEdit = etTime.getText().toString();
                String hargaForEdit = etDate.getText().toString();
                String deskripsiForEdit = etLocation.getText().toString();
                String deskripsiForNego = etNote.getText().toString();

                if(namaForEdit.isEmpty() || hargaForEdit.isEmpty() || deskripsiForEdit.isEmpty()){
                    restOfDialog.setVisibility(View.VISIBLE);
                    dialogProgressBar.setVisibility(View.INVISIBLE);
                } else {
                    submitNego(namaForEdit , hargaForEdit , deskripsiForEdit , deskripsiForNego);
                    dialog.dismiss();

                }
            }
        });

        Button dialogDismissButton = dialog.findViewById(R.id.fragment_history_detail_nego_dialog_dimiss_btn);
        dialogDismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        return true;
    }

    boolean submitNego(String jam , String tanggal , String alamat , String keterangan){
        Log.d(TAG, "submitNego: "+CurrentUserModel.getCurrentUserModel().getId());
        String mboh =CurrentUserModel.getCurrentUserModel().getId().equals(mHistoryModel.getId_lakon()) ?mHistoryModel.getId_member() : mHistoryModel.getId_lakon();
        Log.d(TAG, "submitNego: "+mboh);
        AndroidNetworking.post(getContext().getString(R.string.web_submit_nego))
                .addHeaders("key",getContext().getString(R.string.api_key))
                .addBodyParameter("id_pemesanan", mId)
                .addBodyParameter("jam", jam)
                .addBodyParameter("tanggal", tanggal)
                .addBodyParameter("alamat", alamat)
                .addBodyParameter("keterangan", keterangan)
                .addBodyParameter("id_pengirim", CurrentUserModel.getCurrentUserModel().getId())
                .addBodyParameter("id_pengirim_notif", CurrentUserModel.getCurrentUserModel().getId())
                .addBodyParameter("id_penerima_notif", CurrentUserModel.getCurrentUserModel().getId().equals(mHistoryModel.getId_lakon()) ?mHistoryModel.getId_member() : mHistoryModel.getId_lakon())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "onResponse: success" + response.toString());
                        try{
                            if(response.get("status").equals("failed")){
                                Toast.makeText(getContext() , "Nego gagal disimpan" , Toast.LENGTH_SHORT).show();
                            } else if(response.get("status").equals("success")){
                                Toast.makeText(getContext() , "Nego berhasil disimpan" , Toast.LENGTH_SHORT).show();
                            }
                        }catch (JSONException e){
                            Log.w(TAG, "onResponse: json error" + e.toString() );
                            Toast.makeText(getContext() , "Terjadi kesalahan pada server" , Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(TAG, "onResponse fanerror : " + anError.getErrorBody());
                        Toast.makeText(getContext(), "" + anError.getErrorBody(), Toast.LENGTH_SHORT).show();
                    }
                });
        return true;
    }

    boolean showTerimaDialog(){
        final Dialog dialog = new Dialog(getContext());

        dialog.setContentView(R.layout.fragment_profile_harga_custom_dialog_for_delete_layanan);
        dialog.setTitle("DELETE LAYANAN");
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final RelativeLayout restOfDialog = (RelativeLayout) dialog.findViewById(R.id.profile_harga_delete_main_relative_layout);
        final ProgressBar dialogProgressBar = (ProgressBar) dialog.findViewById(R.id.profile_harga_delete_progress_bar);
        final TextView textView = dialog.findViewById(R.id.profile_harga_delete_deskripsi_layanan_text);
        textView.setText("Yakin ingin menerima transaksi ini");
        dialogProgressBar.setVisibility(View.INVISIBLE);

        Button dialogButton = (Button) dialog.findViewById(R.id.profile_harga_delete_submit_button);
        dialogButton.setText("Lanjutkan");
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                restOfDialog.setVisibility(View.INVISIBLE);
                dialogProgressBar.setVisibility(View.VISIBLE);

                submitPenerimaanTransaksi();
                dialog.dismiss();
            }
        });

        Button dialogDismissButton =dialog.findViewById(R.id.profile_harga_delete_cancel_button);
        dialogDismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        return true;

    }

    boolean submitPenerimaanTransaksi(){
        AndroidNetworking.post(getContext().getString(R.string.web_terima_transaksi))
                .addHeaders("key",getContext().getString(R.string.api_key))
                .addBodyParameter("id_pemesanan", mId)
                .addBodyParameter("jam" , txtTime.getText().toString())
                .addBodyParameter("tanggal" , txtDate.getText().toString())
                .addBodyParameter("alamat" , txtLocation.getText().toString())
                .addBodyParameter("id_pengirim_notif", CurrentUserModel.getCurrentUserModel().getId())
                .addBodyParameter("id_penerima_notif", CurrentUserModel.getCurrentUserModel().getId().equals(mHistoryModel.getId_lakon()) ?mHistoryModel.getId_member() : mHistoryModel.getId_lakon())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "onResponse: success" + response.toString());
                        try{
                            if(response.get("status").equals("failed")){
                                Toast.makeText(getContext() , "penerimaan transaksi gagal" , Toast.LENGTH_SHORT).show();
                            } else if(response.get("status").equals("success")){
                                Toast.makeText(getContext() , "penerimaan transaksi berhasil" , Toast.LENGTH_SHORT).show();
                                ((MainActivity)getContext()).handleNavigation(R.id.chat_menu);
                            }
                        }catch (JSONException e){
                            Log.w(TAG, "onResponse: json error" + e.toString());
                            Toast.makeText(getContext() , "Terjadi kesalahan pada server" , Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(TAG, "onResponse fanerror : " + anError.toString() );
                        Toast.makeText(getContext(), "" + anError.getErrorBody(), Toast.LENGTH_SHORT).show();
                    }
                });
        return true;
    }


    boolean showTolakDialog(){
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.fragment_profile_harga_custom_dialog_for_delete_layanan);
        dialog.setTitle("DELETE LAYANAN");
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        final RelativeLayout restOfDialog = dialog.findViewById(R.id.profile_harga_delete_main_relative_layout);
        final ProgressBar dialogProgressBar = dialog.findViewById(R.id.profile_harga_delete_progress_bar);
        final TextView textView = dialog.findViewById(R.id.profile_harga_delete_deskripsi_layanan_text);
        textView.setText("Yakin ingin menolak transaksi ini");
        dialogProgressBar.setVisibility(View.INVISIBLE);
        Button dialogButton = dialog.findViewById(R.id.profile_harga_delete_submit_button);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext() , "pembaharuan layanan gagal" , Toast.LENGTH_SHORT);
                restOfDialog.setVisibility(View.INVISIBLE);
                dialogProgressBar.setVisibility(View.VISIBLE);
                String user_id = CurrentUserModel.getCurrentUserModel().getId();
                AndroidNetworking.post(getActivity().getString(R.string.web_tolak_transaksi))
                        .addHeaders("key",getActivity().getString(R.string.api_key))
                        .addBodyParameter("id_pemesanan", mId)
                        .addBodyParameter("id_pengirim_notif", user_id)
                        .addBodyParameter("id_penerima_notif", user_id.equals(mHistoryModel.getId_lakon()) ?mHistoryModel.getId_member() : mHistoryModel.getId_lakon())
                        .setPriority(Priority.MEDIUM)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.d(TAG, "onResponse submit tolak transaksi : success" + response.toString());

                                try{
                                    if(response.get("status").equals("failed")){
                                        Toast.makeText(getContext() , "data gagal disimpan" , Toast.LENGTH_SHORT);
                                        dialog.dismiss();
                                    } else if(response.get("status").equals("success")){
                                        ((MainActivity) getActivity()).handleNavigation(R.id.chat_menu);
                                        dialog.dismiss();
                                    }
                                }catch (JSONException e){
                                    Log.w(TAG, "onResponse: json error" + e.toString() );
                                    Toast.makeText(getContext(), "Teerjadi kesalahan pada server" , Toast.LENGTH_SHORT).show();
                                    dialog.dismiss();
                                }
                            }

                            @Override
                            public void onError(ANError anError) {
                                Log.d(TAG, "onResponse fanerror : " + anError.getErrorBody() );
                                Toast.makeText(getContext(), "" +anError.getErrorBody(), Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }
                        });

            }
        });
        Button dialogDismissButton = (Button) dialog.findViewById(R.id.profile_harga_delete_cancel_button);
        dialogDismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        return true;
    }

    private void  openDatePicker(TextView txt){
        final TextView txtDate  = txt;
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dpd = new DatePickerDialog(getContext(),
                new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        String day = dayOfMonth /10 < 1? "0" + dayOfMonth : ""+ dayOfMonth;
                        String month = monthOfYear /10 < 1? "0" + (monthOfYear + 1) : ""+ (monthOfYear + 1);

                        txtDate.setText(day + "-" + (month)+ "-" + year);

                    }
                }, mYear, mMonth, mDay);
        dpd.show();

    }

    private void  openTimePicker(TextView txt){
        final TextView txtTime  = txt;
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                String hour = selectedHour /10 < 1? "0" + selectedHour : ""+ selectedHour;
                String minute = selectedMinute /10 < 1? "0" + selectedMinute : ""+ selectedMinute;

                txtTime.setText( hour + ":" + minute);
            }
        }, hour, minute, true);
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    @Override
    public void onPositiveButtonClicked(int rate, String comment) {
        AndroidNetworking.post(getActivity().getString(R.string.web_insert_review))
                .addHeaders("key",getActivity().getString(R.string.api_key))
                .addBodyParameter("id_pelakon", mId)
                .addBodyParameter("id_member", CurrentUserModel.getCurrentUserModel().getId())
                .addBodyParameter("rating", ""+rate)
                .addBodyParameter("ulasan", comment)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "onResponse: get nego success get nego" + response.toString());

                        try{
                            if(response.getString("status").equalsIgnoreCase("failed")){
//                                Toast.makeText(getContext(), "" + response.get("status").equals("msg"), Toast.LENGTH_SHORT).show();
                                Toast.makeText(getContext(), "Teerjadi kesalahan pada server" , Toast.LENGTH_SHORT).show();
                                swipeRefreshLayout.setRefreshing(false);
                            } else if(response.get("status").equals("success")){
                                Toast.makeText(getContext(), "Terimakasih atas ratingnya..." , Toast.LENGTH_SHORT).show();

                            }
                        }catch (JSONException e){
                            Log.w(TAG, "onResponse: json error get nego" + e.toString() );
                            Toast.makeText(getContext(), "Teerjadi kesalahan pada server" , Toast.LENGTH_SHORT).show();
                            swipeRefreshLayout.setRefreshing(false);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(TAG, "onResponse fanerror get nego: " + anError.toString() );
                        Toast.makeText(getContext(), "" + anError.getErrorDetail() + anError.getErrorBody(), Toast.LENGTH_SHORT).show();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                });
    }

    @Override
    public void onNegativeButtonClicked() {

    }

    @Override
    public void onNeutralButtonClicked() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.mId = getArguments().getString(TRANSACTION_ID);
            this.mAvailable = getArguments().getBoolean(TRANSACTION_AVAILABLE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_history_detail, container, false);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
