package com.studio.karya.lakonesia;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.studio.karya.lakonesia.Model.CurrentUserModel;
import com.studio.karya.lakonesia.Utils.PublicString;

import org.json.JSONException;
import org.json.JSONObject;

public class RegisterActivity extends AppCompatActivity {


    private Button btn_lakon_register , btn_member_register;
    private String TAG = "===== Register Activity";
    private TextView signupText;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        btn_lakon_register = findViewById(R.id.btn_register_lakon);

        signupText = findViewById(R.id.text_signup);
        progressBar = findViewById(R.id.progressbar_signup);

        final GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        try{
            Log.w(TAG, "photo url : "+ account.getPhotoUrl().toString() );
        }catch (Exception e){

        }
        btn_lakon_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_lakon_register.setClickable(false);
                btn_member_register.setClickable(false);
                signupText.setVisibility(View.INVISIBLE);
                progressBar.setVisibility(View.VISIBLE);
                handleRegisterAsLakon();
            }
        });

        btn_member_register = findViewById(R.id.btn_register_member);
        btn_member_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_member_register.setClickable(false);
                btn_lakon_register.setClickable(false);
                signupText.setVisibility(View.INVISIBLE);
                progressBar.setVisibility(View.VISIBLE);
                handleRegisterToWebservice(account.getEmail(), account.getDisplayName(), "",  "google", "member",account.getId());
            }
        });
    }


    private void handleRegisterToWebservice(String email ,String nama ,String foto_url ,String sosial_media ,String role ,String id_sosial_media){
        AndroidNetworking.post(getString(R.string.web_register))
                .addHeaders("key","12345")
                .addBodyParameter("email", email)
                .addBodyParameter("nama", nama)
                .addBodyParameter("foto_url", foto_url)
                .addBodyParameter("sosial_media", sosial_media)
                .addBodyParameter("role", role)
                .addBodyParameter("no_hp", "0")
                .addBodyParameter("asal_komunitas", "null")
                .addBodyParameter("id_sosial_media", id_sosial_media)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "onResponse: " + response.toString());

                        try{
                            if(response.get("status").equals("failed")){
                                Toast.makeText(RegisterActivity.this, "Register Gagal", Toast.LENGTH_LONG).show();
                                if(response.get("status").equals("failed")){

                                }
                                Log.w(TAG, "onResponse: "+ response.toString() );
                            } else if(response.get("status").equals("success")){
//                                PublicString.setPhoneNumber(response.getString("phone"));
//                                CurrentUserModel.setCurrentUserModel(
//                                        response.getJSONArray("data").getJSONObject(0).getString("id"),
//                                        response.getJSONArray("data").getJSONObject(0).getString("nama"),
//                                        response.getJSONArray("data").getJSONObject(0).getString("email"),
//                                        response.getJSONArray("data").getJSONObject(0).getString("role"),
//                                        response.getJSONArray("data").getJSONObject(0).getString("foto_url"),
//                                        response.getJSONArray("data").getJSONObject(0).getString("deskripsi"),
//                                        response.getJSONArray("data").getJSONObject(0).getString("id_kategori"),
//                                        response.getJSONArray("data").getJSONObject(0).getString("nama_kategori")
//                                );
//                                Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
//                                startActivity(intent);
//                                finish();
                                handleLoginToWebservice(response.getString("email"));
                            }
                        }catch (JSONException e){
                            Log.e(TAG, "onResponse json error: " + e.getMessage() );
                        }

                        btn_member_register.setClickable(true);
                        btn_lakon_register.setClickable(true);
                        progressBar.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(TAG, "onResponse: " + anError.toString() + anError.getMessage() + anError.getErrorBody() );
                        Toast.makeText(RegisterActivity.this, "" + anError.getErrorDetail(), Toast.LENGTH_SHORT).show();

                        btn_member_register.setClickable(true);
                        btn_lakon_register.setClickable(true);
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                });
    }

    private void handleLoginToWebservice(String email){
        AndroidNetworking.post(getString(R.string.web_login))
                .addHeaders("key",getString(R.string.api_key))
                .addBodyParameter("email", email)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG , "onResponse: success " + response.toString());

                        try{
                            if(response.get("status").equals("failed")){
                                Log.w(TAG, "onResponse: " + "user unregistererd" );
                                Intent signInIntent = new Intent(RegisterActivity.this, RegisterActivity.class);
                                startActivityForResult(signInIntent, 1);
                            } else if(response.get("status").equals("success")){
                                Log.w(TAG, "onResponse: succes" + response.toString() );
                                PublicString.setPhoneNumber(response.getString("phone"));
                                CurrentUserModel.setCurrentUserModel(
                                        response.getJSONArray("data").getJSONObject(0).getString("id"),
                                        response.getJSONArray("data").getJSONObject(0).getString("nama"),
                                        response.getJSONArray("data").getJSONObject(0).getString("email"),
                                        response.getJSONArray("data").getJSONObject(0).getString("role"),
                                        response.getJSONArray("data").getJSONObject(0).getString("foto_url"),
                                        response.getJSONArray("data").getJSONObject(0).getString("deskripsi"),
                                        response.getJSONArray("data").getJSONObject(0).getString("id_kategori"),
                                        response.getJSONArray("data").getJSONObject(0).getString("nama_kategori")
                                );
                                Intent signInIntent = new Intent(RegisterActivity.this, MainActivity.class);
                                startActivityForResult(signInIntent, 1);
                                finish();
                            }
                        }catch (JSONException e){
                            Log.w(TAG, "onResponse json error : " + e.toString() );
                            progressBar.setVisibility(View.INVISIBLE);
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(TAG, "onResponse an error: " + anError.getErrorDetail());
                        Toast.makeText(RegisterActivity.this, "" + anError.getErrorDetail(), Toast.LENGTH_SHORT).show();
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                });
    }

    private void handleRegisterAsLakon(){
        AndroidNetworking.get(getString(R.string.link_register_lakon))
                .addHeaders("key",getString(R.string.api_key))
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "onResponse: " + response.toString());

                        try{
                            if(response.get("status").equals("failed")){
                                Toast.makeText(RegisterActivity.this, "Register Gagal", Toast.LENGTH_LONG).show();
                                if(response.get("status").equals("failed")){

                                }
                                Log.w(TAG, "onResponse: "+ response.toString() );
                            } else if(response.get("status").equals("success")){
//                                PublicString.setPhoneNumber(response.getString("phone"));
                                String link = response.getString("data");
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
                                startActivity(browserIntent);
                            }
                        }catch (JSONException e){
                            Log.e(TAG, "onResponse json error: " + e.getMessage() );
                        }

                        btn_member_register.setClickable(true);
                        btn_lakon_register.setClickable(true);
                        progressBar.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(TAG, "onResponse: " + anError.toString() + anError.getErrorBody() + anError.getMessage() + anError.getErrorDetail());
                        Toast.makeText(RegisterActivity.this, "" + anError.getErrorDetail(), Toast.LENGTH_SHORT).show();

                        btn_member_register.setClickable(true);
                        btn_lakon_register.setClickable(true);
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                });
    }
}
