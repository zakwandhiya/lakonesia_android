package com.studio.karya.lakonesia.Model;

import java.util.ArrayList;

public class KategoriModel {
    String nama , id;
    int foto_url;
    static ArrayList<KategoriModel> kategoriModels;

    public KategoriModel(String id, String nama , int foto_url) {
        this.nama = nama;
        this.id = id;
        this.foto_url = foto_url;
    }

    public static ArrayList<KategoriModel> getKategoriModels() {
        return kategoriModels;
    }

    public static void setKategoriModels(ArrayList<KategoriModel> tmp) {
        kategoriModels = tmp;
    }

    public int getFoto_url() {
        return foto_url;
    }

    public void setFoto_url(int foto_url) {
        this.foto_url = foto_url;
    }


    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
