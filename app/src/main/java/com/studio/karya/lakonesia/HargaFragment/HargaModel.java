package com.studio.karya.lakonesia.HargaFragment;

public class HargaModel {
    private String id_layanan , id_pelakon , nama_layanan , deskripsi , harga  , harga_promo, status_promo;
    private boolean expanded;
    private int quantity;

    public HargaModel(String id_layanan, String id_pelakon, String nama_layanan, String deskripsi, String harga, String status_promo , String harga_promo) {
        this.id_layanan = id_layanan;
        this.id_pelakon = id_pelakon;
        this.nama_layanan = nama_layanan;
        this.deskripsi = deskripsi;
        this.harga = harga;
        this.status_promo = status_promo;
        this.harga_promo = harga_promo;
        this.expanded = false;
        this.quantity = 0;
    }

    public String getHarga_promo() {
        return harga_promo;
    }

    public void setHarga_promo(String harga_promo) {
        this.harga_promo = harga_promo;
    }

    public int getQuantity() {
        return quantity;
    }

    public void tambahQty(){
        quantity++;
    }

    public void kurangQty(){
        if(quantity > 0)quantity--;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    public String getId_layanan() {
        return id_layanan;
    }

    public void setId_layanan(String id_layanan) {
        this.id_layanan = id_layanan;
    }

    public String getId_pelakon() {
        return id_pelakon;
    }

    public void setId_pelakon(String id_pelakon) {
        this.id_pelakon = id_pelakon;
    }

    public String getNama_layanan() {
        return nama_layanan;
    }

    public void setNama_layanan(String nama_layanan) {
        this.nama_layanan = nama_layanan;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getStatus_promo() {
        return status_promo;
    }

    public void setStatus_promo(String status_promo) {
        this.status_promo = status_promo;
    }
}
