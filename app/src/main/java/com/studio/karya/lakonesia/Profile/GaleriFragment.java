package com.studio.karya.lakonesia.Profile;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.studio.karya.lakonesia.Model.CurrentUserModel;
import com.studio.karya.lakonesia.Model.GaleryModel;
import com.studio.karya.lakonesia.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link GaleriFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link GaleriFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GaleriFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private GridLayoutManager gridLayoutManager;
    private RecyclerView recyclerView;
    private GaleryAdapter galeryAdapter;
    private ArrayList<GaleryModel> listGalery  = new ArrayList<>();
    private OnFragmentInteractionListener mListener;
    private FloatingActionButton fab ;


    public GaleriFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment GaleriFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GaleriFragment newInstance(String param1, String param2) {
        GaleriFragment fragment = new GaleriFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        gridLayoutManager = new GridLayoutManager(getContext(), 3);
        fab = getView().findViewById(R.id.floatingActionButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().startActivity(new Intent(getActivity(),UploadImageActivity.class));
            }
        });
        recyclerView = getView().findViewById(R.id.rv_galery);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(gridLayoutManager);
        getGalery();
    }

    @Override
    public void onResume() {
        super.onResume();

        getGalery();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gridLayoutManager = new GridLayoutManager(getContext(), 3);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile_galeri, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    void getGalery(){
        AndroidNetworking.get("http://api.lakonesia.com/MasterController/galery")
                .addHeaders("key",getString(R.string.api_key))
                .addQueryParameter("id_pelakon", CurrentUserModel.getCurrentUserModel().getId())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            if(response.get("status").equals("failed")){
                                Toast.makeText(getContext(), "" + response.get("msg"), Toast.LENGTH_SHORT).show();
                            } else if(response.get("status").equals("success")){
                                listGalery.clear();
                                for (int i = 0; i <response.getJSONArray("data").length() ; i++) {
                                    try {
                                        JSONObject jsonObject = response.getJSONArray("data").getJSONObject(i);
                                        GaleryModel galeryModel = new GaleryModel(
                                                jsonObject.getString("id_foto"),
                                                jsonObject.getString("id_pelakon"),
                                                jsonObject.getString("foto_url")
                                        );
                                        listGalery.add(galeryModel);

                                        galeryAdapter = new GaleryAdapter(listGalery);
                                        recyclerView.setAdapter(galeryAdapter);
                                    } catch (JSONException e){
                                        Toast.makeText(getActivity(), "Terjadi kesalahan pada server", Toast.LENGTH_SHORT).show();
                                    }

                                }
                            }
                        }catch (JSONException e){
                            Toast.makeText(getActivity(), "Terjadi kesalahan pada server", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(getContext(), "Terjadi kesalahan pada server", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
