package com.studio.karya.lakonesia.Model;

public class GaleryModel {
    private String idFoto;
    private String idPelakon;
    private String fotoUrl;
    public GaleryModel(String idFoto, String idPelakon, String fotoUrl) {
        this.idFoto = idFoto;
        this.idPelakon = idPelakon;
        this.fotoUrl = fotoUrl;
    }
    public String getIdFoto() {
        return idFoto;
    }

    public void setIdFoto(String idFoto) {
        this.idFoto = idFoto;
    }

    public String getIdPelakon() {
        return idPelakon;
    }

    public void setIdPelakon(String idPelakon) {
        this.idPelakon = idPelakon;
    }

    public String getFotoUrl() {
        return fotoUrl;
    }

    public void setFotoUrl(String fotoUrl) {
        this.fotoUrl = fotoUrl;
    }

}