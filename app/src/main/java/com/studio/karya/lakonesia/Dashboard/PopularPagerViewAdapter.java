package com.studio.karya.lakonesia.Dashboard;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.squareup.picasso.Picasso;
import com.studio.karya.lakonesia.MainActivity;
import com.studio.karya.lakonesia.Model.KategoriModel;
import com.studio.karya.lakonesia.R;
import com.studio.karya.lakonesia.SearchFragment;
import com.studio.karya.lakonesia.Transaction.TransactionActivity;
import com.studio.karya.lakonesia.Utils.PublicString;

import java.util.ArrayList;
import java.util.List;

public class PopularPagerViewAdapter extends RecyclerView.Adapter<PopularPagerViewAdapter.PopularRecycleViewHolder> {

        private ArrayList<LakonRecycleViewModel> dataList;
        private MainActivity mainActivity;

        public PopularPagerViewAdapter(ArrayList<LakonRecycleViewModel> dataList , MainActivity mainActivity) {
            this.dataList = dataList;
            this.mainActivity = mainActivity;
        }

        public void addItem(ArrayList<LakonRecycleViewModel> mData) {
            this.dataList = mData;
            notifyDataSetChanged();
        }

        @Override
        public PopularRecycleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.fragment_dashboard_kategori_layout, parent, false);
            return new PopularRecycleViewHolder(view);
        }

        @Override
        public void onBindViewHolder(PopularRecycleViewHolder holder, int position) {
            final int positionIndex = position;
            holder.txtNama.setText(dataList.get(position).getNama());

            Picasso.get().load(dataList.get(position).getFoto_url()).fit().centerCrop().into(holder.ivKategoriIcon);

            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mainActivity.replaceFragment(1 , SearchFragment.newInstance(dataList.get(positionIndex).getNama()));

                }
            });
//        holder.txtNpm.setText(dataList.get(position).getNpm());
//        holder.txtNoHp.setText(dataList.get(position).getNohp());
        }

        @Override
        public int getItemCount() {
            return (dataList != null) ? dataList.size() : 0;
        }

        public class PopularRecycleViewHolder extends RecyclerView.ViewHolder{
            private TextView txtNama, txtNpm, txtNoHp;
            private ImageView ivKategoriIcon;
            private View view;


            public PopularRecycleViewHolder(View itemView) {
                super(itemView);
                view  = itemView;
                txtNama = (TextView) itemView.findViewById(R.id.dashboard_kategori_title);
                ivKategoriIcon = (ImageView) itemView.findViewById(R.id.dashboard_kategori_img);
//            txtNpm = (TextView) itemView.findViewById(R.id.txt_npm_mahasiswa);
//            txtNoHp = (TextView) itemView.findViewById(R.id.txt_nohp_mahasiswa);
            }
        }
}
