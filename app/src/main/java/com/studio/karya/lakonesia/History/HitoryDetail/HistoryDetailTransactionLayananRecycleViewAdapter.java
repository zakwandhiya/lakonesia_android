package com.studio.karya.lakonesia.History.HitoryDetail;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.studio.karya.lakonesia.History.HistoryList.HistoryTransactionLayananListModel;
import com.studio.karya.lakonesia.R;

import java.util.ArrayList;

public class HistoryDetailTransactionLayananRecycleViewAdapter extends RecyclerView.Adapter<HistoryDetailTransactionLayananRecycleViewAdapter.HistoryDetailTransactionLayananRecycleViewHolder> {

    private String TAG = "===== History Detail Layanan Adapter";

    private ArrayList<HistoryTransactionLayananListModel> dataList;
    private FragmentActivity activity;

    public HistoryDetailTransactionLayananRecycleViewAdapter(ArrayList<HistoryTransactionLayananListModel> dataList , FragmentActivity activity) {
        this.dataList = dataList;
        this.activity = activity;
    }

    public void addItem(ArrayList<HistoryTransactionLayananListModel> mData ) {
        this.dataList = mData;
        notifyDataSetChanged();
    }

    @Override
    public HistoryDetailTransactionLayananRecycleViewAdapter.HistoryDetailTransactionLayananRecycleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.activity_transaction_recycle_view_layout, parent, false);
        return new HistoryDetailTransactionLayananRecycleViewAdapter.HistoryDetailTransactionLayananRecycleViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final HistoryDetailTransactionLayananRecycleViewAdapter.HistoryDetailTransactionLayananRecycleViewHolder holder, int position) {

        Log.w(TAG, "onBindViewHolder: " );
        holder.txtNamaLayanan.setText(dataList.get(position).getKuantitas()+"x "+dataList.get(position).getNama_layanan());
        holder.txtHarga.setText("RP "+dataList.get(position).getHarga());


        final int positionTmp = position;

    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class HistoryDetailTransactionLayananRecycleViewHolder extends RecyclerView.ViewHolder{
        private TextView txtNamaLayanan, txtHarga;

        public HistoryDetailTransactionLayananRecycleViewHolder(View itemView) {
            super(itemView);
            Log.w(TAG, "HistoryDetailTransactionRecycleViewHolder: " );
            txtNamaLayanan = itemView.findViewById(R.id.transaction_dialog_nama_layanan);
//            txtKuantitas = itemView.findViewById(R.id.transaction_dialog_kuantitas);
            txtHarga = itemView.findViewById(R.id.transaction_dialog_harga);


        }
    }

}
