package com.studio.karya.lakonesia.Dashboard;

public class IklanModels {
    private String image, id, nama, email, foto_url, deskripsi;

    public IklanModels(String image, String id, String nama, String email, String foto_url, String deskripsi) {
        this.image = image;
        this.id = id;
        this.nama = nama;
        this.email = email;
        this.foto_url = foto_url;
        this.deskripsi = deskripsi;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFoto_url() {
        return foto_url;
    }

    public void setFoto_url(String foto_url) {
        this.foto_url = foto_url;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }
}
