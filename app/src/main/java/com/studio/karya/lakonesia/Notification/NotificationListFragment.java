package com.studio.karya.lakonesia.Notification;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.aurelhubert.ahbottomnavigation.notification.AHNotification;
import com.studio.karya.lakonesia.MainActivity;
import com.studio.karya.lakonesia.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class NotificationListFragment extends Fragment {
    private static final String USER_ID = "USER_ID";

    private String mId;


    private final static String TAG = "===== Notification List";


    private RecyclerView recyclerViewNotification;
    private ArrayList<NotificationModel> notificationArrayList;
    private NotificationRecycleViewAdapter notificationAdapter;
    private ProgressBar notificationListProgressbar;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ImageView notFoundImageView;
    private RecyclerView.LayoutManager layoutManager;
    private int unreadNotif;

    private OnFragmentInteractionListener mListener;

    public NotificationListFragment() {
    }

    public static NotificationListFragment newInstance(String param1) {
        NotificationListFragment fragment = new NotificationListFragment();
        Bundle args = new Bundle();
        args.putString(USER_ID, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mId = getArguments().getString(USER_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_notification_list, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initTransacRecycleView();
        notificationListProgressbar = getView().findViewById(R.id.fragment_notification_list_progressbar);
        notFoundImageView = getView().findViewById(R.id.fragment_notification_list_not_found);
        swipeRefreshLayout = getView().findViewById(R.id.fragment_notification_list_swipe_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initTransacRecycleView();
            }
        });
    }

    public void decreaseNotif(){
        unreadNotif--;
        if(unreadNotif > 0){
            AHNotification notification = new AHNotification.Builder()
                    .setText(""+unreadNotif)
                    .setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorAccent))
                    .setTextColor(ContextCompat.getColor(getContext(), R.color.white))
                    .build();
            ((MainActivity)getActivity()).getBottomNavigation().setNotification(notification, 1);
        } else {
            ((MainActivity)getActivity()).getBottomNavigation().setNotification("",1);
        }
    }


    void initTransacRecycleView(){
        unreadNotif = 0;

        recyclerViewNotification = getView().findViewById(R.id.fragment_notification_list_recycle_view);

        layoutManager = new LinearLayoutManager(getActivity());
        recyclerViewNotification.setLayoutManager(layoutManager);
        recyclerViewNotification.setNestedScrollingEnabled(false);

        notificationAdapter = new NotificationRecycleViewAdapter(notificationArrayList, getActivity() , this);
        recyclerViewNotification.setAdapter(notificationAdapter);

        recyclerViewNotification.addOnScrollListener(new RecyclerView.OnScrollListener(){
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int topRowVerticalPosition =
                        (recyclerView == null || recyclerView.getChildCount() == 0) ? 0 : recyclerView.getChildAt(0).getTop();
                swipeRefreshLayout.setEnabled(topRowVerticalPosition >= 0);

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        notificationArrayList = new ArrayList<>();

        getNotification(mId);

    }

    void getNotification(String id){
        AndroidNetworking.post(getString(R.string.web_get_notifikasi))
                .addHeaders("key",getString(R.string.api_key))
                .addBodyParameter("id_penerima",id)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.w(TAG, "onResponse: " + response.toString());
                        try{
                            if(response.get("status").equals("failed")){
                                Log.w(TAG, "onResponse failed: "+ response );
                                Toast.makeText(getContext(), "" + response.get("msg"), Toast.LENGTH_SHORT).show();
                                notificationListProgressbar.setVisibility(View.GONE);
                                swipeRefreshLayout.setRefreshing(false);
                            } else if(response.get("status").equals("success")){
                                unreadNotif = 0;
                                Log.w(TAG, "onResponse success: "+ response );
                                notificationListProgressbar.setVisibility(View.GONE);
                                swipeRefreshLayout.setRefreshing(false);
                                notificationArrayList = new ArrayList<>();
                                for (int i = 0; i < response.getJSONArray("data").length() ; i++) {
                                    NotificationModel notificationModelTmp = new NotificationModel(
                                            response.getJSONArray("data").getJSONObject(i).getString("id_notifikasi"),
                                            response.getJSONArray("data").getJSONObject(i).getString("id_penerima"),
                                            response.getJSONArray("data").getJSONObject(i).getString("id_pengirim"),
                                            response.getJSONArray("data").getJSONObject(i).getString("nama_penerima"),
                                            response.getJSONArray("data").getJSONObject(i).getString("nama_pengirim"),
                                            response.getJSONArray("data").getJSONObject(i).getString("teks_notifikasi"),
                                            response.getJSONArray("data").getJSONObject(i).getString("tipe_notifikasi"),
                                            response.getJSONArray("data").getJSONObject(i).getString("dibaca"),
                                            response.getJSONArray("data").getJSONObject(i).getString("tanggal_dibuat"),
                                            response.getJSONArray("data").getJSONObject(i).getString("tanggal_diubah")
                                    );

                                    if(response.getJSONArray("data").getJSONObject(i).getString("dibaca").equals("0")){
                                        unreadNotif++;

                                    }

                                    if(notificationModelTmp.getTipe_notifikasi().equals("11") || notificationModelTmp.getTipe_notifikasi().equals("10") || notificationModelTmp.getTipe_notifikasi().equals("21")){
                                        notificationModelTmp.setId_transaksi(response.getJSONArray("data").getJSONObject(i).getString("id_transaksi"));
                                    }

                                    notificationArrayList.add(notificationModelTmp);
                                    Log.w(TAG, "onResponse get notif: "+ i + " " + response.getJSONArray("data").getJSONObject(i));
                                }

                                if(unreadNotif > 0){
                                    AHNotification notification = new AHNotification.Builder()
                                            .setText(""+unreadNotif)
                                            .setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorAccent))
                                            .setTextColor(ContextCompat.getColor(getContext(), R.color.white))
                                            .build();
                                    ((MainActivity)getActivity()).getBottomNavigation().setNotification(notification, 1);
                                }

                                recyclerViewNotification.setAdapter(null);
                                recyclerViewNotification.setLayoutManager(null);
                                recyclerViewNotification.setAdapter(notificationAdapter);
                                recyclerViewNotification.setLayoutManager(layoutManager);

                                notificationAdapter.addItem(notificationArrayList);

                            }


                            if(notificationArrayList.size() > 0){
                                notFoundImageView.setVisibility(View.GONE);
                            }else {
                                notFoundImageView.setVisibility(View.VISIBLE);
                            }
                        }catch (JSONException e){
                            Log.w(TAG, "onResponse json error : " + e.toString());
                            Toast.makeText(getActivity(), "Terjadi kesalahan pada server", Toast.LENGTH_SHORT).show();
                            notificationListProgressbar.setVisibility(View.GONE);
                            swipeRefreshLayout.setRefreshing(false);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.w(TAG, "onResponse error: " + anError.getErrorBody() +  anError.getErrorDetail() + anError.getMessage());
                        Toast.makeText(getContext(), "Terjadi kesalahan pada server", Toast.LENGTH_SHORT).show();
                        notificationListProgressbar.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                    }
                });
    }


    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
