package com.studio.karya.lakonesia.History.HistoryList;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.studio.karya.lakonesia.Model.CurrentUserModel;
import com.studio.karya.lakonesia.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class HistoryListFragment extends Fragment {
    private OnFragmentInteractionListener mListener;

    private final static String TAG = "===== History List Frag";

    private static final String TRANSACTION_ID = "TRANSACTION_ID";

    private String mId;

    private RecyclerView recyclerViewTransaction;
    private ArrayList<HistoryTransactionListModel> transactionArrayList;
    private HistoryTransactionRecycleViewAdapter historyAdapter;
    private ProgressBar historyListProgressbar;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ImageView notFoundImageView;

    public HistoryListFragment() {
    }

    public static HistoryListFragment newInstance(String id) {
        HistoryListFragment fragment = new HistoryListFragment();
        Bundle args = new Bundle();
        args.putString(TRANSACTION_ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.mId = getArguments().getString(TRANSACTION_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_history_list , container, false);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initTransacRecycleView();
        historyListProgressbar = getView().findViewById(R.id.fragment_history_list_progressbar);


        notFoundImageView = getView().findViewById(R.id.fragment_history_list_illustration_image);
        swipeRefreshLayout = getView().findViewById(R.id.fragment_history_list_swipe_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initTransacRecycleView();
            }
        });
    }

    void initTransacRecycleView(){
        recyclerViewTransaction = getView().findViewById(R.id.fragment_history_list_recycle_view);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerViewTransaction.setLayoutManager(layoutManager);
        recyclerViewTransaction.setNestedScrollingEnabled(false);

        historyAdapter = new HistoryTransactionRecycleViewAdapter(transactionArrayList , getActivity());
        recyclerViewTransaction.setAdapter(historyAdapter);

        recyclerViewTransaction.addOnScrollListener(new RecyclerView.OnScrollListener(){
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int topRowVerticalPosition =
                        (recyclerView == null || recyclerView.getChildCount() == 0) ? 0 : recyclerView.getChildAt(0).getTop();
                swipeRefreshLayout.setEnabled(topRowVerticalPosition >= 0);

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        transactionArrayList = new ArrayList<>();

        addTransaction(mId);

    }

    void addTransaction(String id){
        AndroidNetworking.post(getString(R.string.web_get_transac))
                .addHeaders("key",getString(R.string.api_key))
                .addBodyParameter("id",id)
                .addBodyParameter("role",CurrentUserModel.getCurrentUserModel().getRole())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "onResponse: " + response.toString());
                        try{
                            if(response.get("status").equals("failed")){
                                Log.w(TAG, "onResponse failed: "+ response );
                                Toast.makeText(getContext(), "" + response.get("msg"), Toast.LENGTH_SHORT).show();
                                historyListProgressbar.setVisibility(View.GONE);
                                swipeRefreshLayout.setRefreshing(false);

                            } else if(response.get("status").equals("success")){
                                Log.w(TAG, "onResponse success: "+ response );
                                historyListProgressbar.setVisibility(View.GONE);
                                swipeRefreshLayout.setRefreshing(false);
                                transactionArrayList = new ArrayList<>();
                                for (int i = 0; i <response.getJSONArray("data").length() ; i++) {
                                    Log.w(TAG, "onResponse success: "+ i + " " + response );
                                    try {
                                        JSONObject jsonObject = response.getJSONArray("data").getJSONObject(i);
                                        ArrayList<HistoryTransactionLayananListModel> tmp = new ArrayList<>();
                                        for (int j = 0; j <jsonObject.getJSONArray("transaksi_item").length() ; j++) {
                                            tmp.add(new HistoryTransactionLayananListModel(
                                                    jsonObject.getJSONArray("transaksi_item").getJSONObject(j).getString("nama_layanan"),
                                                    jsonObject.getJSONArray("transaksi_item").getJSONObject(j).getString("kuantitas"),
                                                    jsonObject.getJSONArray("transaksi_item").getJSONObject(j).getString("harga_satuan")
                                            ));
                                        }
                                        transactionArrayList.add(new HistoryTransactionListModel(
                                                response.getJSONArray("data").getJSONObject(i).getString("id_pemesanan"),
                                                response.getJSONArray("data").getJSONObject(i).getString("nama_pelakon"),
                                                response.getJSONArray("data").getJSONObject(i).getString("nama_member"),
                                                response.getJSONArray("data").getJSONObject(i).getString("status_kesepakatan"),
                                                response.getJSONArray("data").getJSONObject(i).getString("keterangan"),
                                                response.getJSONArray("data").getJSONObject(i).getString("alamat"),
                                                response.getJSONArray("data").getJSONObject(i).getString("jam"),
                                                response.getJSONArray("data").getJSONObject(i).getString("tanggal"),
                                                response.getJSONArray("data").getJSONObject(i).getString("total_harga"),//price
                                                response.getJSONArray("data").getJSONObject(i).getString("ongkos"),//admin fee
                                                response.getJSONArray("data").getJSONObject(i).getString("total_harga"),//total
                                                response.getJSONArray("data").getJSONObject(i).getString("tipe_pembayaran"),//payment
                                                response.getJSONArray("data").getJSONObject(i).getString("id_pelakon"),//id lakon
                                                response.getJSONArray("data").getJSONObject(i).getString("id_member"),//id member
                                                tmp)
                                        );

                                        historyAdapter.addItem(transactionArrayList);
                                        historyAdapter.notifyDataSetChanged();


                                    } catch (JSONException e){
                                        historyListProgressbar.setVisibility(View.GONE);
                                        swipeRefreshLayout.setRefreshing(false);
                                        Log.w(TAG, "onResponse json error : " + e.toString() );
                                        Toast.makeText(getActivity(), "Terjadi kesalahan pada server", Toast.LENGTH_SHORT).show();
                                    }

                                }
                            }
                            if(transactionArrayList.size() > 0){
                                notFoundImageView.setVisibility(View.GONE);
                            }else {
                                notFoundImageView.setVisibility(View.VISIBLE);
                            }
                        }catch (JSONException e){
                            Log.w(TAG, "onResponse json error : " + e.toString());
                            Toast.makeText(getActivity(), "Terjadi kesalahan pada server", Toast.LENGTH_SHORT).show();
                            historyListProgressbar.setVisibility(View.GONE);
                            swipeRefreshLayout.setRefreshing(false);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.w(TAG, "onResponse error: " + anError.toString() );
                        Toast.makeText(getContext(), "Terjadi kesalahan pada server", Toast.LENGTH_SHORT).show();
                        historyListProgressbar.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                    }
                });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
