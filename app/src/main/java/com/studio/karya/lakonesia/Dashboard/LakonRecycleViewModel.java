package com.studio.karya.lakonesia.Dashboard;

public class LakonRecycleViewModel {
    private String id_pelakon , nama , email ,deskripsi , foto_url ,id_layanan, nama_layanan , harga_service , nama_kategori , status_promo , harga_promo, rating_user;

    public LakonRecycleViewModel(String id_pelakon, String nama, String email, String deskripsi, String foto_url, String id_layanan, String nama_layanan, String harga_service, String nama_kategori, String status_promo, String harga_promo, String rating_user) {
        this.id_pelakon = id_pelakon;
        this.nama = nama;
        this.email = email;
        this.deskripsi = deskripsi;
        this.foto_url = foto_url;
        this.id_layanan = id_layanan;
        this.nama_layanan = nama_layanan;
        this.harga_service = harga_service;
        this.nama_kategori = nama_kategori;
        this.status_promo = status_promo;
        this.harga_promo = harga_promo;
        this.rating_user = rating_user;
    }

    public String getNama_kategori() {
        return nama_kategori;
    }

    public void setNama_kategori(String nama_kategori) {
        this.nama_kategori = nama_kategori;
    }

    public String getStatus_promo() {
        return status_promo;
    }

    public void setStatus_promo(String status_promo) {
        this.status_promo = status_promo;
    }

    public String getRating_user() {
        return rating_user;
    }

    public String getHarga_promo() {
        return harga_promo;
    }

    public void setHarga_promo(String harga_promo) {
        this.harga_promo = harga_promo;
    }

    public String getId_pelakon() {
        return id_pelakon;
    }

    public void setId_pelakon(String id_pelakon) {
        this.id_pelakon = id_pelakon;
    }

    public String getHarga_layanan() {
        return harga_service;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId_layanan() {
        return id_layanan;
    }

    public void setId_layanan(String id_layanan) {
        this.id_layanan = id_layanan;
    }

    public void setHarga_service(String harga_service) {
        this.harga_service = harga_service;
    }


    public String getId_layan() {
        return id_layanan;
    }

    public void setId_layan(String id_layan) {
        this.id_layanan = id_layan;
    }

    public String getHarga_service() {
        return harga_service;
    }

    public String getNama_layanan() {
        return nama_layanan;
    }

    public void setNama_layanan(String nama_layanan) {
        this.nama_layanan = nama_layanan;
    }

    public String getFoto_url() {
        return foto_url;
    }

    public void setFoto_url(String foto_url) {
        this.foto_url = foto_url;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }
}
