package com.studio.karya.lakonesia;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import org.json.JSONObject;

public class SandBoxActivity extends AppCompatActivity {

    private TextView tv_result;

    private String TAG = "========== Sandbox Activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sand_box);
        AndroidNetworking.initialize(getApplicationContext());
        tv_result = findViewById(R.id.text_result);

        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);

        Log.w(TAG, "onCreate: " + account.getEmail());
        AndroidNetworking.post("http://lakonesia.playon-id.com/AuthController/prosesMasuk")
                .addHeaders("key","12345")
                .addBodyParameter("email", account.getEmail())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("FAN", "onResponse: success" + response.toString());
                        tv_result.setText("result : \n" + response.toString());
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d("FAN", "onResponse: " + anError.toString() );
                    }
                });
    }
}
