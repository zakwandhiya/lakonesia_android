package com.studio.karya.lakonesia.EditProfile;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.studio.karya.lakonesia.Model.CurrentUserModel;
import com.studio.karya.lakonesia.R;


public class EditProfileFragment extends Fragment {
    private static final String TAG = "===== Edit Profile Frag";
    private static final String USER_ID = "USER_ID";

    private String mId;

    private OnFragmentInteractionListener mListener;

    private TextView tvNama , tvDesktipsi ,tvKategori;

    public EditProfileFragment() {
    }

    public static EditProfileFragment newInstance(String mId) {
        EditProfileFragment fragment = new EditProfileFragment();
        Bundle args = new Bundle();
        args.putString(USER_ID, mId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mId = getArguments().getString(USER_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_edit_profile, container, false);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        tvNama = getView().findViewById(R.id.fragment_edit_profile_nama_text);
        tvKategori = getView().findViewById(R.id.fragment_edit_profile_kategori_text);
        tvDesktipsi = getView().findViewById(R.id.fragment_edit_profile_deskripsi_text);

        setText();

        LinearLayout editNama = getView().findViewById(R.id.fragment_edit_profile_nama_user_linear_layout);
        LinearLayout editDeskripsi = getView().findViewById(R.id.fragment_edit_profile_deskripsi_linear_layout);
        LinearLayout editKategori = getView().findViewById(R.id.fragment_edit_profile_kategori_user_linear_layout);

        final EditCallback editCallback = new EditCallback() {
            @Override
            public void callbackMethod(boolean status) {
                setText();
            }
        };

        editNama.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditProfileDialog editProfileDialog = EditProfileDialog.newInstance(mId , EditColumn.EDIT_NAMA , CurrentUserModel.getCurrentUserModel().getNama() , editCallback);
                editProfileDialog.show(getFragmentManager() , "edit nama");
            }
        });

        editDeskripsi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.w(TAG, "onClick edit deskripsi : " );
                EditProfileDialog editProfileDialog = EditProfileDialog.newInstance(mId , EditColumn.EDIT_DESKRIPSI , CurrentUserModel.getCurrentUserModel().getDeskripsi() , editCallback);
                editProfileDialog.show(getFragmentManager() , "edit deskripsi");
            }
        });

        editKategori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.w(TAG, "onClick edit kategori : " );
                EditProfileKategoriDialog editProfileDialog = EditProfileKategoriDialog.newInstance(mId , CurrentUserModel.getCurrentUserModel().getDeskripsi() , editCallback);
                editProfileDialog.show(getFragmentManager() , "edit deskripsi");
            }
        });

        if(!CurrentUserModel.getCurrentUserModel().getDeskripsi().isEmpty()){
            tvDesktipsi.setText(CurrentUserModel.getCurrentUserModel().getDeskripsi());
        }

        Log.w(TAG, "onActivityCreated: nama" + CurrentUserModel.getCurrentUserModel().getNama());
        Log.w(TAG, "onActivityCreated: foto url" + CurrentUserModel.getCurrentUserModel().getFoto_url());

        ImageView profileImage = getView().findViewById(R.id.fragment_edit_profile_foto);

        if(CurrentUserModel.getCurrentUserModel().getFoto_url() != null && !CurrentUserModel.getCurrentUserModel().getFoto_url().isEmpty()){
            Picasso.get().load(CurrentUserModel.getCurrentUserModel().getFoto_url()).fit().centerCrop().into(profileImage);
        }

        Toolbar editProfileAppBar = getView().findViewById(R.id.fragment_edit_profile_appbar);
        editProfileAppBar.setNavigationIcon(R.drawable.ic_back_button);
        editProfileAppBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }

    private void setText(){
        tvNama.setText(CurrentUserModel.getCurrentUserModel().getNama());

        if(CurrentUserModel.getCurrentUserModel().getDeskripsi() != null && !CurrentUserModel.getCurrentUserModel().getDeskripsi().isEmpty()){
            tvDesktipsi.setText(CurrentUserModel.getCurrentUserModel().getDeskripsi());
        }

        if(CurrentUserModel.getCurrentUserModel().getNama_kategori() != null && !CurrentUserModel.getCurrentUserModel().getNama_kategori().isEmpty()){
            tvKategori.setText(CurrentUserModel.getCurrentUserModel().getNama_kategori());
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
