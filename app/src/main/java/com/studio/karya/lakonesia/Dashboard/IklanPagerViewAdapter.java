package com.studio.karya.lakonesia.Dashboard;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.squareup.picasso.Picasso;
import com.studio.karya.lakonesia.R;
import com.studio.karya.lakonesia.Transaction.TransactionActivity;
import com.studio.karya.lakonesia.Utils.PublicString;

import java.util.ArrayList;
import java.util.List;

import static com.facebook.share.internal.DeviceShareDialogFragment.TAG;

public class IklanPagerViewAdapter extends PagerAdapter {

    private List<IklanModels> models;
    private Context context;

    public IklanPagerViewAdapter(List<IklanModels> iklanArrayList, Context context) {
        this.models= iklanArrayList;
        this.context = context;
    }

    public void addItem(ArrayList<IklanModels> mData) {
        this.models = mData;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return models.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        LayoutInflater layoutInflater = (LayoutInflater) container.getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.fragment_dashboard_iklan_layout, container, false );
        ImageView imageView;
        imageView = view.findViewById(R.id.dashboard_iklan_img);

        Picasso.get().load(models.get(position).getImage()).fit().centerCrop().into(imageView);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, TransactionActivity.class);
                intent.putExtra(PublicString.TRANSACTION_LAKON_ID, models.get(position).getId());
                intent.putExtra(PublicString.TRANSACTION_LAKON_NAMA, models.get(position).getNama());
                intent.putExtra(PublicString.TRANSACTION_LAKON_EMAIL, models.get(position).getEmail());
                intent.putExtra(PublicString.TRANSACTION_LAKON_FOTO_URL, models.get(position).getFoto_url());
                intent.putExtra(PublicString.TRANSACTION_LAKON_DESKRIPSI, models.get(position).getDeskripsi());
                context.startActivity(intent);
            }
        });

        ((ViewPager) container).addView(view, 0);

        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }
}
