package com.studio.karya.lakonesia.History.HistoryList;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.studio.karya.lakonesia.History.HitoryDetail.HistoryDetailFragment;
import com.studio.karya.lakonesia.MainActivity;
import com.studio.karya.lakonesia.Model.CurrentUserModel;
import com.studio.karya.lakonesia.R;
import com.studio.karya.lakonesia.Utils.StringUtils;
import com.studio.karya.lakonesia.Utils.PublicString;

import java.util.ArrayList;

public class HistoryTransactionRecycleViewAdapter extends RecyclerView.Adapter<HistoryTransactionRecycleViewAdapter.HistoryTransactionRecycleViewHolder> {

    private String TAG = "===== History Transaction Adapter";

    private ArrayList<HistoryTransactionListModel> dataList;
    private FragmentActivity activity;

    public HistoryTransactionRecycleViewAdapter(ArrayList<HistoryTransactionListModel> dataList , FragmentActivity activity) {
        this.dataList = dataList;
        this.activity = activity;
    }

    public void addItem(ArrayList<HistoryTransactionListModel> mData ) {
        this.dataList = mData;
        notifyDataSetChanged();
    }

    @Override
    public HistoryTransactionRecycleViewAdapter.HistoryTransactionRecycleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.fragment_history_transact_layout, parent, false);
        return new HistoryTransactionRecycleViewAdapter.HistoryTransactionRecycleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final HistoryTransactionRecycleViewAdapter.HistoryTransactionRecycleViewHolder holder, int position) {

        if(CurrentUserModel.getCurrentUserModel().getRole().equals(PublicString.ROLE_LAKON)){
            holder.txtNamaLakon.setText(dataList.get(position).getNama_member());
        } else if(CurrentUserModel.getCurrentUserModel().getRole().equals(PublicString.ROLE_MEMBER)){
            holder.txtNamaLakon.setText(dataList.get(position).getNama_lakon());
        }
//        String date = StringUtils.dayName(dataList.get(position).getDate(),"DD-MM-YYYY") +", "+StringUtils.dateFormat(dataList.get(position).getDate()) ;
        holder.txtDate.setText(dataList.get(position).getDate());
        holder.txtTime.setText(dataList.get(position).getTime());
        holder.txtLocation.setText(dataList.get(position).getAlamat());
        holder.txtStatus.setText(dataList.get(position).getStatus().toUpperCase());
//        holder.txtNote.setText(dataList.get(position).getNote());

        final int positionTmp = position;
        holder.btnViewTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean available = false;
                if(dataList.get(positionTmp).getStatus().equals("pending")){
                    available = true;
                }
                ((MainActivity)activity).replaceFragment(3 , HistoryDetailFragment.newInstance(dataList.get(positionTmp).getId_transaksi() ,dataList.get(positionTmp) , available));
//                HistoryDetailFragment historyDetailFragment= HistoryDetailFragment.newInstance(dataList.get(positionTmp).getId_transaksi() ,dataList.get(positionTmp) , available);
//                FragmentManager fragmentManager = activity.getSupportFragmentManager();
//                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                fragmentTransaction.replace(R.id.fragment_history_frame_layout , historyDetailFragment);
//                fragmentTransaction.addToBackStack(null);
//                fragmentTransaction.commit();
            }
        });

    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class HistoryTransactionRecycleViewHolder extends RecyclerView.ViewHolder{
        private TextView txtNamaLakon, txtDate, txtTime , txtNote ,txtLocation , txtStatus   ;
        private RelativeLayout btnViewTransaction;

        public HistoryTransactionRecycleViewHolder(View itemView) {
            super(itemView);
            btnViewTransaction = itemView.findViewById(R.id.btndetailhistory);
            txtNamaLakon = itemView.findViewById(R.id.fragment_history_transac_nama_lakon_text);
            txtDate = itemView.findViewById(R.id.fragment_history_transac_date_text);
            txtTime = itemView.findViewById(R.id.fragment_history_transac_time_text);
//            txtNote = itemView.findViewById(R.id.fragment_history_transac_note_txt);
            txtLocation = itemView.findViewById(R.id.fragment_history_transac_location_txt);
            txtStatus = itemView.findViewById(R.id.fragment_history_transac_status_txt);


        }
    }

}
