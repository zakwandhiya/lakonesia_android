package com.studio.karya.lakonesia;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.google.zxing.WriterException;
import com.studio.karya.lakonesia.Model.CurrentUserModel;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;

public class QRCodeActivity extends AppCompatActivity {

    ImageView qrImage;
    Bitmap bitmap;

    private String TAG = "===== QR code Activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcode);
        WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        int width = point.x;
        int height = point.y;
        int smallerDimension = width < height ? width : height;
        smallerDimension = smallerDimension * 3 / 4;
        QRGEncoder qrgEncoder = new QRGEncoder(CurrentUserModel.getCurrentUserModel().getEmail(), null, QRGContents.Type.TEXT, smallerDimension);
        qrImage = findViewById(R.id.activity_qr_scanner_image);
        try {
            bitmap = qrgEncoder.encodeAsBitmap();
            qrImage.setImageBitmap(bitmap);
        } catch (WriterException e) {
            Log.v(TAG, e.toString());
        }

        Button qrScann = findViewById(R.id.activity_qr_scanner_button);
        qrScann.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(QRCodeActivity.this, QRScannActivity.class);
                startActivity(intent);
            }
        });
    }
}
