package com.studio.karya.lakonesia.Transaction;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.studio.karya.lakonesia.R;

import java.util.ArrayList;

public class TransactionRecycleViewAdapter extends RecyclerView.Adapter<TransactionRecycleViewAdapter.TransactionRecycleViewHolder> {

    private String TAG = "===== Transaction Adapter";

    private ArrayList<TransactionModel> dataList;

    private FragmentActivity activity;
    private Context context;

    public TransactionRecycleViewAdapter(ArrayList<TransactionModel> dataList) {
        this.dataList = dataList;
    }

    public void addItem(ArrayList<TransactionModel> mData , FragmentActivity activity , Context context) {
        this.dataList = mData;
        this.activity =  activity;
        this.context = context;
        notifyDataSetChanged();
    }

    @Override
    public TransactionRecycleViewAdapter.TransactionRecycleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.activity_transaction_recycle_view_layout, parent, false);
        return new TransactionRecycleViewAdapter.TransactionRecycleViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final TransactionRecycleViewAdapter.TransactionRecycleViewHolder holder, int position) {
        holder.txtNama.setText(dataList.get(position).getKuantitas()+"x "+dataList.get(position).getNama_layanan());
        holder.txtHarga.setText("@ RP " + dataList.get(position).getHarga());
        Log.w(TAG, "initLakonRecycleView: "+ dataList.get(position).getNama_layanan() + "     " + dataList.get(position).getKuantitas() + "     " +position);

        final TransactionModel tmpTransactionModel = dataList.get(position);
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class TransactionRecycleViewHolder extends RecyclerView.ViewHolder{
        private TextView txtNama, txtHarga;

        public TransactionRecycleViewHolder(View itemView) {
            super(itemView);
            txtNama =   itemView.findViewById(R.id.transaction_dialog_nama_layanan);
            txtHarga =  itemView.findViewById(R.id.transaction_dialog_harga);
        }
    }

}
