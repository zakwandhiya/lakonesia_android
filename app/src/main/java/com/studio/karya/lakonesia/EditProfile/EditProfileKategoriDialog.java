package com.studio.karya.lakonesia.EditProfile;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import androidx.fragment.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.studio.karya.lakonesia.Model.CurrentUserModel;
import com.studio.karya.lakonesia.Model.KategoriModel;
import com.studio.karya.lakonesia.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class EditProfileKategoriDialog extends BottomSheetDialogFragment {
    private BottomSheetListener mListener;


    private String TAG = "===== Edit Dialog";

    private String mId , mNilai;
    private EditCallback mEditCallback;

    public EditProfileKategoriDialog() {
    }

    public static EditProfileKategoriDialog newInstance(String mId , String nilai , EditCallback editCallback) {
        EditProfileKategoriDialog transactionDialog = new EditProfileKategoriDialog();
        transactionDialog.mId = mId;
        transactionDialog.mNilai = nilai;
        transactionDialog.mEditCallback = editCallback;
        return transactionDialog;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_edit_profile_set_kategori_dialog_layout, container, false);
        return v;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        TextView editLabel = getView().findViewById(R.id.edit_profile_dialog_label_text);

        editLabel.setText("Kategori : ");

//        final TextView inputText = getView().findViewById(R.id.edit_profile_dialog_input);
//        inputText.setText(mNilai);
//
        final RadioGroup radioGroup = getView().findViewById(R.id.edit_profile_dialog_radio_button);
        radioGroup.setOrientation(LinearLayout.VERTICAL);
        final ArrayList<KategoriModel> kategoriModels = KategoriModel.getKategoriModels();
        for (int i = 0; i < kategoriModels.size() ; i++) {
            RadioButton rdbtn = new RadioButton(getContext());

            rdbtn.setId(Integer.parseInt( kategoriModels.get(i).getId()));
            rdbtn.setText("" +  kategoriModels.get(i).getNama());
            radioGroup.addView(rdbtn);
        }

        TextView dismiss = getView().findViewById(R.id.edit_profile_dialog_dismiss_button);
        dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


//        for (int row = 0; row < 1; row++) {
//            RadioGroup ll = new RadioGroup(getContext());
//            ll.setOrientation(LinearLayout.VERTICAL);
//
//            for (int i = 0; i < kategoriModels.size() ; i++) {
//                RadioButton rdbtn = new RadioButton(getContext());
//                rdbtn.setId(Integer.parseInt( kategoriModels.get(i).getId()));
//                rdbtn.setText("" +  kategoriModels.get(i).getNama());
//                ll.addView(rdbtn);
//            }
////            ((ViewGroup) getView().findViewById(R.id.edit_profile_dialog_radio_button)).addView(ll);
//
//            radioGroup.addView(ll);
//        }


        TextView submit = getView().findViewById(R.id.edit_profile_dialog_submit_button);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = "" + radioGroup.getCheckedRadioButtonId();
                String nama = "";
                for (KategoriModel kategoriModel : kategoriModels){
                    if(kategoriModel.getId().equals(id)){
                        nama = kategoriModel.getNama();
                    }
                }
                Log.w(TAG, "onClick: "+ id + " " + nama );
                handleUpdateProfile( id , nama);
            }
        });
        radioGroup.check(Integer.parseInt( CurrentUserModel.getCurrentUserModel().getId_kategori()));

    }

    private void handleUpdateProfile(final String nilai , final String nama){
        AndroidNetworking.post(getString(R.string.web_update_akun))
                .addHeaders("key" , getString(R.string.api_key))
                .addBodyParameter("id" , mId)
                .addBodyParameter("kolom" , "id_kategori")
                .addBodyParameter("nilai" , nilai)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG , "onResponse: success " + response.toString());

                        try{
                            if(response.get("status").equals("failed")){
                                Log.w(TAG, "onResponse: " + "user unregistererd" );
                                Toast.makeText(getActivity(), "update profile gagal", Toast.LENGTH_SHORT).show();
                                dismiss();
                            } else if(response.get("status").equals("success")){
                                Log.w(TAG, "onResponse: success" + response.toString() );
                                Toast.makeText(getActivity(), "profile berhasil diperbaharui", Toast.LENGTH_SHORT).show();

                                CurrentUserModel.getCurrentUserModel().setId_kategori(nilai);
                                CurrentUserModel.getCurrentUserModel().setNama_kategori(nama);

                                mEditCallback.callbackMethod(true);
                                dismiss();
                            }
                        }catch (JSONException e){
                            Log.w(TAG, "onResponse json error : " + e.toString() );
                            dismiss();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(TAG, "onResponse an error: " + anError.getErrorBody());
                        Toast.makeText(getActivity(), "" + anError.getErrorDetail() + anError.getErrorBody(), Toast.LENGTH_SHORT).show();
                        dismiss();
                    }
                });
    }



    public interface BottomSheetListener {
        void onButtonClicked(String text);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogStyle);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }
}

