package com.studio.karya.lakonesia.History.HitoryDetail;

public class HistoryDetailTransactionNegoModel {
    private String id_nego, date , time , location , note , id_pengirim , nama_pegirim , id_transaksi , tanggal_dibuat , tanggal_diubah;

    public HistoryDetailTransactionNegoModel(String id_nego, String date, String time, String location, String note, String id_pengirim, String nama_pegirim, String id_transaksi, String tanggal_dibuat, String tanggal_diubah) {
        this.id_nego = id_nego;
        this.date = date;
        this.time = time;
        this.location = location;
        this.note = note;
        this.id_pengirim = id_pengirim;
        this.nama_pegirim = nama_pegirim;
        this.id_transaksi = id_transaksi;
        this.tanggal_dibuat = tanggal_dibuat;
        this.tanggal_diubah = tanggal_diubah;
    }

    public String getId_pengirim() {
        return id_pengirim;
    }

    public void setId_pengirim(String id_pengirim) {
        this.id_pengirim = id_pengirim;
    }

    public String getNama_pegirim() {
        return nama_pegirim;
    }

    public void setNama_pegirim(String nama_pegirim) {
        this.nama_pegirim = nama_pegirim;
    }

    public String getId_transaksi() {
        return id_transaksi;
    }

    public void setId_transaksi(String id_transaksi) {
        this.id_transaksi = id_transaksi;
    }

    public String getTanggal_dibuat() {
        return tanggal_dibuat;
    }

    public void setTanggal_dibuat(String tanggal_dibuat) {
        this.tanggal_dibuat = tanggal_dibuat;
    }

    public String getTanggal_diubah() {
        return tanggal_diubah;
    }

    public void setTanggal_diubah(String tanggal_diubah) {
        this.tanggal_diubah = tanggal_diubah;
    }

    public String getId_nego() {
        return id_nego;
    }

    public void setId_nego(String id_nego) {
        this.id_nego = id_nego;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
