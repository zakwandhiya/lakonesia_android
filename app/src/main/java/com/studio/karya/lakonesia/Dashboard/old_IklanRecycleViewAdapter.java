package com.studio.karya.lakonesia.Dashboard;

import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.studio.karya.lakonesia.R;

import java.util.ArrayList;


public class old_IklanRecycleViewAdapter extends RecyclerView.Adapter<old_IklanRecycleViewAdapter.IklanRecycleViewHolder> {

    private ArrayList<old_IklanModel> dataList;

    public interface EventListener {
        void onEvent(int data);
    }

    public old_IklanRecycleViewAdapter(ArrayList<old_IklanModel> dataList) {
        this.dataList = dataList;

    }

    public void addItem(ArrayList<old_IklanModel> mData) {
        this.dataList = mData;
        notifyDataSetChanged();
    }

    @Override
    public IklanRecycleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.fragment_dashboard_iklan_layout, parent, false);
        View viewDashboard = layoutInflater.inflate(R.layout.fragment_dashboard, parent, false);
        return new IklanRecycleViewHolder(view, viewDashboard);
    }

    @Override
    public void onBindViewHolder(final IklanRecycleViewHolder holder, final int position) {
        if(position == 0){
            holder.linearLayout.setVisibility(View.VISIBLE);
        }
        Picasso.get().load(dataList.get(position).getFoto_url()).fit().centerCrop().into(holder.ivIklanBanner);
        holder.ivIklanBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("Hitung", String.valueOf(getItemCount()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }


    public class IklanRecycleViewHolder extends RecyclerView.ViewHolder{
        private ImageView ivIklanBanner;
        private LinearLayout linearLayout;
        private TextView zxc;
        public IklanRecycleViewHolder(View itemView, View viewDashboard) {
            super(itemView);
            ivIklanBanner = (ImageView) itemView.findViewById(R.id.dashboard_iklan_img);
            linearLayout = itemView.findViewById(R.id.fragment_dashbooard_iklan_layout_linear_layout);
        }
    }
}
