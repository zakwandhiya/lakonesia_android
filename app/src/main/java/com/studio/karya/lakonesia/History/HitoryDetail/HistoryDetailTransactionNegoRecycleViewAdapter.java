package com.studio.karya.lakonesia.History.HitoryDetail;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.studio.karya.lakonesia.MainActivity;
import com.studio.karya.lakonesia.Model.CurrentUserModel;
import com.studio.karya.lakonesia.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

public class HistoryDetailTransactionNegoRecycleViewAdapter extends RecyclerView.Adapter<HistoryDetailTransactionNegoRecycleViewAdapter.HistoryDetailTransactionRecycleViewHolder> {

    private String TAG = "===== History Detail Nego Adapter";

    private ArrayList<HistoryDetailTransactionNegoModel> dataList;
    private FragmentActivity activity;
    private HistoryDetailFragment historyDetailFragment;

    public HistoryDetailTransactionNegoRecycleViewAdapter(ArrayList<HistoryDetailTransactionNegoModel> dataList , FragmentActivity activity , HistoryDetailFragment historyDetailFragment) {
        this.dataList = dataList;
        this.activity = activity;
        this.historyDetailFragment = historyDetailFragment;
    }

    public void addItem(ArrayList<HistoryDetailTransactionNegoModel> mData ) {
        this.dataList = mData;
        Log.w(TAG, "addItem size : "+ mData.size() + " " + this.dataList.size() );
        notifyDataSetChanged();
    }

    @Override
    public HistoryDetailTransactionNegoRecycleViewAdapter.HistoryDetailTransactionRecycleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.fragment_history_detail_nego_layout, parent, false);
        return new HistoryDetailTransactionNegoRecycleViewAdapter.HistoryDetailTransactionRecycleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final HistoryDetailTransactionNegoRecycleViewAdapter.HistoryDetailTransactionRecycleViewHolder holder, int position) {

        Log.w(TAG, "onBindViewHolder: " );

        holder.date.setText(dataList.get(position).getDate());
        holder.time.setText(dataList.get(position).getTime());
        holder.location.setText(dataList.get(position).getLocation());
        holder.note.setText(dataList.get(position).getNote());
//        holder.nama.setText(dataList.get(position).getNama_pegirim());

//        holder.waktu.setText(dataList.get(position).getTanggal_dibuat());

        if(dataList.get(position).getId_pengirim().equals(CurrentUserModel.getCurrentUserModel().getId())){
            holder.label_lakon.setVisibility(View.GONE);
//            holder.space_lakon.setVisibility(View.GONE);
        } else {
            holder.label_member.setVisibility(View.GONE);
//            holder.space_member.setVisibility(View.GONE);
            if(position == dataList.size()-1){
//                holder.nego_button.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class HistoryDetailTransactionRecycleViewHolder extends RecyclerView.ViewHolder{
        private TextView date , time , location , note , nama;
        private View label_member , label_lakon ;
        private Button nego , terima;
        private View view;

        public HistoryDetailTransactionRecycleViewHolder(View itemView) {
            super(itemView);
            Log.w(TAG, "HistoryDetailTransactionRecycleViewHolder: " );
            view = itemView;
            nama = itemView.findViewById(R.id.fragment_history_recycle_nama_lakon);
//            waktu = itemView.findViewById(R.id.fragment_history_recycle_waktu_nego_dibuat);
            date = itemView.findViewById(R.id.fragment_history_detail_nego_date_text);
            time = itemView.findViewById(R.id.fragment_history_detail_nego_time_text);
            location = itemView.findViewById(R.id.fragment_history_detail_nego_location_text);
            note = itemView.findViewById(R.id.fragment_history_detail_nego_note_text);
            label_lakon = itemView.findViewById(R.id.fragment_history_detail_nego_member_line);
            label_member = itemView.findViewById(R.id.fragment_history_detail_nego_member_line);
//            space_member =  itemView.findViewById(R.id.fragment_history_detail_nego_member_linear_layout);
//            space_lakon =  itemView.findViewById(R.id.fragment_history_detail_nego_lakon_linear_layout);
//            nego_button =  itemView.findViewById(R.id.fragment_history_detail_nego_buttons);

        }
    }

}
