package com.studio.karya.lakonesia.Profile;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.UploadProgressListener;
import com.studio.karya.lakonesia.Model.CurrentUserModel;
import com.studio.karya.lakonesia.R;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONObject;

import java.io.File;

public class UploadImageActivity extends AppCompatActivity {
    Button select,upload;
    ImageView imageView;
    Uri resultUri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_image);
        imageView = findViewById(R.id.imageView);
        select = findViewById(R.id.imagePick);
        upload = findViewById(R.id.upload);

        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setMinCropResultSize(512,512)
                        .setAspectRatio(1,1)
                        .start(UploadImageActivity.this);
            }
        });
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (resultUri==null){
                    Toast.makeText(UploadImageActivity.this,"masukkan gambar terlebih dahulu",Toast.LENGTH_LONG).show();
                }else{

                    upload(resultUri);
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                resultUri = result.getUri();
                imageView.setImageURI(resultUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    public void upload(Uri imageUri){
        File file = new File(imageUri.getPath());
        AndroidNetworking.upload("http://api.lakonesia.com/MasterController/galery")
                .addHeaders("key",getString(R.string.api_key))
                .addMultipartFile("image",file)
                .addMultipartParameter("id_pelakon", CurrentUserModel.getCurrentUserModel().getId())
                .setPriority(Priority.HIGH)
                .build()
                .setUploadProgressListener(new UploadProgressListener() {
                    @Override
                    public void onProgress(long bytesUploaded, long totalBytes) {
                        // do anything with progress
                    }
                })
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        Log.e("aaa", "onError: "+ response.toString());
                        // do anything with response
                        Log.d("aaa", "onResponse: "+response.toString());
                        Intent in = new Intent();
                        setResult(1,in);
                        finish();
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.e("aaa", "onError: "+ error.getResponse().toString());
                    }
                });

    }

}
