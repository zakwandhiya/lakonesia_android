package com.studio.karya.lakonesia;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Rect;
import androidx.annotation.NonNull;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.studio.karya.lakonesia.Dashboard.DashboardFragment;
import com.studio.karya.lakonesia.EditProfile.EditProfileFragment;
import com.studio.karya.lakonesia.History.HistoryList.HistoryListFragment;
import com.studio.karya.lakonesia.History.HitoryDetail.HistoryDetailFragment;
import com.studio.karya.lakonesia.Model.CurrentUserModel;
import com.studio.karya.lakonesia.Notification.NotificationListFragment;
import com.studio.karya.lakonesia.Profile.ProfileFragment;

public class MainActivity extends AppCompatActivity
        implements BottomNavigationView.OnNavigationItemSelectedListener{

    Fragment fragment1, fragment2 , fragment3 , fragment4 , active;
    int fragmentID;
    int fragmentIndex;
    boolean backPressed;

    private String FRAGMENT_STATE_ARRAY = "STATE_ARRAY",FRAGMENT_INDEX = "FRAGMENT_INDEX" , TAG = "===== Main Activity";

    SparseArray<Fragment.SavedState> savedStateSparseArray;
    FragmentManager fm;

    AHBottomNavigation bottomNavigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        savedStateSparseArray = new SparseArray<>();
        backPressed = false;

        fragment1 = DashboardFragment.newInstance(
                CurrentUserModel.getCurrentUserModel().getId(),
                CurrentUserModel.getCurrentUserModel().getRole()
        );
        fragment2 = NotificationListFragment.newInstance(
                CurrentUserModel.getCurrentUserModel().getId()
        );
        fragment3 = HistoryListFragment.newInstance(
                CurrentUserModel.getCurrentUserModel().getId()
        );
        fragment4 = ProfileFragment.newProfileInstance(
                CurrentUserModel.getCurrentUserModel().getId()
        );
        fm = getSupportFragmentManager();

        active = fragment1;

        fm.beginTransaction().add(R.id.fl_container, fragment3, "3").hide(fragment3).commit();
        fm.beginTransaction().add(R.id.fl_container, fragment2, "2").hide(fragment2).commit();
        fm.beginTransaction().add(R.id.fl_container, fragment1, "1").hide(fragment1).commit();
        fm.beginTransaction().add(R.id.fl_container, fragment4, "4").hide(fragment4).commit();


        handleNavigation(R.id.home_menu);
        fragmentID = R.id.home_menu;


        bottomNavigation = findViewById(R.id.bn_main);

        AHBottomNavigationItem item1 = new AHBottomNavigationItem("Home", R.drawable.ic_home, R.color.colorPrimary);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem("Notifikasi", R.drawable.ic_notifikasi, R.color.colorPrimary);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem("Transaksi", R.drawable.ic_transaksi, R.color.colorPrimary);
        AHBottomNavigationItem item4 = new AHBottomNavigationItem("Profile", R.drawable.ic_profile, R.color.colorPrimary);

        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item2);
        bottomNavigation.setNotificationMarginLeft(12,12);
        bottomNavigation.setNotification("0", bottomNavigation.getItemsCount() - 1);
        bottomNavigation.addItem(item3);
        bottomNavigation.addItem(item4);

        bottomNavigation.setPadding(0 , 0 ,0 ,10);

        if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE) {
            bottomNavigation.setPadding(0 , 0 ,0 ,10);
        }
        else if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
            bottomNavigation.setPadding(0 , 0 ,0 ,10);
        }
        else if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_SMALL) {
            bottomNavigation.setPadding(10 , 0 ,10 ,10);
        }
        else {
        }



        bottomNavigation.setAccentColor(getResources().getColor( R.color.colorPrimary));

        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);

        bottomNavigation.setBehaviorTranslationEnabled(false);

        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                int id = R.id.home_menu;

                switch (position){
                    case 0 : {
                        id = R.id.home_menu;
                        break;
                    }
                    case 1 : {
                        id = R.id.favorite_menu;
                        break;
                    }
                    case 2 : {
                        id = R.id.chat_menu;
                        break;
                    }
                    case 3 : {
                        id = R.id.profile_menu;
                        break;
                    }
                }
                handleNavigation(id);
                return true;
            }
        });

    }


    @Override
    public void onBackPressed() {
        if(fragmentID == R.id.home_menu && fragment1 instanceof SearchFragment){
            handleNavigation(R.id.home_menu);
        }
        else if(fragmentID == R.id.favorite_menu && fragment2 instanceof HistoryDetailFragment){
            handleNavigation(R.id.favorite_menu);
        }
        else if(fragmentID == R.id.chat_menu && fragment3 instanceof HistoryDetailFragment){
            handleNavigation(R.id.chat_menu);
        }
        else if(fragmentID == R.id.profile_menu && fragment4 instanceof EditProfileFragment){
            handleNavigation(R.id.profile_menu);
        }else{
            if(backPressed){
                finish();
                Log.w("===== Main Activity", "onBackPressed: " + "back pressed true");
            }
            else{
                Toast.makeText(this, "Ketuk kembali satu kali lagi untuk menutup aplikasi", Toast.LENGTH_SHORT).show();
                backPressed = true;
                Log.w("===== Main Activity", "onBackPressed: " + "back pressed set to true");
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(2000);
                            backPressed = false;
                            Log.w("===== Main Activity", "onBackPressed: " + "back pressed false from thread sleep");
                        }
                        catch (Exception e){
                            System.err.println(e);
                            Log.w("===== Main Activity", "onBackPressed: " + e.getMessage() );
                        }
                    }
                }).start();
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSparseParcelableArray(FRAGMENT_STATE_ARRAY, savedStateSparseArray);
        outState.putInt(FRAGMENT_INDEX, fragmentIndex);
        //Save the fragment's instance
    }

    public void replaceFragment(int index , Fragment fragment ){
        switch(index){
            case 1:{
                fm.beginTransaction().remove(fragment1).commit();
                fm.beginTransaction().add(R.id.fl_container , fragment , "1").commit();
                fragment1 = fragment;
                active = fragment;
            }break;
            case 2:{
                fm.beginTransaction().remove(fragment2).commit();
                fm.beginTransaction().add(R.id.fl_container , fragment , "2").commit();
                fragment2 = fragment;
                active = fragment;
            }break;
            case 3:{
                fm.beginTransaction().remove(fragment3).commit();
                fm.beginTransaction().add(R.id.fl_container , fragment , "3").commit();
                fragment3 = fragment;
                active = fragment;

            }break;
            case 4:{
                fm.beginTransaction().remove(fragment4).commit();
                fm.beginTransaction().add(R.id.fl_container , fragment , "4").commit();
                fragment4 = fragment;
                active = fragment;

            }break;
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent( event );
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;
        return handleNavigation(item.getItemId());
    }

    public boolean handleNavigation(int id){
        switch (id){
            case R.id.home_menu:
                if(fragmentID == R.id.home_menu || fragment1 == null){
                    fm.beginTransaction().remove(fragment1).commit();
                    fragment1 = DashboardFragment.newInstance(CurrentUserModel.getCurrentUserModel().getId() , CurrentUserModel.getCurrentUserModel().getRole());
                    fm.beginTransaction().add(R.id.fl_container , fragment1 , "1").commit();
                }else {
                    fm.beginTransaction().hide(active).show(fragment1).commit();
                }

                active = fragment1;
                fragmentID = R.id.home_menu;
                return true;
            case R.id.favorite_menu:
                if(fragmentID == R.id.favorite_menu || fragment2 == null){
                    fm.beginTransaction().remove(fragment2).commit();
                    fragment2 = NotificationListFragment.newInstance(CurrentUserModel.getCurrentUserModel().getId());
                    fm.beginTransaction().add(R.id.fl_container , fragment2 , "2").commit();
                }else {
                    fm.beginTransaction().hide(active).show(fragment2).commit();
                }

                active = fragment2;
                fragmentID = R.id.favorite_menu;
                return true;
            case R.id.chat_menu:
                if(fragmentID == R.id.chat_menu || fragment3 == null){
                    fm.beginTransaction().remove(fragment3).commit();
                    fragment3 = HistoryListFragment.newInstance(CurrentUserModel.getCurrentUserModel().getId());
                    fm.beginTransaction().add(R.id.fl_container , fragment3 , "3").commit();
                }else {
                    fm.beginTransaction().hide(active).show(fragment3).commit();
                }

                active = fragment3;
                fragmentID = R.id.chat_menu;
                return true;
            case R.id.profile_menu:
                if(fragmentID == R.id.profile_menu || fragment4 == null){
                    fm.beginTransaction().remove(fragment4).commit();
                    fragment4 = ProfileFragment.newProfileInstance(CurrentUserModel.getCurrentUserModel().getId());
                    fm.beginTransaction().add(R.id.fl_container , fragment4 , "4").commit();
                }else {
                    fm.beginTransaction().hide(active).show(fragment4).commit();
                }

                active = fragment4;
                fragmentID = R.id.profile_menu;
                return true;
        }
        return false;
    }


    public AHBottomNavigation getBottomNavigation() {
        return bottomNavigation;
    }
}
